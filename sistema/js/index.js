app.controller('homeController', function ($scope, $http) {

    var fecha=new Date();
    /*var mes=fecha.getMonth()+1;
    var fechaS= fecha.getDate()+"/"+ mes+"/"+fecha.getFullYear();
 alert(fechaS);*/
    var fechaA=sumarDias(fecha,-1);
   /* mes=fechaA.getMonth()+1;
    var fechaAS= fechaA.getDate()+"/"+ mes+"/"+fechaA.getFullYear();
    
    alert(fechaAS);*/
    $http.post(DIR_SERVICIOS_JS + 'Service1.asmx/resumenEstadisticasDia', {
            fecha:  fechaA
        })
        .then(function (response) {

            var oJSON = JSON.parse(response.data.d);
            $scope.resumenA = oJSON.resumen;
            $scope.JSONResumenDistribuidoresA = oJSON.resumenDistribuidor;

            cargarGraficosEstadistica('#piechart-placeholderA', $scope.resumenA.porcentajeSatisfechos,$scope.resumenA.porcentajeRegularSatisfechos,  $scope.resumenA.porcentajeInsatisfechos);


        }, (function (errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));

    fecha=new Date();

    $http.post(DIR_SERVICIOS_JS + 'Service1.asmx/resumenEstadisticasDia', {
            fecha: fecha
        })
        .then(function (response) {

            var oJSON = JSON.parse(response.data.d);
            $scope.resumen = oJSON.resumen;
            $scope.JSONResumenDistribuidores = oJSON.resumenDistribuidor;

            cargarGraficosEstadistica('#piechart-placeholder', $scope.resumen.porcentajeSatisfechos,$scope.resumen.porcentajeRegularSatisfechos, $scope.resumen.porcentajeInsatisfechos);


        }, (function (errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));



});


function cargarGraficosEstadistica(div, porcentajeSatisfechos, porcentaRegularSatisfecho, porcentajeInsatisfechos) {
    var placeholder = $(div).css({
        'width': '90%',
        'min-height': '150px'
    });
    var data = [
        {
            label: "clientes satisfechos",
            data: porcentajeSatisfechos,
            color: "#68BC31"
        },
        
        {
            label: "clientes regular satisfechos",
            data: porcentaRegularSatisfecho,
            color: "#F48F44"
        },

        {
            label: "clientes NO satisfechos",
            data: porcentajeInsatisfechos,
            color: "#DA5430"
        },

			  ]

    function drawPieChart(placeholder, data, position) {
        $.plot(placeholder, data, {
            series: {
                pie: {
                    show: true,
                    tilt: 0.8,
                    highlight: {
                        opacity: 0.25
                    },
                    stroke: {
                        color: '#fff',
                        width: 2
                    },
                    startAngle: 2
                }
            },
            legend: {
                show: true,
                position: position || "ne",
                labelBoxBorderColor: null,
                margin: [-30, 15]
            },
            grid: {
                hoverable: true,
                clickable: true
            }
        })
    }
    drawPieChart(placeholder, data);

    /**
    we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
    so that's not needed actually.
    */
    placeholder.data('chart', data);
    placeholder.data('draw', drawPieChart);


    //pie chart tooltip example
    var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
    var previousPoint = null;

    placeholder.on('plothover', function (event, pos, item) {
        if (item) {
            if (previousPoint != item.seriesIndex) {
                previousPoint = item.seriesIndex;
                var tip = item.series['label'] + " : " + item.series['percent'] + '%';
                $tooltip.show().children(0).text(tip);
            }
            $tooltip.css({
                top: pos.pageY + 10,
                left: pos.pageX + 10
            });
        } else {
            $tooltip.hide();
            previousPoint = null;
        }

    });
}
function sumarDias(fecha, dias){
  fecha.setDate(fecha.getDate() + dias);
  return fecha;
}
