var code = "";


function getLocalIPs(callback) {
    var ips = [];

    var RTCPeerConnection = window.RTCPeerConnection ||
        window.webkitRTCPeerConnection || window.mozRTCPeerConnection;

    var pc = new RTCPeerConnection({
        // Don't specify any stun/turn servers, otherwise you will
        // also find your public IP addresses.
        iceServers: []
    });
    // Add a media line, this is needed to activate candidate gathering.
    pc.createDataChannel('');

    // onicecandidate is triggered whenever a candidate has been found.
    pc.onicecandidate = function (e) {
        if (!e.candidate) { // Candidate gathering completed.
            pc.close();
            callback(ips);
            return;
        }
        var ip = /^candidate:.+ (\S+) \d+ typ/.exec(e.candidate.candidate)[1];
        if (ips.indexOf(ip) == -1) // avoid duplicate entries (tcp/udp)
            ips.push(ip);
    };
    pc.createOffer(function (sdp) {
        pc.setLocalDescription(sdp);
    }, function onerror() {});
}


getLocalIPs(function (ips) { // <!-- ips is an array of local IP addresses.
    var f = new Date();
    var n = f.getDate() + f.getFullYear();
     
   // code = (n.toString() +'---'+ ips[0].toString()+'---'+window.ui.os); // hex_md5(ips[0]); ;
   
     
});




function showLoading(nombreDiv, msg) {
    $(nombreDiv).addClass("screemDark");

    $(nombreDiv + " .divMensaje").detach();
    // $(nombreDiv).prepend("<div class='divMensaje'>Cargando...<div/>"); 
}

function hideLoading(nombreDiv) {
    $(nombreDiv).removeClass("screemDark");
    $(nombreDiv + " .divMensaje").detach();


}

$('#busquedaGeneral').on('input',function(e){
    //alert($('#busquedaGeneral').val());
     var myHilitor = new Hilitor("content"); // id of the element to parse
  myHilitor.apply($('#busquedaGeneral').val());
});

/*
$("#busquedaGeneral").change(function(){
  alert("The text has been changed.");
});*/

var app = angular.module('appWillay', ['ngRoute', 'ngCookies', 'ngDialog', 'ui.rCalendar','ui.bootstrap']);
app.constant('paginationConfig', {
        itemsPerPage: 10,
        boundaryLinks: false,
        directionLinks: true,
        firstText: 'inicio',
        previousText: 'Anterior',
        nextText: 'Siguiente',
        lastText: 'Fin',
        rotate: true
    })
app.filter('beginning_data', function() {
    return function(input, begin) {
        if (input) {
            begin = +begin;
            return input.slice(begin);
        }
        return [];
    }
});


var DIR_SERVICIOS_JS = 'sistema/servicios/';


//damos configuración de ruteo a nuestro sistema de login
app.config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    var rutaPlantilla="sistema/plantillas/";
    $routeProvider.when("/login", {
            controller: "loginController",
            templateUrl: rutaPlantilla+"login.html"

        })
        .when("/home", {
            controller: "homeController",
            templateUrl: rutaPlantilla+"index.html"
        })
    
     .when("/agregarAnuncio", {
            controller: "agregarAnuncioController",
            templateUrl: rutaPlantilla+"agregarAnuncio.html"
        })
        .when("/anunciosVigentes", {
            controller: "anunciosVigentesController",
            templateUrl: rutaPlantilla+"anunciosVigentes.html"
        })

        .when("/", {
            controller: "homeController",
            templateUrl: rutaPlantilla+"index.html"

        })
        .when("/salir", {
            controller: "loginController",
            templateUrl: rutaPlantilla+"login.html"

        })
        .when("/controlVenta", {
            controller: "controlVentaController",
            templateUrl: rutaPlantilla+"controlVenta.html"
        })
        .when("/deudas", {
            controller: "deudasController",
            templateUrl: rutaPlantilla+"deudas.html"
        })
        .when("/jornada", {
            controller: "controlJornadaController",
            templateUrl: rutaPlantilla+"jornadaDistribuidor.html"
        })
        .when("/stock", {
            controller: "stockController",
            templateUrl: rutaPlantilla+"stock.html"
        })
        .when("/movimientoFierros", {
            controller: "movimientoFierrosController",
            templateUrl: rutaPlantilla+"movimientoFierros.html"
        })
        .when("/movimientoCargas", {
            controller: "movimientoCargasController",
            templateUrl: rutaPlantilla+"movimientoCargas.html"
        })
        .when("/jornadaMensual", {
            controller: "controlDistribuidoresJornadaController",
            templateUrl: rutaPlantilla+"distribuidoresJornadaMensual.html"
        })
        .when("/faltasJornada", {
            controller: "faltasJornadaController",
            templateUrl: rutaPlantilla+"faltasJornada.html"
        })
        .when("/pagosydescuentos", {
            controller: "pagosydescuentosController",
            templateUrl: rutaPlantilla+"pagosydescuentos.html"
        })
      .when("/transferencias", {
            controller: "transferenciasController",
            templateUrl: rutaPlantilla+"transferencias.html"
        })
});





app.factory("auth", function ($rootScope, $cookies, $cookieStore, $location, $http) {
    return {
        login: function (username, password) {






            $http.post(DIR_SERVICIOS_JS + 'service.php', {
                    usuario: username,
                    clave: password,
                    metodo:"iniciarSesion"
                })
                .then(function (response) {

                    var val = response.data;
                    $rootScope.mensajeLogin = val.usuario.mensaje;
              //  alert(code);
                    if (val.usuario.hallado == true) {
                      
                            $cookies.username = username;

                            $cookieStore.put("logueado", true);
                            $cookieStore.put("usuario", username);
                            $cookieStore.put("nombreUsuario", val.usuario.nombres);
                            $cookieStore.put("tipoUsuario", val.usuario.tipo);
                            $cookieStore.put("token", val.usuario.token);
                           

                            $rootScope.username = username;
                            $rootScope.password = "";
                            $rootScope.logueado = true;
                           /* $rootScope.localesGlobal = val.locales;
                            $rootScope.localGlobal = val.locales[0];
                            $cookieStore.put("local", $rootScope.localGlobal);*/
                            $rootScope.nombreUsuario = val.usuario.nombres;
                           /* $rootScope.nombreLocal = $rootScope.localGlobal.nombres;
                            $cookieStore.put("modoVisual", 'admi');*/
                            //mandamos a la home
                            //   alert("valido");
                            $rootScope.mensajeLogin = "";
                            $location.path("/home");
                         
                    }



                }, (function (errorResponse) {
                    //console.log('Error: ' + errorResponse);
                }));


            //creamos la cookie con el nombre que nos han pasado

            /*if (username == "admin" && password == "admin") {
                $cookies.username = username;
                $cookieStore.put("local", local);
                $cookieStore.put("usuario", username);
                $rootScope.username = username;
                $rootScope.password = password;
                $rootScope.logueado = true;
                //mandamos a la home
                $location.path("/home");
            }*/
        },
        logout: function () {
            //al hacer logout eliminamos la cookie con $cookieStore.remove
            var tok = $cookieStore.get("token");
            $cookieStore.remove("token");
            $cookieStore.remove("codigo");
            $cookieStore.remove("username");
            $cookieStore.remove("password");
            $cookieStore.remove("usuario");
            $cookieStore.remove("modoVisual");
            $cookieStore.remove("logueado");
            $cookieStore.remove("tipoUsuario");
            $rootScope.usuario = null;
            $rootScope.password = null;


          /*  $http.post(DIR_SERVICIOS_JS + 'Service1.asmx/cerrarSesion', {
                    pToken: tok
                })
                .then(function (response) {

                    //$scope.JSONlista = JSON.parse(response.data.d); 



                }, (function (errorResponse) {
                    //console.log('Error: ' + errorResponse);
                }));*/

            //mandamos al login
            // $location.path("/login");
        },
        checkStatus: function () {
            var codig = $cookieStore.get("codigo");
            var modoV = $cookieStore.get("modoVisual");


            var tk = $cookieStore.get("token");
            if (tk != null && tk != '' && $location.path() != '/login' && $location.path() != '/salir') {

                //alert($location.path());
                $rootScope.nombreUsuario = $cookieStore.get("nombreUsuario");
               /* $http.post(DIR_SERVICIOS_JS + 'Service1.asmx/validarSesion', {
                        pToken: tk
                    })
                    .then(function (response) {

                        var obj = JSON.parse(response.data.d);
                        var v = obj.valido;

                        if (v == false) {

                            $cookieStore.remove("token");
                            $cookieStore.remove("username");
                            $cookieStore.remove("password");
                            $cookieStore.remove("usuario");
                            $cookieStore.remove("logueado");
                            $cookieStore.remove("modoVisual");
                            $cookieStore.remove("logueado");
                            $cookieStore.remove("tipoUsuario");

                            $rootScope.usuario = null;
                            $rootScope.password = null;
                            $location.path("/login");
                        }




                    }, (function (errorResponse) {
                        //console.log('Error: ' + errorResponse);
                    }));*/

                //if (codig == code) 
                if (modoV == "lector") {
                    $location.path("/misPedidos");

                } else {
                    var user = $cookieStore.get("usuario");
                    var log = $cookieStore.get("logueado");
                    var tipoU = $cookieStore.get("tipoUsuario");
                    //creamos un array con las rutas que queremos controlar

                    var rutasPrivadas = ["/", "/venta", "/home", "/dashboard", "/login", "/jornadaMensual", "/pagosydescuentos", "/faltasJornada", "/controlVenta"];

                    if (this.in_array($location.path(), rutasPrivadas) && (user == "" || log != true)) {
                        $location.path("/login");
                    } else {
                        var rutasPrivadasAd = ["/jornadaMensual", "/faltasJornada"];
                        if (this.in_array($location.path(), rutasPrivadasAd) && (tipoU != "super-administrador")) {
                            $location.path("/home");
                        }

                    }
                    //en el caso de que intente acceder al login y ya haya iniciado sesión lo mandamos a la home
                    if (this.in_array("/login", rutasPrivadas) && typeof ($cookies.username) != "undefined") {
                        // $location.path("/home");
                    }

                    if (user == "" || log != true) {
                        $location.path("/login");
                    }
                }

            } else
                $location.path("/login");


        },
        in_array: function (needle, haystack) {
            var key = '';
            for (key in haystack) {
                if (haystack[key] == needle) {
                    return true;
                }
            }
            return false;
        }
    }
});

function getIP() {
    window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection; //compatibility for firefox and chrome
    var pc = new RTCPeerConnection({
            iceServers: []
        }),
        noop = function () {};
    pc.createDataChannel(""); //create a bogus data channel
    pc.createOffer(pc.setLocalDescription.bind(pc), noop); // create offer and set local description
    pc.onicecandidate = function (ice) { //listen for candidate events
        if (!ice || !ice.candidate || !ice.candidate.candidate) return;
        var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
        //console.log('my IP: ', myIP);   
        pc.onicecandidate = noop;
        return myIP;
    };

}




app.controller('loginController', function ($scope, $http, auth) {
    //la función login que llamamos en la vista llama a la función
    //login de la factoria auth pasando lo que contiene el campo
    //de texto del formulario
    auth.logout();

    $scope.username = "";
    var ip = "";
    /* if (java && java.net)
ip = ''+java.net.InetAddress.getLocalHost().getHostAddress();
else ip = 'unknown';*/


    $scope.salir = function () {
        auth.logout();

    }



    $scope.login = function () {
        auth.login($scope.usuario, $scope.clave);
        $scope.clave = "";
    }

});

app.run(function ($rootScope, $cookieStore, auth) {
    //al cambiar de rutas


    $rootScope.$on('$routeChangeStart', function () {
        //llamamos a checkStatus, el cual lo hemos definido en la factoria auth
        //la cuál hemos inyectado en la acción run de la aplicación
        $rootScope.token = $cookieStore.get("token");
        $rootScope.tipoUsuario = $cookieStore.get("tipoUsuario");
        $rootScope.modoVisual = $cookieStore.get("modoVisual");
        auth.checkStatus();
    })
})
