app.controller('agregarAnuncioController', function($scope,$rootScope, $http,$cookieStore,ngDialog) {

     this.myDate = new Date();
  this.isOpen = false;
    
$scope.mostrarDetalleDeuda=false;
$scope.JSONDeudas=[];
  $scope.JSONCategorias=[];
    $scope.JSONDepartamentos=[];
    $scope.JSONProvincias=[];
    $rootScope.tituloPagina='Anuncios';
    $scope.JSONDistritos=[];
    
  

  var todayTime = new Date();
    var month = todayTime.getMonth() + 1;
    var day = todayTime.getDate();
    var year = todayTime.getFullYear();
    $scope.fecha = month + "/" + day + "/" + year;
    
    $scope.localDistribucion = $cookieStore.get("local");
    
    
       // listamos los anuncios 
    $http.post(DIR_SERVICIOS_JS + 'service.php', {
        token:'',
        metodo:'listaCategorias'
    })
        .then(function(response) {
           
            $scope.JSONCategorias = response.data.categorias; 
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
    
    
     $scope.listarAnuncios= function()
     {
    // listamos las categoriuas 
    $http.post(DIR_SERVICIOS_JS + 'anuncio.php', {
        token:'',
        operacion:'listar',
        habilitados:1,
        NOVencidos:0
    })
        .then(function(response) {
           
            $scope.JSONAnucios = response.data.anuncios; 
         $scope.filter_data = $scope.JSONDepartamentos.length;
        $scope.entire_user = $scope.JSONDepartamentos.length;
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
     }
     $scope.listarAnuncios();
    // listamops los departamentos 
 $http.post(DIR_SERVICIOS_JS + 'service.php', {
        token:'',
        metodo:'listaDepartamentos',
       departamento_id:0
    })
        .then(function(response) {
           
            $scope.JSONDepartamentos  = response.data.departamentos; 
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
    
   
    $scope.guardarAnuncio=function()
    {
         $http.post(DIR_SERVICIOS_JS + 'anuncio.php', {
        token:'',
            
        operacion:'guardar',
        id:0,
        titulo:$scope.titulo,
        descripcion:$scope.descripcion,
        duracion:10,
        destacado:0,
        habilitado:1,
             
        subcategoria_id:$scope.subcategoria.id,
        usuario_id:0,
        nombreContacto:$scope.nombre,
        telefonoContacto1:$scope.telefonoContacto1,
        telefonoContacto2:$scope.telefonoContacto2,
        latitud:$scope.latitud,
        longitud:$scope.longitud,
        distrito_id: $scope.distrito.id,
        movil:0    
    })
        .then(function(response) {
           
           var resultado = data == null ?'nulo' : data.exito;// (data.categorias instanceof Array ? data.categorias : [data.categorias]);

        var mensaje = data == null ?'nulo' : data.mensaje;
      


         if (resultado==1)
        {

           
            alert('El anuncio fue guardada',1);

        }

        else

        {

            alert('El anuncio no pudo ser guardado',0);

        }
             
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
          $scope.listarAnuncios(); 
        
    }
  
$scope.calcularTotalAmortizacion=function()
{
  var totalM=0;
  for (var i = 0; i < $scope.JSONDeudasClienteCargas.length; i++) {
           
             var amortizacion=$scope.JSONDeudasClienteCargas[i].amortizacion;
             if (amortizacion!=undefined)
             totalM= totalM +amortizacion;
            
        }
       $scope.totalCobroDeuda=totalM;  

}

 $scope.cambioCategoria=function()
    {
        var categoria_id=$scope.categoria.id;
          $http.post(DIR_SERVICIOS_JS + 'service.php', {
        token:'',
        metodo:'listaSubCategorias',
        categoria_id:categoria_id
    })
        .then(function(response) {
           
            $scope.JSONSubCategorias = response.data.subcategorias; 
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
        
    }
 $scope.cambioDistrito= function ()
 {
     $scope.latitud=$scope.distrito.latitud.toString();
     $scope.longitud=  $scope.distrito.longitud.toString();     
 }
 
 $scope.cambioDepartamento=function()
 {
      var departamento_id=$scope.departamento.id;
          $http.post(DIR_SERVICIOS_JS + 'service.php', {
        token:'',
        metodo:'listaProvincias',
        departamento_id:departamento_id,
        provincia_id:0
    })
        .then(function(response) {
           
            $scope.JSONProvincias = response.data.provincias; 
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
     
 }
$scope.cambioProvincia= function ()
{
    var provincia_id=$scope.provincia.id;
          $http.post(DIR_SERVICIOS_JS + 'service.php', {
        token:'',
        metodo:'listaDistritos',
        provincia_id:provincia_id,
        distrito_id:0
    })
        .then(function(response) {
           
            $scope.JSONDistritos = response.data.distritos; 
           
    
    
        }, (function(errorResponse) {
            //console.log('Error: ' + errorResponse);
        }));
        
}
 
$scope.fnMostrarDeuda= function(id)
{
  $scope.mostrarDetalleDeuda=true;
  $http.post(DIR_SERVICIOS_JS +'Service1.asmx/deudasCliente',{ClienteId:id})
      .then(function(response) {
       // var array=data == null ? [] : (data.d instanceof Array ? data.d : [data.d]);
       
       var datos=JSON.parse(response.data.d);// 
       $scope.Cliente=datos.cliente;
       $scope.JSONDeudasClienteCargas=datos.cargas;
       $scope.JSONDeudasClienteFierros=datos.fierros;
      

      // hideLoading("#divDeudas");
      $scope.totalCobroDeuda=0;
  $scope.totalCobroFierro=0
      
         
      },function(responseError) {
       // console.log('Error: ' + data);
      //  hideLoading("#divDeudas");
      }) ;

}
$scope.guardarCobranza= function()
{
      var cobranzasCarga = [];
    var cobranzasFierros=[];
    
    /********************** guardamos las cargas clientes *********************/
    
    var observacion ="";
    var correcto=true;
    for (var i = $scope.JSONDeudasClienteCargas.length - 1; i >= 0; i--) {
        var amortizacion=$scope.JSONDeudasClienteCargas[i].amortizacion;
        var deuda=$scope.JSONDeudasClienteCargas[i].deuda;
        if (amortizacion!=null && amortizacion>0)
            {
                if (amortizacion>deuda)
                    {
                        correcto=false;
                        
                    }
                else
                    {
                        cobranzasCarga.unshift({
                                 "venta_id": $scope.JSONDeudasClienteCargas[i].id,
                                "monto":amortizacion,
                              "observacion":observacion
                        });
                        
                    }
            }
       
    }
    
   
    
    /********************** guardamos los fierros cobrados *****************************/
    observacion ="";
    correcto=true;
    
        for (var i = $scope.JSONDeudasClienteFierros.length - 1; i >= 0; i--) {
        var nroBalonesDevolucion=$scope.JSONDeudasClienteFierros[i].devolucionFierros;
        var deuda=$scope.JSONDeudasClienteFierros[i].deuda;
        if (nroBalonesDevolucion!=null && nroBalonesDevolucion>0)
            {
                if (nroBalonesDevolucion>deuda)
                    {
                        correcto=false;
                        
                    }
                else
                    {
                        cobranzasFierros.unshift({
                                 "venta_id": $scope.JSONDeudasClienteFierros[i].venta_id,
                                "producto_codigo":$scope.JSONDeudasClienteFierros[i].producto_codigo,
                                "observacion":observacion,
                                "cantidad":nroBalonesDevolucion,
                                "localId": $scope.localDistribucion.id
                            
                        });
                        
                    }
            }
       
    }
    
    
    $http.post(DIR_SERVICIOS_JS + 'Service1.asmx/registrarCobranza', {DNI:$scope.distribuidor.DNI,pagos:cobranzasCarga,devolucionesFierros:cobranzasFierros })
       .then(function(response) {
          $scope.mostrarDetalleDeuda=false;
           //$scope.JSONlista = JSON.parse(response.data.d); 
       $scope.refrescarDeudas();
         ngDialog.open({
                    template: '<h2>Cobranza registrada!</h2>',
                      className: 'ngdialog-theme-default',
                    plain: true
                });
       
       
       //$('#modalCombustible').modal('hide');
       
       // alert("ejecutado"+cobranzasCarga[0].monto);
          
   
   
       }, (function(errorResponse) {
           //console.log('Error: ' + errorResponse);
       }));
    
    
   
    
}
  
 $scope.refrescarDeudas= function()
  {
    //showLoading("#divDeudas","Cargando...");
     $http.post(DIR_SERVICIOS_JS +'Service2.asmx/listaDeudasCompletas',{})
      .then(function(response) {
       // var array=data == null ? [] : (data.d instanceof Array ? data.d : [data.d]);
       
       $scope.JSONDeudas=JSON.parse(response.data.d);// 


      // hideLoading("#divDeudas");
      
         
      },function(responseError) {
       // console.log('Error: ' + data);
      //  hideLoading("#divDeudas");
      }) ;

      $http.post(DIR_SERVICIOS_JS +'Service1.asmx/deudasCobradasDia',{pFecha: $scope.fecha})
      .then(function(response) {
       // var array=data == null ? [] : (data.d instanceof Array ? data.d : [data.d]);
       
        var datos =JSON.parse(response.data.d);// 
        $scope.JSONCobranzasCargas=datos.cobranzasCarga;
        $scope.JSONCobranzasFierros=datos.cobranzasFierros;


      // hideLoading("#divDeudas");
      
         
      },function(responseError) {
       // console.log('Error: ' + data);
      //  hideLoading("#divDeudas");
      }) ;



  };

$scope.seleccionar= function()
{
  
   $scope.itemsSeleccionados='';
   for (var i =  $scope.JSONDeudas.length - 1; i >= 0; i--) {
       if ($scope.JSONDeudas[i].seleccionado==true)
       {
        $scope.itemsSeleccionados=$scope.itemsSeleccionados+','+$scope.JSONDeudas[i].cliente_id;
       }
    } 
 
}

  $scope.refrescarDeudas();
    /* cargasmo a los distribuidores  */
    $http.post( DIR_SERVICIOS_JS +'Service2.asmx/listaDistribuidores', {}).then(function(response) {
        // var array=data == null ? [] : (data.d instanceof Array ? data.d : [data.d]);
        $scope.JSONDistribuidores = JSON.parse(response.data.d); // 
        $scope.Distribuidor=$scope.JSONDistribuidores[0];
    },function(responseError){

    }  );


});