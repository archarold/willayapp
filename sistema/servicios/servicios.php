<?php
include "conexion.php";
$respuesta = array();

//**** listar  categorias *********
function listaCategorias()
{
 $respuesta=array();
    try
    {
        // leemos las categoria disponibles en la  base de datos
        $conexion = crearConexion();
        if ($conexion != null) {
            $sql                     = "select * from categoria order by orden";
            $categoria_id            = isset($_GET["idSeleccionado"]) ? $_GET["idSeleccionado"] : null;
            $resultado               = $conexion->query($sql);
            $respuesta["categorias"] = array();
            /*
            $detalle = $conexion->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo '{"categorias": ' . json_encode($detalle) . '}';*/

            while ($row = $resultado->fetch_assoc()) {
                array_push($respuesta["categorias"], $row);
                if ($categoria_id != null && $categoria_id == $row["id"]) {
                    $respuesta["itemSeleccionado"] = $row;
                }

            }
            $resultado->free();
            $conexion->close();
            $respuesta["exito"]   = 1;
            $respuesta["mensaje"] = MSG_CONSULTA_EXITO;
        } else {
            $respuesta["exito"]   = 0;
            $respuesta["mensaje"] = MSG_ERROR_CONEXION_BD;
        }

    } catch (Exception $ex) {
        $respuesta["exito"]   = 0;
        $respuesta["mensaje"] = MSG_ERROR_CONSULTA;

    }
 //echo json_encode($respuesta);
    return  $respuesta;
    
} //****-- fin de listar categoria 
//echo json_encode($respuesta);


function listaSubCategorias($categoria_id)
{
 $respuesta=array();
    try
    {
        // leemos las categoria disponibles en la  base de datos
        $conexion = crearConexion();
        if ($conexion != null) {
            $sql                     = "select * from subcategoria where categoria_id=$categoria_id order by nombre";
           
            $categoria_id            = isset($_GET["idSeleccionado"]) ? $_GET["idSeleccionado"] : null;
            $resultado               = $conexion->query($sql);
            $respuesta["subcategorias"] = array();
            /*
            $detalle = $conexion->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo '{"categorias": ' . json_encode($detalle) . '}';*/

            while ($row = $resultado->fetch_assoc()) {
                array_push($respuesta["subcategorias"], $row);
                if ($categoria_id != null && $categoria_id == $row["id"]) {
                    $respuesta["itemSeleccionado"] = $row;
                }

            }
            $resultado->free();
            $conexion->close();
            $respuesta["exito"]   = 1;
            $respuesta["mensaje"] = MSG_CONSULTA_EXITO;
        } else {
            $respuesta["exito"]   = 0;
            $respuesta["mensaje"] = MSG_ERROR_CONEXION_BD;
        }

    } catch (Exception $ex) {
        $respuesta["exito"]   = 0;
        $respuesta["mensaje"] = MSG_ERROR_CONSULTA;

    }
 //echo json_encode($respuesta);
    return  $respuesta;
    
} //****-- fin de listar categoria 


//****= lñiostar de partamentos 
function listaDepartamentos($departamento_idSel)
{
     $respuesta = array();
try
{
     // leemos las categoria disponibles en la  base de datos
    $conexion=crearConexion();
    if ($conexion!=null)
    {
      $departamento_id= is_numeric( $departamento_idSel) ?$departamento_idSel:null;
        
      $sql="select * from departamento";
       
      $resultado= $conexion->query($sql);
      $respuesta["departamentos"]=array();
   
        
      while ($row = $resultado->fetch_assoc())
       {
         $departamento = $row;
 
         array_push($respuesta["departamentos"],$departamento);
          
        
         if ( $departamento_id!=null&&$row['id']==$departamento_id)
         {
             
            $respuesta["itemSeleccionado"]=$row;
         }
       }
       $resultado->free();
       $conexion->close();
       $respuesta["exito"]=1;
       $respuesta["mensaje"]=MSG_CONSULTA_EXITO;
   }
   else {
     $respuesta["exito"]=0;
     $respuesta["mensaje"]=MSG_ERROR_CONEXION_BD;
   }

}
catch(Exception $ex)
{
  $respuesta["exito"]=0;
  $respuesta["mensaje"]=MSG_ERROR_CONSULTA;

}
    return $respuesta;
}

function listaProvincias($departamento_id,$provincia_id)
{
    try {

$departamento_id_get= isset($departamento_id)&&is_numeric($departamento_id)?$departamento_id:null;

$provincia_idSel=isset($provincia_id)&& is_numeric($provincia_id)?$provincia_id:null;

    $respuesta= array();



  $conexion= crearConexion();

  if ($conexion!=null)

  {

    $sql="select * from provincia where departamento_id=".$departamento_id_get;





      $resultado= $conexion->query($sql);

      $respuesta["provincias"]=array();

      while ($row = $resultado->fetch_assoc())

       {

        



         array_push($respuesta["provincias"],$row);

         if ( $provincia_idSel!=null&&$row['id']==$provincia_idSel)

         {

            $respuesta["itemSeleccionado"]=$row;

         }

       }

       $resultado->free();

     $conexion->close();

     $respuesta["exito"]=1;

     $respuesta["mensaje"]=MSG_CONSULTA_EXITO;



  }

  else {

    $respuesta["exito"]=0;

    $respuesta["mensaje"]=MSG_ERROR_CONEXION_BD;





  }



} catch (Exception $e) {

  $respuesta["exito"]=0;

  $respuesta["mensaje"]=MSG_ERROR_CONSULTA;

}

return $respuesta;
    
}

function listaDistritos($provincia_id,$distrito_id)
{
    try {
$provincia_id_get= isset($provincia_id)&&is_numeric($provincia_id)? $provincia_id:null;
$distrito_id_sel=isset($distrito_id)&& is_numeric($distrito_id)?$distrito_id:null;
$respuesta= array();

  $conexion= crearConexion();
  if ($conexion!=null)
  {
    $sql="select * from distrito where provincia_id=".$provincia_id_get;

     $resultado= $conexion->query($sql);
      $respuesta["distritos"]=array();
      while ($row = $resultado->fetch_assoc())
       {
        

         array_push($respuesta["distritos"],$row);
         if ( $distrito_id_sel!=null&&$row['id']==$distrito_id_sel)
         {
            $respuesta["itemSeleccionado"]=$row;
         }
       }
       $resultado->free();

     $conexion->close();
     $respuesta["exito"]=1;
     $respuesta["mensaje"]=MSG_CONSULTA_EXITO;

  }
  else {
    $respuesta["exito"]=0;
    $respuesta["mensaje"]=MSG_ERROR_CONEXION_BD;


  }

} catch (Exception $e) {
  $respuesta["exito"]=0;
  $respuesta["mensaje"]=MSG_ERROR_CONSULTA;
}

    return $respuesta;
}

//*******================ iniciar sesion =========================
function iniciarSesion($usuario,$clave)
{
     try {
  
         
         $respuesta= array();

           $conexion= crearConexion();
           if ($conexion!=null)
           {
            $respuesta =array();
        
           $sql="call iniciarSesion('$usuario','$clave')";
           $resultado=$conexion->query($sql);
               
           if(!$resultado) {
           //Mensaje de error
           echo $conexion->error;
        }   

          while($row=$resultado->fetch_assoc()) {
             # code...
             $respuesta["usuario"]=$row;
           }

              $conexion->close();
              $respuesta["exito"]=1;
              $respuesta["mensaje"]=MSG_CONSULTA_EXITO;

           }
           else {
             $respuesta["exito"]=0;
             $respuesta["mensaje"]=MSG_ERROR_CONEXION_BD;


           }

         } catch (Exception $e) {
           $respuesta["exito"]=0;
           $respuesta["mensaje"]=MSG_ERROR_CONSULTA;
         }
    
    return  $respuesta;
}
?>
