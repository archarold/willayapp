<?php
include("conexion.php");

function guardar()
{
    try {

      
        $titulo= $_POST["titulo"];
        $descripcion= $_POST["descripcion"];
        $subcategoria_id= $_POST["subcategoria_id"];
        $usuario_id= $_POST["usuario_id"];
        $nombreContacto= $_POST["nombreContacto"];
        $telefonoContacto1= $_POST["telefonoContacto1"];
        $telefonoContacto2= isset($_POST["telefonoContacto2"])?$_POST["telefonoContacto2"]:null;
        $latitud= isset($_POST["latitud"])?$_POST["latitud"]:null;
        $longitud= isset($_POST["longitud"])?$_POST["longitud"]:null;
        $distrito_id= isset($_POST["distrito_id"])?$_POST["distrito_id"]:null;
        $movil= isset($_POST["movil"])?$_POST["movil"]:0;
$habilitado= isset($_POST["habilitado"])?$_POST["habilitado"]:null;

        $respuesta= array();

          $conexion= crearConexion();
          if ($conexion!=null)
          {
            $fechaactual = getdate();
            $fecha_hoy = new DateTime($fechaactual['year']+'-'+$fechaactual['mon']+'-'+$fechaactual['mday']);
            $fecha_caducidad=clone $fecha_hoy;
            $fecha_caducidad->add(new DateInterval('P10D'));
            $fecha_caducidad_s= $fecha_caducidad->format('Y-m-d') ;
            $fecha_hoy_s= $fecha_hoy->format('Y-m-d H:i:s') ;
          //  echo $fecha_caducidad_s;


          //  $sql="INSERT INTO anuncio(`id`, `titulo`, `descripcion`, `fecha_publicacion`, `fecha_caducidad`, `destacado`, `habilitado`, `subcategoria_id`, `distrito_id`, `usuario_id`, `ubicacion`,`nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `latitud`, `longitud`,`movil`)  VALUES (null,?,?,?,?,0,?,?,?,?,'',?,?,? ,?,?,?)";
              $sql="call guardarAnuncio(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $statement= $conexion->prepare($sql);
             // echo $subcategoria_id.' '.$distrito_id.' '.$usuario_id;
            $statement->bind_param('sissiiiiiisssddi',$a=' ',$b=0, $titulo,$descripcion,$c=3,$d=0,$e= 1,$subcategoria_id,$distrito_id, $usuario_id,$nombreContacto,$telefonoContacto1,$telefonoContacto2,$latitud,$longitud,$f=1);
            $statement->execute();
            $id= $statement->insert_id;
            $statement->close();

            $categoria_id=null;
            $provincia_id=null;
            $departamento_id=null;
            $keywords="";

          //    echo $sql;
            // vemos a que categoria pertenece y el nombre d ela subcategoria a agregamos a los keyword
            if (is_numeric($subcategoria_id))
            {
              $sql="select id,nombre,categoria_id from subcategoria where id =".$subcategoria_id;
              // echo $sql;
              $resultado= $conexion->query($sql);
              while ($row = $resultado->fetch_assoc())
              {

                $categoria_id=$row["categoria_id"];
                $keywords=$keywords." ".$row["nombre"] ;
              }
            }


            $sql="INSERT INTO anuncio_busqueda(`anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`)
              VALUES (?,?,?,0,?,?,null,null,?,?,?,?,?,?,?,?)";

              $statement= $conexion->prepare($sql);
              $statement->bind_param('issssiiiissss',$id, $titulo,$descripcion,$fecha_hoy_s,$fecha_caducidad_s,$subcategoria_id,$categoria_id,$provincia_id,$departamento_id,$keywords,$nombreContacto,$telefonoContacto1,$telefonoContacto2);
              $statement->execute();
              $statement->close();


            $conexion->close();
            $respuesta["exito"]=1;
            $respuesta["mensaje"]=MSG_CONSULTA_EXITO;

          }else {
            $respuesta["exito"]=0;
            $respuesta["mensaje"]=MSG_ERROR_CONEXION_BD;
          }

    } catch (Exception $e) {
      $respuesta["exito"]=0;
      $respuesta["mensaje"]=MSG_ERROR_CONSULTA;
    }
    return $respuesta;
}

function modificar()
{
   try {

       $anuncio_id= $_POST["id"];
        $titulo= $_POST["titulo"];
        $descripcion= $_POST["descripcion"];
        $subcategoria_id= $_POST["subcategoria_id"];
        $fecha_caducidad= $_POST["fecha_caducidad"];
        $usuario_id= $_POST["usuario_id"];
        $nombreContacto= $_POST["nombreContacto"];
        $telefonoContacto1= $_POST["telefonoContacto1"];
        $telefonoContacto2= isset($_POST["telefonoContacto2"])?$_POST["telefonoContacto2"]:null;
        $latitud= isset($_POST["latitud"])?$_POST["latitud"]:null;
        $longitud= isset($_POST["longitud"])?$_POST["longitud"]:null;
        $distrito_id= isset($_POST["distrito_id"])?$_POST["distrito_id"]:null;
     
        $destacado= isset($_POST["destacado"])?$_POST["destacado"]:0;
        $habilitado= isset($_POST["habilitado"])?$_POST["habilitado"]:null;


        $respuesta= array();

          $conexion= crearConexion();
          if ($conexion!=null)
          {
            $fechaactual = getdate();
            $fecha_hoy = new DateTime($fechaactual['year']+'-'+$fechaactual['mon']+'-'+$fechaactual['mday']);
           /* $fecha_caducidad=clone $fecha_hoy;
            $fecha_caducidad->add(new DateInterval('P10D'));*/
            $fechaCaducidad= new DateTime($fecha_caducidad);
            $fechaCaducidadS= $fechaCaducidad->format('Y-m-d H:i:s') ;
          //  echo $fecha_caducidad_s;

            $sql= "UPDATE `anuncio` SET `titulo`=?,`descripcion`=?,`fecha_caducidad`=?,`destacado`=?,`habilitado`=?,`subcategoria_id`=?,`distrito_id`=?,`usuario_id`=?,
            `nombreContacto`=?,`telefonoContacto1`=?,`telefonoContacto2`=?,`latitud`=?,`longitud`=? WHERE id=?";            
            $statement= $conexion->prepare($sql);
            $statement->bind_param('sssiiiiisssddi',$titulo,$descripcion,$fechaCaducidadS,$destacado,$habilitado, $subcategoria_id,$distrito_id, $usuario_id,$nombreContacto,$telefonoContacto1,$telefonoContacto2,$latitud,$longitud,$anuncio_id);
            $statement->execute();
            $id= $statement->insert_id;
            $statement->close();

            $categoria_id=null;
            $provincia_id=null;
            $departamento_id=null;
            $keywords="";

            // vemos a que categoria pertenece y el nombre d ela subcategoria a agregamos a los keyword
            if (is_numeric($subcategoria_id))
            {
              $sql="select id,nombre,categoria_id from subcategoria where id =".$subcategoria_id;
              // echo $sql;
              $resultado= $conexion->query($sql);
              while ($row = $resultado->fetch_assoc())
              {

                $categoria_id=$row["categoria_id"];
                $keywords=$keywords." ".$row["nombre"] ;
              }
            }


            $sql= "UPDATE `anuncio_busqueda` SET
             `titulo`=?,
             `descripcion`=?,
             `destacado`=?,
             `fecha_caducidad`=?,
             `latitud`=?,
             `longitud`=?,
             `subcategoria_id`=?,
             `categoria_id`=?,
             `provincia_id`=?,
             `departamento_id`=?,
             `keywords`=?,
             `nombreContacto`=?,
             `telefonoContacto1`=?,
             `telefonoContacto2`=? WHERE `anuncio_id`=?";       


              $statement= $conexion->prepare($sql);
               $statement->bind_param('ssisddiiiissssi',
               $titulo,$descripcion,$destacado,$fecha_caducidad,$latitud, $longitud,$subcategoria_id,$categoria_id,
               $provincia_id,$departamento_id,$keywords,$nombreContacto,$telefonoContacto1,$telefonoContacto2,$anuncio_id);
              $statement->execute();
              $statement->close();


            $conexion->close();
            $respuesta["exito"]=1;
            $respuesta["mensaje"]=MSG_CONSULTA_EXITO;

          }else {
            $respuesta["exito"]=0;
            $respuesta["mensaje"]=MSG_ERROR_CONEXION_BD;
          }

    } catch (Exception $e) {
      $respuesta["exito"]=0;
      $respuesta["mensaje"]=MSG_ERROR_CONSULTA;
    }
    return $respuesta;

}

function eliminar()
{
     try{

        
       
        $cat_id=isset($_POST['id'])&&is_numeric($_POST['id'])? $_POST['id']:null;
        $respuesta= array();

        $conexion= crearConexion();
        if ($conexion!=null)
        {
            $sql ='DELETE FROM `anuncio`  WHERE id=?';
                $statement= $conexion->prepare($sql);
                $statement->bind_param('i',$cat_id);
                $statement->execute();
                
                $statement->close();

                $sql ='DELETE FROM `anuncio_busqueda`  WHERE anuncio_id=?';
                $statement= $conexion->prepare($sql);
                $statement->bind_param('i',$cat_id);
                $statement->execute();
                
                $statement->close();



                $conexion->close();
                $respuesta["exito"]=1;
                $respuesta["mensaje"]=MSG_CONSULTA_EXITO;
        }
     } catch (Exception $e) {
      $respuesta["exito"]=0;
      $respuesta["mensaje"]=MSG_ERROR_CONSULTA;
    }
    return $respuesta;    
}

function listar($token, $SoloHabilitados,$NOVencidos)
{
     try{

        
       
         
        $respuesta= array();

        $conexion= crearConexion();
        if ($conexion!=null)
        {
            $sql ='call listaAnuncios (?,?,?)';
                
                $statement= $conexion->prepare($sql);
                $statement->bind_param('sii',$token,$SoloHabilitados,$NOVencidos);
                $statement->execute();
                $data = $statement->get_result();
                $statement->close();
               $conexion->close();
            
              
        $respuesta["anuncios"]=array();
        while($row=mysqli_fetch_array($data)){
           array_push($respuesta["anuncios"],$row);
           
        }

                $respuesta["exito"]=1;
                $respuesta["mensaje"]=MSG_CONSULTA_EXITO;
        }
     } catch (Exception $e) {
      $respuesta["exito"]=0;
      $respuesta["mensaje"]=MSG_ERROR_CONSULTA;
    }
    return $respuesta;    
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && empty($_POST))
{
//  echo "post:";

    $_POST = json_decode(file_get_contents('php://input'), true);
  //    print_r   ($_POST);
}


$operacion=isset($_POST['operacion'])?$_POST['operacion']:null;
if ($operacion!=null)
{
    if ($operacion=='guardar')
    {
      $r=guardar();
       
    }
    if ($operacion=='modificar')
    {
      $r=modificar();
    }

     if ($operacion=='eliminar')
    {
      $r=eliminar();
    }
    
     if ($operacion=='listar')
    {
         $token=isset($_POST['token'])?$_POST['token']:'';
         $SoloHabilitados=isset($_POST['habilitados'])?$_POST['habilitados']:1;
         $NOVencidos=isset($_POST['NOVencidos'])?$_POST['NOVencidos']:0;
      $r=listar($token, $SoloHabilitados,$NOVencidos);
    }
}


echo json_encode($r);


?>
