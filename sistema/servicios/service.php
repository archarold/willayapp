<?php
 include "servicios.php";
  $_POST = json_decode(file_get_contents('php://input'), true);
 $metodo= isset( $_POST['metodo'])? $_POST['metodo']:'no';
 $token= isset($_POST['token'])?$_POST['token']:'';

//print_r( $_POST);
switch ($metodo) {
    case 'listaCategorias':
         $lista= listaCategorias();
         echo json_encode($lista);
         break;
      case 'listaSubCategorias':
        $categoria_id=isset($_POST['categoria_id'])?$_POST['categoria_id']:'';
         $lista= listaSubCategorias($categoria_id);
         echo json_encode($lista);
         break;
        
        case 'listaDepartamentos':
        $departamento_id=isset($_POST['departamento_id'])?$_POST['departamento_id']:'';
        $listaDepartamentos= listaDepartamentos($departamento_id);
        echo json_encode($listaDepartamentos);        
        break;
        
        case 'listaProvincias':
        $provincia_id=isset($_POST['provincia_id'])?$_POST['provincia_id']:'';
        $departamento_id=isset($_POST['departamento_id'])?$_POST['departamento_id']:'';
        $listaProvincias= listaProvincias($departamento_id,$provincia_id);
        echo json_encode($listaProvincias);        
        break;
        
         case 'listaDistritos':
        $provincia_id=isset($_POST['provincia_id'])?$_POST['provincia_id']:'';
        $distrito_id=isset($_POST['distrito_id'])?$_POST['distrito_id']:'';
        $listaDistritos= listaDistritos($provincia_id,$distrito_id);
        echo json_encode($listaDistritos);        
        break;
        
        
    case 'iniciarSesion':
        $usuario=isset($_POST['usuario'])?$_POST['usuario']:'';
        $clave=isset($_POST['clave'])?$_POST['clave']:'';
        $sesionUsuario= iniciarSesion($usuario,$clave);
        echo json_encode($sesionUsuario);        
        break;
}

?>
