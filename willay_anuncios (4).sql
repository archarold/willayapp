-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-12-2018 a las 16:52:55
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `willay_anuncios`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`willay`@`localhost` PROCEDURE `anunciosDeCategoria` (`pCategoriaId` INT)  BEGIN
CREATE TEMPORARY TABLE IF NOT EXISTS anuncioTmpC ( id int, titulo varchar(80), descripcion varchar(500),destacado tinyint,fecha_publicacion DATETIME,fecha_caducidad DATETIME,
latitud DOUBLE,longitud DOUBLE,subcategoria_id INT,categoria_id INT,provincia_id INT,departamento_id int,keysword VARCHAR(200),nombreContacto VARCHAR(50), telefonoContacto1 VARCHAR(11), telefonoContacto2 VARCHAR(11),
 url_imagen1 VARCHAR(100), url_imagen2  VARCHAR(100), url_imagen3  VARCHAR(100), url_imagen4   VARCHAR(100),distancia DOUBLE );
   insert into anuncioTmpC SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`,NULL FROM anuncio_busqueda  WHERE categoria_id=pCategoriaId; 
update anuncio as a inner join anuncioTmpC as t on a.id =t.id  set a.conteoListado=a.conteoListado+1;
select * from  anuncioTmpC;
END$$

CREATE DEFINER=`willay`@`localhost` PROCEDURE `buscarAnuncio` (`pUsuario_id` INT, `pLatitud` DOUBLE, `pLongitud` DOUBLE, `pCategoria_id` INT, `pPalabra` VARCHAR(255))  BEGIN
CREATE TEMPORARY TABLE IF NOT EXISTS anuncioTmp ( id int, titulo varchar(80), descripcion varchar(500),destacado tinyint,fecha_publicacion DATETIME,fecha_caducidad DATETIME,
latitud DOUBLE,longitud DOUBLE,subcategoria_id INT,categoria_id INT,provincia_id INT,departamento_id int,keysword VARCHAR(200),nombreContacto VARCHAR(50), telefonoContacto1 VARCHAR(11), telefonoContacto2 VARCHAR(11),
 url_imagen1 VARCHAR(100), url_imagen2  VARCHAR(100), url_imagen3  VARCHAR(100), url_imagen4   VARCHAR(100),distancia DOUBLE );
delete from anuncioTmp;
		if (pCategoria_id is null) then 
						if (pLongitud is not null AND pLatitud is not null ) THEN 
								set @s= CONCAT(" Insert into anuncioTmp SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`, 
								SQRT(POW(",CONVERT (latitud-pLatitud,CHAR(15))," ,2) + POW(",CONVERT ( longitud-pLongitud,CHAR(15)),",2)) as 'distancia' 
								 FROM anuncio_busqueda 
								WHERE  MATCH(titulo, descripcion,keywords) AGAINST (? IN BOOLEAN MODE) order by fecha_publicacion desc, distancia desc");
						ELSE
								set @s= " Insert into anuncioTmp SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`,NULL
									 FROM anuncio_busqueda 
									WHERE  MATCH(titulo, descripcion,keywords) AGAINST (? IN BOOLEAN MODE) order by fecha_publicacion desc";
						END IF;
		ELSE
						if (pLongitud is not null AND pLatitud is not null ) THEN 
							set @s= CONCAT(" Insert into anuncioTmp SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`, 
								SQRT(POW(",CONVERT (latitud-pLatitud,CHAR(15))," ,2) + POW(",CONVERT ( longitud-pLongitud,CHAR(15)),",2)) as 'distancia'
								 FROM anuncio_busqueda 
								WHERE categoria_id=pCategoria_id and  MATCH(titulo, descripcion,keywords) AGAINST (pPalabra IN BOOLEAN MODE) order by fecha_publicacion desc, distancia desc");
						ELSE
								set @s= CONCAT("INsert into anuncioTmp SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`,null
									 FROM anuncio_busqueda 
									WHERE categoria_id=", CONVERT(pCategoria_id,CHAR(5)) ," and  MATCH(titulo, descripcion,keywords) AGAINST (? IN BOOLEAN MODE) order by fecha_publicacion desc");
						END IF;
		END IF;
set @p=pPalabra;
 PREPARE stmt2 FROM @s;
EXECUTE stmt2 USING @p;
DEALLOCATE PREPARE stmt2;
set @s=CONCAT("INSERT INTO busqueda_usuario  VALUES (null,?,NOW(),NOW(),",CONVERT(pUsuario_id,CHAR(5)) ,")");
PREPARE stmt1 FROM @s;
EXECUTE stmt1 USING @p;
DEALLOCATE PREPARE stmt1;
update anuncio as a inner join anuncioTmp as t on a.id =t.id  set a.conteoListado=a.conteoListado+1;
select * from anuncioTmp;
END$$

CREATE DEFINER=`willay`@`localhost` PROCEDURE `busquedaGeneral` (`pUsuario_id` INT, `pLatitud` DOUBLE, `pLongitud` DOUBLE, `nroDias` INT)  BEGIN
declare fechaUltimaRevision Date;
declare palabraUltimo  VARCHAR(100);
declare diferencia INT;
declare busqueda_id INT;
   select id, fechaRevision,palabra into busqueda_id, fechaUltimaRevision, palabraUltimo 
from
 busqueda_usuario where usuario_id=pUsuario_id order by fecha desc LIMIT 1;
	set diferencia= DATEDIFF(fechaUltimaRevision,NOW());
 if (diferencia >=nroDias or fechaUltimaRevision is null ) THEN
		call nroNuevoAnuncios(fechaUltimaRevision, pLatitud,pLongitud,palabraUltimo);
		update busqueda_usuario set fechaRevision=NOW() where id=busqueda_id;
end if ;

END$$

CREATE DEFINER=`willay`@`localhost` PROCEDURE `estadisticasAnuncio` (`pAnuncio_id` INT)  BEGIN
		select conteoListado,conteoCompartido,conteoVieronTelefono into @aConteoListado,@aConteoCompartido,@aConteoVieronTelefono from anuncio where id =pAnuncio_id;
 select COUNT(*) into @aContenidoFavorito  from anuncio_favorito where anuncio_id=pAnuncio_id;
select @aConteoListado as listado,@aConteoCompartido as compartido,@aConteoVieronTelefono as vieronTelefono,@aContenidoFavorito as favorito;
END$$

CREATE DEFINER=`willay`@`localhost` PROCEDURE `guardarAnuncio` (`titulo` VARCHAR(80), `descripcion` VARCHAR(500), `duracion` INT, `destacado` TINYINT, `habilitado` TINYINT, `subcategoria_id` INT, `distrito_id` INT, `usuario_id` INT, `nombreContacto` VARCHAR(100), `telefonoContacto1` VARCHAR(11), `telefonoContacto2` VARCHAR(11), `latitud` DOUBLE, `longitud` DOUBLE, `movil` TINYINT)  BEGIN
declare fechaCaducidad DATE;
 SET fechaCaducidad = ADDDATE(NOW(), INTERVAL duracion DAY);
 INSERT INTO `anuncio`(`id`, `titulo`, `descripcion`, `fecha_publicacion`, `fecha_caducidad`, `destacado`, `habilitado`, `subcategoria_id`, `distrito_id`, `usuario_id`, `ubicacion`,`nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `latitud`, `longitud`,`movil`)
            VALUES (null,titulo,descripcion,NOW(),fechaCaducidad,destacado,habilitado,subcategoria_id,distrito_id,usuario_id,'',nombreContacto,telefonoContacto1,telefonoContacto2 ,latitud,longitud,movil);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `iniciarSesion` (IN `pUsuario` VARCHAR(50), IN `pClave` VARCHAR(50))  BEGIN
	#Routine body goes here... declare aId int;
 
  select 	id into aId from administrador  where clave=MD5(pClave) and (usuario=pUsuario or correo=pUsuario) and habilitado=1;
	
	if (aId is null) then
	 select 0 as id,'clave o usuario incorrecto' as mensaje, false as hallado;
	 else
		select id,usuario,nombre as nombres, tipo,usuario_id,'12345x' as token, true as hallado,'acceso aceptado' as mensaje	from administrador where id=aId;
		end if;

END$$

CREATE DEFINER=`willay`@`localhost` PROCEDURE `nroNuevoAnuncios` (`fecha_ultimaconsulta` DATETIME, `pLatitud` DOUBLE, `pLongitud` DOUBLE, `pPalabra` VARCHAR(255))  BEGIN
CREATE TEMPORARY TABLE IF NOT EXISTS anuncioTmpNA ( id int, titulo varchar(80), descripcion varchar(500),destacado tinyint,fecha_publicacion DATETIME,fecha_caducidad DATETIME,
latitud DOUBLE,longitud DOUBLE,subcategoria_id INT,categoria_id INT,provincia_id INT,departamento_id int,keysword VARCHAR(200),nombreContacto VARCHAR(50), telefonoContacto1 VARCHAR(11), telefonoContacto2 VARCHAR(11),
 url_imagen1 VARCHAR(100), url_imagen2  VARCHAR(100), url_imagen3  VARCHAR(100), url_imagen4   VARCHAR(100),distancia DOUBLE );
delete from anuncioTmpNA;
						if (pLongitud is not null AND pLatitud is not null ) THEN 
								set @s= CONCAT("insert into anuncioTmpNA SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`, 
								SQRT(POW(",CONVERT (latitud-pLatitud,CHAR(15))," ,2) + POW(",CONVERT ( longitud-pLongitud,CHAR(15)),",2)) as 'distancia' 
								 FROM anuncio_busqueda 
								WHERE `", fecha_ultimaconsulta,"` < fecha_publicacion  and  MATCH(titulo, descripcion,keywords) AGAINST (? IN BOOLEAN MODE) order by fecha_publicacion desc, distancia desc");
						ELSE
								set @s=CONCAT( "insert into anuncioTmpNA  SELECT `anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`,NULL
									 FROM anuncio_busqueda 
									WHERE '", fecha_ultimaconsulta,"'< fecha_publicacion and   MATCH(titulo, descripcion,keywords) AGAINST (? IN BOOLEAN MODE) order by fecha_publicacion desc");
						END IF;
set @p=pPalabra;
 PREPARE stmt2 FROM @s;
EXECUTE stmt2 USING @p;
DEALLOCATE PREPARE stmt2;
select pPalabra as palabra, IFNULL(count(*),0) as nroNuevos from anuncioTmpNA;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `clave` varchar(500) DEFAULT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `habilitado` tinyint(1) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `usuario`, `correo`, `clave`, `nombre`, `tipo`, `habilitado`, `usuario_id`) VALUES
(1, 'luis', 'luis@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Luis Werner', 'super-administrador', 1, 48),
(9, 'Josant', 'manager@willayanunciosperu.com', '6981598cc19891ef7426f353aa347fdb', 'Jose Antonio', 'super-administrador', 1, 52);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncio`
--

CREATE TABLE `anuncio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(80) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fecha_publicacion` datetime DEFAULT NULL,
  `fecha_caducidad` datetime DEFAULT NULL,
  `destacado` tinyint(1) DEFAULT NULL,
  `habilitado` tinyint(1) DEFAULT NULL,
  `subcategoria_id` int(11) NOT NULL,
  `distrito_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `ubicacion` varchar(250) NOT NULL,
  `nombreContacto` varchar(45) DEFAULT NULL,
  `telefonoContacto1` varchar(15) DEFAULT NULL,
  `telefonoContacto2` varchar(15) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `movil` tinyint(1) DEFAULT NULL,
  `url_imagen1` varchar(100) DEFAULT NULL,
  `url_imagen2` varchar(100) DEFAULT NULL,
  `url_imagen3` varchar(100) DEFAULT NULL,
  `url_imagen4` varchar(100) DEFAULT NULL,
  `condicion_id` int(11) NOT NULL,
  `fecha_renovacion` datetime DEFAULT NULL,
  `conteoListado` int(11) DEFAULT '0',
  `conteoCompartido` int(11) DEFAULT '0',
  `conteoVieronTelefono` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anuncio`
--

INSERT INTO `anuncio` (`id`, `titulo`, `descripcion`, `fecha_publicacion`, `fecha_caducidad`, `destacado`, `habilitado`, `subcategoria_id`, `distrito_id`, `usuario_id`, `ubicacion`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `latitud`, `longitud`, `movil`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`, `condicion_id`, `fecha_renovacion`, `conteoListado`, `conteoCompartido`, `conteoVieronTelefono`) VALUES
(29, 'Ropa marca Quechua y Adidas', 'vendo saldo de ropa, tallas y precios disponibles, entrega inmediata, descuentos por la compra de 2 a mas prendas', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 78, NULL, 46, '', 'Ozz Zuniga', '974798405', '', -13.533891392858084, -71.97411306202412, 0, '29-1.png', '29-2.png', '29-3.png', '29-4.png', 0, NULL, 104, 0, 1),
(27, 'Dpto. en Anticresis', 'a pocas cuadras del Real Plaza en la av. collasuyo, 2 dormitorios, sala comedor, baÃ±o, lavanderi y kitchenette', '2017-02-25 20:44:00', '2017-03-07 00:00:00', 0, 1, 72, NULL, 46, '', 'Sra. Luzmila', '993026520', '', -13.524346915125166, -71.94914136081934, 0, '27-1.png', '27-2.png', '27-3.png', '27-4.png', 0, NULL, 106, 0, 4),
(28, 'Promocion 2 x1 en colchas', 'antes 80 soles la unidad, ahora 40 soles cada una, aprovecha..', '2017-03-02 20:22:00', '2017-03-12 00:00:00', 0, 1, 51, NULL, 46, '', 'Ernesto Ayme', '971190050', '', -13.535405500421536, -71.93773191422224, 0, '28-1.png', '28-2.png', '28-3.png', '', 0, NULL, 104, 0, 2),
(25, 'Hostal Totalmente Equipado', 'excelente ubicacion consta de 5 pisos, 15 habitaciones totalmente equipadas con baÃ±o privado y clientela fija.', '2017-02-25 20:44:00', '2017-03-07 00:00:00', 0, 1, 97, NULL, 46, '', 'Jhoel Quispe', '984768237', '996761077', -13.537014127290657, -71.96209408342838, 0, '25-1.png', '25-2.png', '', '', 0, NULL, 163, 1, 3),
(26, 'CURSO INTENSIVO DE IMPLANTOLOGIA ORAL', 'Realizada en la ciudad de Cochabamba â€“ Bolivia, DuraciÃ³n: 8 dÃ­as intensivos \nFecha: Del 3 al 10 de Abril del 2017\n20% teorÃ­a - 80% PrÃ¡ctica QuirÃºrgica\nModalidad: todo incluido desde el momento de arribo a nuestra ciudad ( hospedaje, alimentaciÃ³n completa, traslados en la ciudad y aeropuerto), el participantes no tendrÃ¡ ningÃºn gasto adicional\nCosto del Curso:\n1900$ cirugÃ­a de 5 Implantes\n2500$ cirugÃ­a de 10 Implantes\nEn ambos casos el cursante realizara levantes de seno, regeneracione', '2017-02-25 20:44:00', '2017-03-07 00:00:00', 0, 1, 85, NULL, 46, '', 'Ingrid Vargas', '167477793', '', -13.536173796312987, -71.970399543643, 0, '26-1.png', '', '', '', 0, NULL, 58, 0, 0),
(30, 'Practicante Profesional', 'Cuna jardin guarderia Brillant Kids, acercarse con CV a Av. 28 de Julio primer paradero W-2-1, altura del puente, carril de bajada', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 46, NULL, 46, '', 'Alexander Marroquin', '941414720', '', -13.53757738859677, -71.96086026728153, 0, '30-1.png', '', '', '', 0, NULL, 276, 0, 1),
(31, 'Vendo Terreno en Asoc. Saylla', 'al costado de Alicorp de 120 m2, inscrito en registros publicos, solo el lote D-4', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 28, NULL, 46, '', 'Juan Carlos Salas', '957723796', '', -13.530164919618697, -71.97380091995001, 0, '31-1.png', '31-2.png', '', '', 0, NULL, 70, 0, 1),
(32, 'Zapatos de Seguridad talla 43', 'nuevos cumple requisitos de seguridad IOS, aceptas ofertas', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 78, NULL, 46, '', 'Vladimir Abel ZuÃ±iga', '958198445', '', -13.530159704079928, -71.97426829487085, 0, '32-1.png', '', '', '', 0, NULL, 104, 0, 1),
(33, 'Trabajos Integrales para comercios', 'con materiales de buena calidad y a buen costo, Drywall, melamina, instalaciones electricas y de tinas hidromasajes, estructuras metalicas, iluminacion led.', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 17, NULL, 46, '', 'Danny Quispe', '951711381', '', -13.527768693498881, -71.97568584233522, 0, '33-1.png', '', '', '', 0, NULL, 105, 0, 0),
(34, 'Personal area de ventas', 'se necesita colaborador(a) para el area de ventas con estudios tecnicos de adm. Marketing y carreras afines, experiencia en venta de servicios, interesados enviar cv a admin@argensecur.com', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 83, NULL, 46, '', 'Hiroko Nakamura', '999999999', '', -13.526238238542348, -71.9783328473568, 0, '34-1.png', '', '', '', 0, NULL, 103, 0, 2),
(35, 'Vivienda en Urb.Ttio Norte', 'con acceso vehicular, buena iluminacion de 160 m2 y area de retiro de 40m2.', '2017-03-03 20:23:00', '2017-03-13 00:00:00', 0, 1, 31, NULL, 46, '', 'Hernan Marmanillo', '941296602', '', -13.539841503057971, -71.96122437715532, 0, '35-1.png', '', '', '', 0, NULL, 71, 0, 0),
(36, 'Vendo Bicimoto -Remate', 'movilizate mejor por donde quieras de manera economica gasta hasta 5 soles por semana, muy practica.', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, 1, 81, NULL, 46, '', 'David Llaulle', '952728464', '', -13.530548913346074, -71.97503071278334, 0, '36-1.png', '', '', '', 0, NULL, 104, 0, 0),
(37, 'Remato Cocina 4 hornillas', 'tamaÃ±o regular, buen estado 7/10, marca soluz a 170 soles.', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, 1, 77, NULL, 46, '', 'Karin Melany Paez', '965359500', '', -13.536238988892219, -71.96626894176008, 0, '37-1.png', '37-2.png', '', '', 0, NULL, 104, 0, 0),
(38, 'Ab Coaster original', 'Vendo por ocasiÃ³n Ab Coaster original de Quality Semi nuevo interesados llamar al 976342717.', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, NULL, 81, 74, 48, '', 'Mapi Necochea', '976342717', NULL, -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 104, 0, 0),
(39, 'Requiere seÃ±oritas para atenciÃ³n al publico', '\"Cholos CervecerÃ­a\" local turÃ­stico ubicado en el Centro HistÃ³rico de Cusco, especializado en cervezas artesanales y restaurante, iniciando su tercer aÃ±o de funcionamiento, buen sueldo mÃ¡s propinas, excelente ambiente de trabajo y buen trato. RazÃ³n en el mismo local Calle Palacio 110 / Patio', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, NULL, 19, 74, 48, '', 'Carlos Eduardo Torres Arias', '974 742 821', '', -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 328, 1, 0),
(40, 'REQUIERE SRTAS. DE 18 A 25 AÃ‘OS', 'The Palace Club, nuestro trabajo te permitirÃ¡ laborar y a la vez dedicarte a tus actividades acadÃ©micas y/o personales, requisitos: \nmayor de 18 aÃ±os\nresponsables y puntuales\ncontar con disponibilidad para el trabajo\nhorarios flexibles (definidos por ti misma)\nlas interesadas nos pueden contactar al whatsapp 967070099, mayor informaciÃ³n con la srta. katy palomino', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, NULL, 46, 74, 48, '', 'ToÃ±o Barron', '967070099', '', -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 325, 0, 3),
(41, 'Se alquila departamento de estreno', 'en Urb. santa teresa Marcavalle cuenta con tres habitaciones,dos baÃ±os, cocina sala comedor y lavanderÃ­a interesados llamar al 987720218', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, NULL, 68, 74, 48, '', 'Romulo Andres DueÃ±as Herrera', '987720218', '', -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 169, 0, 1),
(42, 'Housekeeping para Machu Picchu', 'Se necesita SeÃ±orita para trabajar en Housekeeping en Machu Picchu con o sin experiencia, Mas informaciÃ³n al numero 940354242 con el Sr Richard.', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, NULL, 40, 74, 48, '', 'Richard Puma Condori', '940354242', '', -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 277, 0, 0),
(43, 'Ropa Exclusiva', 'Ropa exclusiva, para chicas, todas llas tallas disponinles, contactenos', '2017-03-04 20:24:00', '2017-03-14 00:00:00', 0, 1, 78, NULL, 46, '', 'Lan Michi', '999999999', '', -13.528981967308065, -71.97430551052094, 0, '43-1.png', '43-2.png', '43-3.png', '', 0, NULL, 104, 0, 0),
(44, 'Se Requiere Profesora Primaria', 'institucion de prestigio, que se encuentre de la Ugel Cusco.', '2017-03-07 20:27:00', '2017-03-17 00:00:00', 0, 1, 82, NULL, 46, '', 'Paul Condori', '084228211', '', -13.534972945809754, -71.96933001279831, 0, '44-1.png', '', '', '', 0, NULL, 101, 1, 3),
(45, 'Se Requiere Enfermera', 'Tecnica con experiencia, para institucion educativa, horario escolar.', '2017-03-07 20:27:00', '2017-03-17 00:00:00', 0, 1, 82, NULL, 46, '', 'Paul condori', '084228211', '', -13.529928916375416, -71.97530999779701, 0, '45-1.png', '', '', '', 0, NULL, 101, 0, 0),
(46, 'Anfitrionas', 'Requiero seÃ±oritas para publicidad de empresa con aÃ±os de experiencia en el mercado a medio tiempo ', '2017-03-07 20:27:00', '2017-03-17 00:00:00', 0, 1, 46, NULL, 53, '', 'Sr. Jean Paul', '925641057', '', -13.52358216685311, -71.96041401475668, 0, '', '', '', '', 0, NULL, 280, 0, 3),
(47, 'Licencias de conducir - Tramite', 'Se tramitan licencias, revalidacion, recategorizacion, etc.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 50, NULL, 46, '', 'Alex Ttito', '951277397', '957903804', -13.52834077797006, -71.97318937629461, 0, '47-1.png', '47-2.png', '47-3.png', '', 0, NULL, 40, 0, 0),
(48, 'Chofer Turismo', 'Se requiere chofer con categoria 2B para transporte turistico, trabajo bajo presion, se brinda seguro, buen ambiente laboral, presentar CV en la calle chaparo 279; plazo max. de 3 dias.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 42, NULL, 46, '', 'Marco Antonio Villafuerte', '989725501', '', -13.533085929344868, -71.9784827157855, 0, '48-1.png', '', '', '', 0, NULL, 345, 0, 0),
(49, 'Cachoro Labrador Retriever', 'macho de color hueso, vacunado y desparasitado.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 92, NULL, 46, '', 'Procrianzas Petshop', '986769052', '', -13.530090272209296, -71.97592590004206, 0, '49-1.png', '49-2.png', '', '', 0, NULL, 72, 0, 2),
(50, 'Cuna Jardin', 'Con la propuesta pedagogica mas innovadora del cusco, nos encontramos en magisterio', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 50, NULL, 46, '', 'Raul Vega Vasquez', '987789970', '', -13.525903460396417, -71.95304565131664, 0, '50-1.png', '50-2.png', '', '', 0, NULL, 41, 2, 1),
(51, 'Telefono Monedero', '100% rentables, para boticas, librerias, bodegas y cabinas de internet, sin recibos mensuales, informes en Av. centenario 549', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 50, NULL, 46, '', 'Manuel Polido Aller', '988985709', '', -13.536261154365075, -71.97511319071054, 0, '51-1.png', '51-2.png', '51-3.png', '', 0, NULL, 40, 0, 0),
(52, 'Departamemto en venta', 'cerca a la universidad andina y alas peruanas, en 3er piso, trato directo, consta de 2 hab., sala comedor, cocina, lavanderia, roperos empotrados y terma solar; area de 95m2 a $84,000 US; hermosa vista.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 29, NULL, 46, '', 'Luis Nicoli Segura', '987187781', '', -13.554058384591057, -71.8931445479393, 0, '52-1.png', '52-2.png', '52-3.png', '52-4.png', 0, NULL, 73, 0, 0),
(53, 'Cuadernos Stanford', 'LYLYS, La mejor oferta en cusco, estamos en la av. de la cultura1905, magisterio a una cuadra del Real Plaza San Antonio, toda tu lista en un solo lugar', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 51, NULL, 46, '', 'Raul Vega', '999999999', '', -13.529393344611664, -71.94981627166271, 0, '53-1.png', '', '', '', 0, NULL, 105, 0, 0),
(54, 'Departamento en costanera', 'alquilo varios departamentos altura del molino 2; cocina, baÃ±os y 2 habitaciones.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 68, NULL, 46, '', 'Percy Amau', '993787849', '', -13.542253905368263, -71.95990104228258, 0, '54-1763.png', '', '', '', 0, NULL, 169, 0, 1),
(55, 'Departamemto en Urb. Quispicanchis', 'Lo mejores cabados en 3er piso, excelente ilumimacion, 3 dormitorios, sala comedor, cocina, lavanderia, 2 baÃ±o uno con jacuzzi y un ambiente de estudio.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 29, NULL, 46, '', 'Jhonatan Monzon', '944259567', '983885080', -13.535116044219734, -71.9455187022686, 0, '55-1.png', '55-2.png', '55-3.png', '55-4.png', 0, NULL, 73, 0, 1),
(56, 'Terreno por Ticatica', 'vente de lotes inscritos en RRPP a 3 min del arco de ticatica -via sencca; entrega inmediata.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 28, NULL, 46, '', 'Glee Glenycita', '940846004', '', -13.511984161809785, -71.99783116579056, 0, '56-1.png', '56-2.png', '', '', 0, NULL, 70, 0, 1),
(57, 'Departamento en Huancaro', 'vendo depa en residencial huancaro de 108 m2 en 1er piso, consta de sala comedor cocina, 3 baÃ±os y una cochera.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 29, NULL, 46, '', 'Sr. Carlos', '984931209', '', -13.533779260504785, -71.97670307010412, 0, '57-1.png', '57-2.png', '57-3.png', '57-4.png', 0, NULL, 76, 0, 0),
(58, 'Habitaciones en alquiler', 'en quillabamba  se alquila minidepas y habitaciones amoblados.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 69, NULL, 46, '', 'FL Liz', '939302038', '', -12.879588099044064, -72.69635651260614, 0, '58-1.png', '58-2.png', '58-3.png', '', 0, NULL, 177, 0, 0),
(59, 'Habitacion por San Sebastian', 'se alquila hab. amoblada para srta. con baÃ±o privado y tv cable, en 4to piso en la urb. Tupac Amaru E1-11, a una cuadra de la canasta, ref. universidad andina.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 69, NULL, 46, '', 'Camucha Pareja', '950308813', '', -13.557194543465787, -71.92724950611591, 0, '59-1.png', '59-2.png', '59-3.png', '59-4.png', 0, NULL, 165, 0, 0),
(60, 'Mascarilla Negra', 'Elimina puntos negros del rostro, precio 1x5 soles y 3x10 soles.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 50, NULL, 46, '', 'Flor Pacheco', '958121501', '', -13.529984657494019, -71.9785561412573, 0, '60-1.png', '', '', '', 0, NULL, 40, 0, 0),
(61, 'Casa en la av. grau cusco', 'vendo propiedad en la av. principal de 391.41 m2 a $1750 dolares el m2.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 31, NULL, 46, '', 'Erika YÃ¡Ã±ez Holgado', '984108125', '', -13.528093689802963, -71.98048532009125, 0, '61-1.png', '', '', '', 0, NULL, 73, 0, 0),
(62, 'Camaras de seguridad', 'vendo e instalo kit de camaras de vigilancia con resolucion de 1080P.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 50, NULL, 46, '', 'GL Margarita', '978892235', '', -13.524532723158144, -71.9797058030963, 0, '62-1.png', '', '', '', 0, NULL, 40, 0, 0),
(63, 'Zapatos para damas', 'de segundo uso tallas 37/38 tal como estan en las fotos.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 78, NULL, 46, '', 'Lycan Batistuta', '993408448', '', -13.526025375764018, -71.9788283854723, 0, '63-1.png', '63-2.png', '63-3.png', '63-4.png', 0, NULL, 104, 0, 0),
(64, 'trabajo para atencion en Joyeria', 'se necesita srta. en el horario de 2pm a 10pm, con ingles intermedio y experiencia en ventas.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 46, NULL, 46, '', 'Srta. Amaris', '941470899', '', -13.527719471351968, -71.97838112711906, 0, '64-1.png', '', '', '', 0, NULL, 325, 0, 0),
(65, 'Auto en Alquiler', 'modelo Kia Picanto par servicio de taxi de forma permanente, turno noche.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 22, NULL, 46, '', 'Sra. Elyzabeth', '958306060', '', -13.558050119011401, -71.93421754986048, 0, '65-1.png', '', '', '', 0, NULL, 351, 1, 7),
(66, 'Taller de fotografia', 'ahora en cusco, reportaje de boda, inicio este 17 de Abril, duracion una semana/6 horas en los horarios de lunes, miercoles y viernes de 6 a 8 pm; costo unico 250 soles, profesor Diego Vargas.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 89, NULL, 46, '', 'Christian Vargas', '084246683', '', -13.52898979065486, -71.9694671407342, 0, '66-1.png', '', '', '', 0, NULL, 59, 0, 1),
(67, 'Vestidos de Novia', 'venta y alquiler de hermozos vestidos, novedades y diseÃ±os exclusivos a precios insuperables, haz tu pedido ya!', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 50, NULL, 46, '', 'Lucia Montesinos', '921139479', '', -13.530784590004489, -71.9767516851425, 0, '67-1.png', '67-2.png', '', '', 0, NULL, 54, 0, 0),
(68, 'Cabina de internet', 'traspaso internet, por motivos de viaje con 18 maquinas, una fotocopiadora y mostradores, clientela fija; frente a la cooperativa Quillacoop.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 49, NULL, 46, '', 'Alan Alberto Garcia', '984702763', '', -13.542287804645431, -71.90649926662445, 0, '68-1.png', '', '', '', 0, NULL, 40, 0, 0),
(69, 'Habitaciones detras de la Unsaac', 'para estudiantes en la urb. los incas a 200 soles.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 69, NULL, 46, '', 'Ramiro de los rios', '984394085', '984380604', -13.519561819386334, -71.9576969370246, 0, '69-1.png', '', '', '', 0, NULL, 163, 1, 5),
(70, 'Gatitos en Adopcion', 'Son de raza grande, 2 machos y una hembra, alguien responsable.', '2017-03-13 20:33:00', '2017-03-23 00:00:00', 0, 1, 91, NULL, 46, '', 'Tonni Aguilar', '954670892', '', -13.525889117407875, -71.97773806750774, 0, '70-1.png', '', '', '', 0, NULL, 72, 1, 1),
(71, 'Cachora en adopcion', 'Se da en adopcion hermoza cachorra de 5 meses a familia responsable con compromiso de esterelizarla', '2017-03-14 20:34:00', '2017-03-24 00:00:00', 0, 1, 91, NULL, 46, '', 'Shuny Nicole', '940857749', '', -13.54055339408291, -71.96543980389833, 0, '71-1.png', '', '', '', 0, NULL, 72, 0, 0),
(72, 'Camisetas Reductoras', 'Hot Shapers, fabricados en neotex con fibras inteligentes que aumentan tu temperatura corporal, al por mayor y menor. a S/65 soles.', '2017-03-14 20:34:00', '2017-03-24 00:00:00', 0, 1, 50, NULL, 46, '', 'Ismael Denis', '950349409', '', -13.548489987239469, -71.96011628955603, 0, '72-1.png', '72-2.png', '', '', 0, NULL, 40, 0, 0),
(73, 'trabajo en discoteca', 'se necesita seÃ±orita para discoteca zotano', '2017-03-16 20:36:00', '2017-03-26 00:00:00', 0, 1, 43, NULL, 47, '', 'luis', '940414253', '', -13.514765543822573, -71.9678732380271, 0, '', '', '', '', 0, NULL, 328, 2, 5),
(74, 'TOPOGRAFÃA Y OBRAS ', ' \"TopografÃ­a en General\" - \"Estudios de Suelos\"\n* Plano PerimÃ©trico y UbicaciÃ³n (Para TrÃ¡mites Registros PÃºblicos, Ministerio de Agricultura y AutoavalÃºo, etc).\n* Levantamiento TopogrÃ¡fico Lotes, Parcelas, Sub-divisiones, HabilitaciÃ³n Urbana.\n* Trazo y Replanteo de Obras.\n* Estudios TopogrÃ¡ficos Perfiles - Expedientes TÃ©cnicos, Etc.\n* CapacitaciÃ³n en Operador de EstaciÃ³n Total y Civil 3D.', '2017-03-17 20:37:00', '2017-03-27 00:00:00', 0, 1, 17, NULL, 66, '', 'CIATPERU ', '983021380', '084235399', -13.522640082273462, -71.96734014898539, 0, '74-1.png', '', '', '', 0, NULL, 102, 0, 0),
(75, 'moto yamah m7', 'moto yamaha aÃ±o 2016 con papeles en regla', '2017-03-18 20:38:00', '2017-03-28 00:00:00', 0, 1, 26, NULL, 47, '', 'luis', '940414255', '', -13.522640082273462, -71.96734014898539, 0, '75-1.png', '', '', '', 0, NULL, 398, 0, 7),
(76, 'moto ktm 200', 'moto aÃ±o 2016 7000 km recorrido', '2017-03-20 18:40:00', '2017-03-30 00:00:00', 0, 1, 26, NULL, 47, '', 'luis', '940414282', '', -13.522640082273462, -71.96734014898539, 0, '', '', '', '', 0, NULL, 372, 0, 1),
(77, 'compro moto yamaha mt 07', 'compro moto mt 07 yamaha con mÃ¡ximo 10 000 km recorrido', '2017-03-20 18:40:00', '2017-03-30 00:00:00', 0, 1, 26, NULL, 47, '', 'luis', '940414282', '', -13.522640082273462, -71.96734014898539, 0, '', '', '', '', 0, NULL, 371, 0, 0),
(78, 'vendo moto pulsar 200', 'papeles en regla 25 000 km de recorrido', '2017-03-20 19:45:33', '2017-03-30 00:00:00', 0, 1, 26, NULL, 47, '', 'luis', '940414253', '', -13.522640082273462, -71.96734014898539, 0, '', '', '', '', 0, NULL, 369, 1, 3),
(79, 'Camara Espia', 'Tipo lapicero y llavero en remate, solo quedan 2 de cada uno; lapicero 100 soles y llavero a 80 soles.', '2017-03-21 23:47:01', '2017-03-31 00:00:00', 0, 1, 81, NULL, 46, '', 'Hiroko Nakamura', '999999999', '', -13.532141600581715, -71.97281487286092, 0, '79-1.png', '', '', '', 0, NULL, 103, 0, 0),
(80, 'Taller de Danza', 'inscribete ya, marinera, danza noderna, salsa y bachata', '2017-03-22 01:07:04', '2017-04-01 00:00:00', 0, 1, 89, NULL, 46, '', 'Thamy Flores', '984003320', '', -13.5372585985074, -71.90077476203442, 0, '80-1.png', '80-2.png', '80-3.png', '', 0, NULL, 58, 0, 0),
(81, 'Moto Pulsar', 'vendo moto pulsar con detalles, precio negociable.', '2017-03-22 01:10:47', '2017-04-01 00:00:00', 0, 1, 26, NULL, 46, '', 'Antony Gonzales', '958164342', '', -13.574191400045743, -71.95036411285399, 0, '81-1.png', '', '', '', 0, NULL, 367, 0, 7),
(82, 'Casa en Mariscal Gamarra', 'Por motivos de viaje vendo casa en mariscal gamarra, cerca a la av. principal, documentos en regla, a $195 US dolares.', '2017-03-24 10:04:20', '2017-04-03 00:00:00', 0, 1, 31, NULL, 46, '', 'Carlos ivan Campos', '984340160', '974783276', -13.527237680529128, -71.96206759661436, 0, '82-1.png', '82-2.png', '82-3.png', '', 0, NULL, 65, 0, 0),
(83, 'Alquilo amplio sÃ³tano', '140m2 ideal para eventos, conferencias,  almacÃ©n', '2017-03-25 14:11:54', '2017-04-04 00:00:00', 0, 1, 66, NULL, 72, '', 'Dra.Rosa Uribe', '993484394', '', -13.526019182204061, -71.96641813963652, 0, '83-1244.png', '', '', '', 0, NULL, 157, 2, 2),
(84, 'Cocinero, Bartender, Mozos y Azafatas', 'Convoca talentos en rubro gastronomico, sin importar edad ni experiencia para inscripcion en agencia de empleos espezializada en el area de A&B (alimentos y bebidas). interesados en CV joseph@huiza.com', '2017-03-27 01:19:33', '2017-04-06 00:00:00', 0, 1, 19, NULL, 46, '', 'Diana Ochoa', '986289868', '', -13.528313070206833, -71.9753985106945, 0, '84-1.png', '', '', '', 0, NULL, 258, 0, 1),
(85, 'Traspaso Negocio', 'Por ocasion traspaso negocio en zona centrica, calle belen 306 con clientela fija por motivo de viaje', '2017-03-27 01:30:51', '2017-04-06 00:00:00', 0, 1, 49, NULL, 46, '', 'Maria Surco', '936528433', '', -13.536619387235865, -71.98027946054935, 0, '85-1607.png', '', '', '', 0, NULL, 30, 0, 0),
(86, 'Sansung Galaxy S5', 'vendo S5 con detalle libre operador imei original, sensor de huellas.', '2017-03-27 01:55:26', '2017-04-06 00:00:00', 0, 1, 3, NULL, 46, '', 'Jerri Vizcarra', '974321068', '', -13.538947724771766, -71.97334963828325, 0, '86-1.png', '', '', '', 0, NULL, 124, 0, 3),
(87, 'Se tramita Licencia para Moto', 'Rapido y facil, de un diia para el otro, copia del dni y foto tamaÃ±o pasaporte.', '2017-03-27 02:00:11', '2017-04-06 00:00:00', 0, 1, 50, NULL, 46, '', 'Jhon Alcazar', '972132900', '', -13.523324642372621, -71.977828592062, 0, '87-1.png', '', '', '', 0, NULL, 74, 0, 1),
(88, 'Gatito de 4 meses', 'foy en adopcion gatito jugueton y tierno, por motivo de salud de mi niÃ±a.', '2017-03-27 02:02:53', '2017-04-06 00:00:00', 0, 1, 91, NULL, 46, '', 'Rosa Oros', '976322526', '', -13.524059727336438, -71.97781082242727, 0, '88-1.png', '', '', '', 0, NULL, 66, 0, 1),
(89, 'Colores triangulares marca Artesco', 'Solo por pocos dias, colores triangulares grandes y temperas de 1/4.', '2017-03-27 02:07:04', '2017-04-06 00:00:00', 0, 1, 51, NULL, 46, '', 'Romani Jesus', '924773253', '', -13.523380711017392, -71.97552423924208, 0, '89-1.png', '', '', '', 0, NULL, 97, 0, 0),
(90, 'Mesa de estudio', 'Ideal para niÃ±os de primaria, estado 8/10  S/60 soles', '2017-03-27 02:09:32', '2017-04-06 00:00:00', 0, 1, 76, NULL, 46, '', 'Cinthya Arestegui', '940192002', '', -13.526206292838896, -71.97813536971807, 0, '90-1.png', '', '', '', 0, NULL, 95, 0, 3),
(91, 'Ceramicas nuevas', 'vendo ceramicas para cocinar al carbon.', '2017-03-27 02:15:10', '2017-04-06 00:00:00', 0, 1, 51, NULL, 46, '', 'Dyn Cuba Cuenca', '949610588', '', -13.531127184564918, -71.9738807156682, 0, '91-1.png', '91-2.png', '', '', 0, NULL, 97, 0, 0),
(92, 'Handsfree Bluetooth', 'marca sansung original a S/65 soles', '2017-03-27 02:18:05', '2017-04-06 00:00:00', 0, 1, 1, NULL, 46, '', 'Cinthya Arestegui', '940192002', '', -13.523519252670281, -71.97634298354387, 0, '92-1.png', '92-2.png', '', '', 0, NULL, 124, 0, 0),
(93, 'Departamento amoblado', 'Doy en anticresis en la urb. magisterial cuenta con 3 hab., 2 baÃ±os sala comedor, cocina y lavanderia de 120 m2 por 3 aÃ±os.', '2017-03-27 02:23:30', '2017-04-06 00:00:00', 0, 1, 72, NULL, 46, '', 'Ryo Ojeda', '957711930', '', -13.52703199010423, -71.94814290851355, 0, '93-1.png', '93-2.png', '', '', 0, NULL, 83, 0, 0),
(94, 'Vendo mesas y sillas', 'ambas de madera eucalipto, estado 9.5/10 a S/90 y S/60 soles respectivamente.', '2017-03-27 02:27:15', '2017-04-06 00:00:00', 0, 1, 76, NULL, 46, '', 'Cinthya Arestegui', '940192002', '', -13.53703759653836, -71.95936292409897, 0, '94-1.png', '94-2.png', '', '', 0, NULL, 95, 3, 4),
(95, 'Estimulacion Temprana Guarderia', 'estudia con nosotros una carrera tecnica, corta y lucrativa, dirigido a profecionales de la salud, docentes , psicologos y publico en general, forma tu propia guarderia.', '2017-03-27 02:35:48', '2017-04-06 00:00:00', 0, 1, 85, NULL, 46, '', 'Carmen Guzman', '972726794', '', -13.539058876812922, -71.97072442620993, 0, '95-1.png', '95-2.png', '95-3.png', '', 0, NULL, 56, 0, 0),
(96, 'Acuario Completo y Peces', 'Acuario de 50cm, 33cm y 26cm de alto, con agular de soporte, envios a todo el pais, Acuario Arrecife Cusco.', '2017-03-27 02:40:34', '2017-04-06 00:00:00', 0, 1, 53, NULL, 46, '', 'Leo Maresco', '933157134', '', -13.53571777372198, -71.95508647710085, 0, '96-1.png', '', '', '', 0, NULL, 97, 0, 0),
(97, 'Multi usos Just a Trim', 'vendo a tan solo S/45 soles, precio real (80).', '2017-03-27 02:46:05', '2017-04-06 00:00:00', 0, 1, 51, NULL, 46, '', 'Erika Ayrampo', '941543164', '', -13.531332871452387, -71.96884620934725, 0, '97-1.png', '97-2.png', '', '', 0, NULL, 97, 0, 0),
(98, 'Vendo Huawei P9 leica', 'con camara dual, 3 de ram y 32 de almacenamiento, estado 10/10.', '2017-03-27 02:49:35', '2017-04-06 00:00:00', 0, 1, 2, NULL, 46, '', 'Elvira Zuniga Bonet', '974632105', '', -13.523400269843863, -71.98407210409641, 0, '98-1.png', '', '', '', 0, NULL, 124, 0, 1),
(99, 'Terreno en San Jeronimo', 'de 150 m2 a 2 cuadras del mercado Vino Canchon y la plaza de San Jeronimo con documentos en regla y todos los servicios basicos.', '2017-03-27 10:17:53', '2017-04-06 00:00:00', 0, 1, 28, NULL, 46, '', 'Marcos Rueda', '957537444', '984012009', -13.543183852056744, -71.95397570729256, 0, '99-1.png', '99-2.png', '99-3.png', '', 0, NULL, 61, 0, 1),
(100, 'Taller de Cocteleria', 'Especializate en cocteleria clasica e internacional, emprende tu propio negocio, inscripciones abiertas en av. Manantiales #164, consulta tambien por turnos de fin de semana.', '2017-03-27 10:25:29', '2017-04-06 00:00:00', 0, 1, 85, NULL, 46, '', 'Univeridad Global', '987720228', '', -13.533595089519169, -71.94799806922674, 0, '100-1.png', '', '', '', 0, NULL, 56, 0, 0),
(101, 'Sansung Galaxy J7', 'Estado 9.5/10, liberado, doy con audifonos y cable usb a S/620 soles negociable', '2017-03-27 11:30:42', '2017-04-06 00:00:00', 0, 1, 3, NULL, 46, '', 'Jhonhcito RG', '939568756', '', -13.553556438879639, -71.94952491670847, 0, '101-1.png', '', '', '', 0, NULL, 124, 0, 1),
(102, 'Escuela de Artes', 'ExpressArte!, aprovecha el 2x1 en salsa y bachata ademas partipa del sorteo de 5 medias becas, dale \"me gusta\" a la pagina y comparte a 5 amigos esta publicacion.', '2017-03-27 11:38:08', '2017-04-06 00:00:00', 0, 1, 89, NULL, 46, '', 'Thamy Flores', '984003320', '', -13.552514409235737, -71.91425655037165, 0, '102-1.png', '', '', '', 0, NULL, 56, 0, 0),
(103, 'Telefono Publico Rentable', 'deja buen margen de ganancias, hasta mes del 300%, llamanos.', '2017-03-27 11:45:56', '2017-04-06 00:00:00', 0, 1, 50, NULL, 46, '', 'Darck Huillca', '987574106', '954340995', -13.563561255409857, -71.95562291890383, 0, '103-1.png', '', '', '', 0, NULL, 30, 0, 0),
(104, 'Chevrolet Sail 2014', 'Precio de ocasion negociable a $8800.', '2017-03-27 11:50:08', '2017-04-06 00:00:00', 0, 1, 22, NULL, 46, '', 'Jhon Dan Eduardo', '984424281', '', -13.553545356949371, -71.96180809289217, 0, '104-1.png', '104-2.png', '', '', 0, NULL, 318, 0, 0),
(105, 'Audifonos EardPods', 'Originales estado 9/10, seminuevo en caja y con recibo', '2017-03-27 11:57:10', '2017-04-06 00:00:00', 0, 1, 1, NULL, 46, '', 'Jhonhcito RG', '939568756', '', -13.547703152137862, -71.95323340594769, 0, '105-1.png', '', '', '', 0, NULL, 119, 0, 2),
(106, 'Yaris 2013 modelo2014', 'Semifull cierre centralizado, alarma, neblineros, luces led, camara de retroceso, lunas polarizadas.', '2017-03-27 12:06:56', '2017-04-06 00:00:00', 0, 1, 22, NULL, 46, '', 'Gilbert Mozon Diaz', '958069993', '', -13.54018929990083, -71.9658575579524, 0, '106-1.png', '106-2.png', '', '', 0, NULL, 318, 0, 1),
(107, 'nesecito jovenes', 'nesecito seÃ±oritas y jÃ³venes de 18 a 35 aÃ±os para trabajar en oficina en Ã¡rea de recursos humanos, manejo de archivos y recepciÃ³n de llamadas. generando 450 soles semanales. informes al 986849298/941231612 rpc', '2017-03-27 16:33:31', '2017-04-06 00:00:00', 0, 1, 39, NULL, 82, '', 'srta Gutierrez', '941231612', '986849298', -13.52759625295283, -71.94570779800415, 0, '', '', '', '', 0, NULL, 251, 2, 2),
(108, 'Alquilo oficinas', 'Ubicados en Av. Huayruropata entre jr. Calca y Espinar. Ideal para Centros MÃ©dicos,  oficinas financi eras. Acabados personalizables.', '2017-04-01 16:57:12', '2017-04-11 00:00:00', 0, 1, 66, NULL, 72, '', 'Sra. Isabel Pardo', '993484394', '', -13.527354053638549, -71.95929318666458, 0, '108-1.png', '108-2.png', '', '', 0, NULL, 135, 1, 1),
(109, 'sistema constructivos liviana safer', 'Ejecutamos proyectos en cusco y provincias para  empresas publicas y privadas en sistema drywall; cielo raso, diseÃ±os, piso laminado, division de\n ambientes, pintura y remodelacion en general. Con trayectoria de 15 aÃ±os en Brasil, Chile y Australia...', '2017-04-04 05:57:36', '2017-04-14 00:00:00', 0, 1, 38, NULL, 106, '', 'JULIO CESAR', '974272907', '931658478', -13.52980537314787, -71.93388428539038, 0, '109-1.png', '109-2.png', '109-3.png', '', 0, NULL, 79, 0, 0),
(110, 'Cachora Pastor Aleman', 'se vende cachorita pastor aleman, adjunto foto del padre.', '2017-04-09 08:09:56', '2017-04-19 00:00:00', 0, 1, 92, NULL, 46, '', 'Jhon Enciso Quispe', '960688184', '', -13.524667352396737, -71.97436418384314, 0, '110-1.png', '110-2.png', '110-3.png', '', 0, NULL, 44, 0, 0),
(111, 'INEI Convocatoria', 'Se Requiere 1340 aplicadores y orientadores.', '2017-04-09 08:15:17', '2017-04-19 00:00:00', 0, 1, 82, NULL, 46, '', 'Roman Marshall', '997951394', '', -13.559339180348854, -71.95779584348202, 0, '111-1.png', '', '', '', 0, NULL, 68, 3, 3),
(112, 'Mochilas marca Quechua', 'Vendo mochilas marca quechua de 5, 10, 20 y 30 lt; entrega inmediata, consulte precios y disponibilidad.', '2017-04-09 08:22:49', '2017-04-19 00:00:00', 0, 1, 50, NULL, 46, '', 'Ozz Zuniga', '974798405', '', -13.536786931814738, -71.9710446149111, 0, '112-1.png', '112-2.png', '112-3.png', '', 0, NULL, 26, 0, 0),
(113, 'Departamento en Larapa', 'Cuenta con 3 habitaciones, sala comedor, baÃ±o,lavanderia, terma a gas y roperos empotrados.', '2017-04-09 08:28:33', '2017-04-19 00:00:00', 0, 1, 68, NULL, 46, '', 'Victor Eego', '946736360', '', -13.545229852353707, -71.90624479204416, 0, '113-1.png', '113-2.png', '113-3.png', '113-4.png', 0, NULL, 120, 0, 0),
(114, 'Oportunidad de trabajo', 'Se requiere orientadores, aplicadores u operadores informaticos', '2017-04-09 08:35:06', '2017-04-19 00:00:00', 0, 1, 46, NULL, 46, '', 'Moni Li', '928567454', '', -13.54582764488022, -71.95981319993734, 0, '114-1.png', '', '', '', 0, NULL, 180, 0, 5),
(115, 'Mariachis Cusco', 'Mariachis Vargas, somos la mejor opcion para tus compromisos familiares.', '2017-04-16 04:09:15', '2017-04-26 00:00:00', 0, 1, 17, NULL, 46, '', 'Robertito Vargas', '992755332', '946738210', -13.565077455496265, -71.9449695199728, 0, '115-1.png', '115-2.png', '115-3.png', '', 0, NULL, 53, 0, 0),
(116, 'Alquilo casa en urbanizacion Magisterial', 'Casa en alquiler ubicado en urb. magisterial calle oswaldo baca 109 magisterio, consta de 8 ambientes 4 servicios entrada y servicios independiente ideal para negocio y vivienda trato directo con el propietario fotos y detalles www.ubikitajey.jimdo.com celular 983016636 cuscoventas@hotmail.com', '2017-04-16 16:44:27', '2017-04-26 00:00:00', 0, 1, 67, NULL, 143, '', 'Mario Aguilar', '983016636', '955531897', -13.526022441972481, -71.94821868091822, 0, '116-1.png', '', '', '', 0, NULL, 103, 0, 2),
(117, 'Restaurant en traspaso', 'traspaso local comercial & salon de eventos de 100m2 en ttio, frente al parque pukllaycancha', '2017-04-17 05:27:39', '2017-04-27 00:00:00', 0, 1, 49, NULL, 46, '', 'Vizcarra Va-rta', '953490741', '950310017', -13.54524973533539, -71.9689766317606, 0, '117-1.png', '', '', '', 0, NULL, 25, 0, 1),
(118, 'Labradores 100% puros', 'en venta 4 cachorras, interesados contactarse', '2017-04-17 05:32:34', '2017-04-27 00:00:00', 0, 1, 92, NULL, 46, '', 'Jeremy Cuba', '984702637', '', -13.529088560386018, -71.97303682565689, 0, '118-1.png', '118-2.png', '', '', 0, NULL, 38, 0, 0),
(119, 'Recepcionostas', 'si eres super pilas y tienes entre 20 -30 aÃ±os, hablas ingles y sabes computacion, enviar cv a playboxgamingcenter@gmail.com', '2017-04-17 05:38:20', '2017-04-27 00:00:00', 0, 1, 46, NULL, 46, '', 'Angela Zaltsman', '934824637', '', -13.529286099725415, -71.97848908603191, 0, '119-1.png', '', '', '', 0, NULL, 161, 0, 3),
(120, 'Maquinas de helados de crema', 'Nuevas con garantia, dispensadores de 2 sabores y uno mixto, capacidad de 9 litros, se brinda asesoramiento en la elaboracion de sus propias bases de helados.', '2017-04-19 09:12:05', '2017-04-29 00:00:00', 0, 1, 51, NULL, 46, '', 'Renato Leon Uscapi', '925219801', '', -13.552860557844376, -71.95183999836445, 0, '120-1.png', '', '', '', 0, NULL, 56, 0, 0),
(121, 'celular Samsung duos', 'vendo celular samsumg grand prime S/. 300', '2017-04-23 19:34:11', '2017-05-03 00:00:00', 0, 1, 3, NULL, 47, '', 'luis', '940414282', '', -13.533097012228385, -71.98661550879478, 0, '', '', '', '', 0, NULL, 13, 0, 1),
(122, 'Vendo Lote 160 m2', 'Ubicado en APV El Artesano (Alto Qosqo) lote C-7 San SebastiÃ¡n a $20\'000 USD', '2017-05-02 15:18:53', '2017-05-12 00:00:00', 0, 1, 28, NULL, 66, '', 'Isaias Condori ', '983021380', '', -13.535356605817558, -71.956981793046, 0, '122-1.png', '', '', '', 0, NULL, 41, 1, 0),
(123, 'Nancy Manchego en Ukukus Bar', 'El estilo propio de Nancy Manchego a travÃ©s de su voz e interpretaciÃ³n, su sentimentalismo profundo con lo que hace vibrar y despertar al corazÃ³n mas callado e insensible son el sello de su personalidad y han hecho que su canto crezca... Este sabado nos deleitara con sus mejores canciones...solo aqui...', '2017-05-02 20:24:00', '2017-05-12 00:00:00', 0, NULL, 7, 74, 48, '', 'Mady Mejia Delgado', '973 136 432', NULL, -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 43, 0, 2),
(124, 'SE REQUIERE SRTAS', 'COSTA HOME CUSCO, SOLICITA SEÃ‘ORITAS, PARA TRABAJAR LOS FINES DE SEMANA, EL PAGO ES POR HORAS, DEBEN CONTAR CON DISPONIBILIDAD INMEDIATA.', '2017-05-02 20:24:00', '2017-05-12 00:00:00', 0, NULL, 43, 74, 48, '', 'Lucho Palomino', '940800495', '', -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 134, 0, 0),
(125, 'Vendo esmaltes,perfume y rimel natural', 'Productos de belleza en esika,cyzone y lbel', '2017-05-06 20:58:34', '2017-05-16 00:00:00', 0, 1, 51, NULL, 105, '', 'Michell', '943080820', '', -13.527678398559342, -71.95853009819984, 0, '125-1.png', '125-2.png', '', '', 0, NULL, 51, 0, 0),
(126, 'VENDO PERFUME GIULIA DE CYZONE', 'Perfume de 50ml cyzone familia olfativa floral', '2017-05-06 21:11:14', '2017-05-16 00:00:00', 0, 1, 51, NULL, 105, '', 'Michell', '943080820', '', -13.530593571319999, -71.95782467722893, 0, '126-1.png', '126-2.png', '126-3.png', '', 0, NULL, 49, 0, 0),
(127, 'Vendo PERFUME GIULIA DE CYZONE', 'Perfume giulia 50 ml a SOLO 30 SOLES', '2017-05-06 21:15:20', '2017-05-16 00:00:00', 0, 1, 51, NULL, 105, '', 'Michell', '943080820', '', -13.528926877900457, -71.95738110691309, 0, '127-1.png', '127-2.png', '127-3.png', '127-4.png', 0, NULL, 48, 0, 0),
(128, 'Vendo ESMALTE DISEÃ‘ADOR DE UÃ‘AS COLOR NEGRO', 'Esmalte diseÃ±ador de uÃ±as tono negro de CYZONE  a 10 soles\nPRECIAZO', '2017-05-06 21:16:56', '2017-05-16 00:00:00', 0, 1, 51, NULL, 105, '', 'Michell', '943080820', '', -13.528926877900457, -71.95738110691309, 0, '128-1.png', '128-2.png', '128-3.png', '128-4.png', 0, NULL, 48, 0, 1),
(129, 'Vendo GEL FIJADOR PARA CEJAS Y PESTAÃ‘AS CYZONE', 'Secret lash de cyzone a solo 10 soles', '2017-05-06 21:18:59', '2017-05-16 00:00:00', 0, 1, 51, NULL, 105, '', 'Michell', '943080820', '', -13.528926877900457, -71.95738110691309, 0, '129-1.png', '129-2.png', '129-3.png', '129-4.png', 0, NULL, 45, 0, 0),
(130, 'Transporte turistico', 'servicio de calidad, conductores capacitados, puntualidad y seriedad.', '2017-05-17 23:12:52', '2017-05-27 00:00:00', 0, 1, 99, NULL, 46, '', 'Massielita Loayza Lucero', '979396716', '', -13.527402623911083, -71.97704169899225, 0, '130-1.png', '130-2.png', '130-3.png', '', 0, NULL, 2, 0, 0),
(131, 'Mariachi Aguila Negra del Cusco', 'Le ofrece serenatas maxicanas para celebrar sus cumpleaÃ±os, matrimonios,15 aÃ±os, fiestas patronales, etc.', '2017-05-17 23:19:55', '2017-05-27 00:00:00', 0, 1, 6, NULL, 46, '', 'Antonio Aguilar', '927282822', '951190805', -13.534590263078849, -71.96774952113628, 0, '131-1.png', '', '', '', 0, NULL, 34, 0, 0),
(132, 'CoffeeBreak', 'Brindamos servicios personalizados en coffebreak para talleres, eventos,  compromisos, cuenta con una atencion profesional y al mejor precio', '2017-05-18 00:25:40', '2017-05-28 00:00:00', 0, 1, 36, NULL, 46, '', 'Massielita Loayza', '979396716', '', -13.535502963635498, -71.97817124426363, 0, '132-1.png', '132-2.png', '', '', 0, NULL, 32, 0, 1),
(133, 'Terreno en via de evitamiento', 'Urgente, terreno en muy buena ubicacion, techado y al pie de la pista principal, tiene 189 m2 a $2000 US, conversable, preguntar por Juan Jimenez.', '2017-05-18 08:50:20', '2017-05-28 00:00:00', 0, 1, 28, NULL, 46, '', 'Cusco Mistico', '951795506', '', -13.576068964728563, -71.9094667956233, 0, '133-1.png', '133-2.png', '', '', 0, NULL, 31, 0, 0),
(134, 'GASFITERIA A DOMICILIO', 'se realiza todo tipo de trabajos a domicilio especialistas en GASFITERIA termas electrobombas tanques elevados bombas sumergibles y otros', '2017-06-12 16:14:23', '2017-06-22 00:00:00', 0, 1, 10, NULL, 169, '', 'Si. Rosales ', '921965729', '', -13.520617684662774, -71.95465799421072, 0, '', '', '', '', 0, NULL, 27, 0, 2),
(140, 'DORMITORIO PARA NIÃ‘OS CAMAROTES CAMA BOX', 'Consulta rÃ¡pida vÃ­a whatsap 999473013 maestro Juan\nFabrico toda clase de muebles en melanina 15 aÃ±os de experiencia calidad garantizada', '2018-01-22 21:03:17', '2018-02-02 00:00:00', 0, 1, 13, NULL, 226, '', 'Juan Carlos', '999473013', '999570920', -12.154413881541595, -76.97023630142212, 0, '140-1.png', '140-2.png', '140-3.png', '140-4.png', 0, NULL, 9, 0, 0),
(139, 'reposteros closets cocinas roperos de melamine', 'consultas rÃ¡pidas vÃ­a whatsap 999473013 o llamar preguntar por maestro Juan \nFabrico toda clase de muebles en melanina experto en reposteros y closets 15 aÃ±os de experiencia \nAsesoramiento y visitas completamente gratis\nprevio diseÃ±o 3D', '2018-01-18 00:08:45', '2018-01-28 00:00:00', 0, 1, 13, NULL, 226, '', 'Juan', '999473013', '999570920', -12.153656098224268, -76.97336543351412, 0, '139-1.png', '139-2.png', '139-3.png', '139-4.png', 0, NULL, 9, 0, 0),
(141, 'Maestro  en Drywall', 'Maestro en drywall, mejores acabados', '2018-12-01 20:31:00', '2018-12-11 00:00:00', 0, NULL, 38, 74, 48, '', 'Luis Angel', '940414885', '984758527', -13.536275007784575, -71.98680074885488, 0, NULL, NULL, NULL, NULL, 0, NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncio_busqueda`
--

CREATE TABLE `anuncio_busqueda` (
  `anuncio_id` int(11) NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `destacado` tinyint(1) DEFAULT NULL,
  `fecha_publicacion` datetime DEFAULT NULL,
  `fecha_caducidad` datetime DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `subcategoria_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `nombreContacto` varchar(45) DEFAULT NULL,
  `telefonoContacto1` varchar(15) DEFAULT NULL,
  `telefonoContacto2` varchar(15) DEFAULT NULL,
  `url_imagen1` varchar(20) DEFAULT NULL,
  `url_imagen2` varchar(20) DEFAULT NULL,
  `url_imagen3` varchar(20) DEFAULT NULL,
  `url_imagen4` varchar(20) DEFAULT NULL,
  `condicion_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anuncio_busqueda`
--

INSERT INTO `anuncio_busqueda` (`anuncio_id`, `titulo`, `descripcion`, `destacado`, `fecha_publicacion`, `fecha_caducidad`, `latitud`, `longitud`, `subcategoria_id`, `categoria_id`, `provincia_id`, `departamento_id`, `keywords`, `nombreContacto`, `telefonoContacto1`, `telefonoContacto2`, `url_imagen1`, `url_imagen2`, `url_imagen3`, `url_imagen4`, `condicion_id`) VALUES
(27, 'Dpto. en Anticresis', 'a pocas cuadras del Real Plaza en la av. collasuyo, 2 dormitorios, sala comedor, baÃ±o, lavanderi y kitchenette', 0, '2017-02-25 20:44:00', '2017-03-07 00:00:00', -13.524346915125166, -71.94914136081934, 72, 14, NULL, NULL, ' Departamentos', 'Sra. Luzmila', '993026520', '', '27-1.png', '27-2.png', '27-3.png', '27-4.png', NULL),
(25, 'Hostal Totalmente Equipado', 'excelente ubicacion consta de 5 pisos, 15 habitaciones totalmente equipadas con baÃ±o privado y clientela fija.', 0, '2017-02-25 20:44:00', '2017-03-07 00:00:00', -13.537014127290657, -71.96209408342838, 97, 13, NULL, NULL, ' Hoteles', 'Jhoel Quispe', '984768237', '996761077', '25-1.png', '25-2.png', '', '', NULL),
(0, 'Casa en venta', 'dfg', 0, '2017-02-11 20:30:00', '2017-02-21 00:00:00', NULL, NULL, 31, 6, NULL, NULL, ' Casas', 'Ã±luis', '940414285', '', NULL, NULL, NULL, NULL, NULL),
(26, 'CURSO INTENSIVO DE IMPLANTOLOGIA ORAL', 'Realizada en la ciudad de Cochabamba â€“ Bolivia, DuraciÃ³n: 8 dÃ­as intensivos \nFecha: Del 3 al 10 de Abril del 2017\n20% teorÃ­a - 80% PrÃ¡ctica QuirÃºrgica\nModalidad: todo incluido desde el momento de arribo a nuestra ciudad ( hospedaje, alimentaciÃ³n completa, traslados en la ciudad y aeropuerto), el participantes no tendrÃ¡ ningÃºn gasto adicional\nCosto del Curso:\n1900$ cirugÃ­a de 5 Implantes\n2500$ cirugÃ­a de 10 Implantes\nEn ambos casos el cursante realizara levantes de seno, regeneracione', 0, '2017-02-25 20:44:00', '2017-03-07 00:00:00', -13.536173796312987, -71.970399543643, 85, 17, NULL, NULL, ' Cursos', 'Ingrid Vargas', '167477793', '', '26-1.png', '', '', '', NULL),
(28, 'Promocion 2 x1 en colchas', 'antes 80 soles la unidad, ahora 40 soles cada una, aprovecha..', 0, '2017-03-02 20:22:00', '2017-03-12 00:00:00', -13.535405500421536, -71.93773191422224, 51, 11, NULL, NULL, ' Productos', 'Ernesto Ayme', '971190050', '', '28-1.png', '28-2.png', '28-3.png', '', NULL),
(29, 'Ropa marca Quechua y Adidas', 'vendo saldo de ropa, tallas y precios disponibles, entrega inmediata, descuentos por la compra de 2 a mas prendas', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.533891392858084, -71.97411306202412, 78, 15, NULL, NULL, ' Ropa y accesorios', 'Ozz Zuniga', '974798405', '', '29-1.png', '29-2.png', '29-3.png', '29-4.png', NULL),
(30, 'Practicante Profesional', 'Cuna jardin guarderia Brillant Kids, acercarse con CV a Av. 28 de Julio primer paradero W-2-1, altura del puente, carril de bajada', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.53757738859677, -71.96086026728153, 46, 4, NULL, NULL, ' Otros', 'Alexander Marroquin', '941414720', '', '30-1.png', '', '', '', NULL),
(31, 'Vendo Terreno en Asoc. Saylla', 'al costado de Alicorp de 120 m2, inscrito en registros publicos, solo el lote D-4', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.530164919618697, -71.97380091995001, 28, 6, NULL, NULL, ' Terrenos', 'Juan Carlos Salas', '957723796', '', '31-1.png', '31-2.png', '', '', NULL),
(32, 'Zapatos de Seguridad talla 43', 'nuevos cumple requisitos de seguridad IOS, aceptas ofertas', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.530159704079928, -71.97426829487085, 78, 15, NULL, NULL, ' Ropa y accesorios', 'Vladimir Abel ZuÃ±iga', '958198445', '', '32-1.png', '', '', '', NULL),
(33, 'Trabajos Integrales para comercios', 'con materiales de buena calidad y a buen costo, Drywall, melamina, instalaciones electricas y de tinas hidromasajes, estructuras metalicas, iluminacion led.', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.527768693498881, -71.97568584233522, 17, 3, NULL, NULL, ' Otros', 'Danny Quispe', '951711381', '', '33-1.png', '', '', '', NULL),
(34, 'Personal area de ventas', 'se necesita colaborador(a) para el area de ventas con estudios tecnicos de adm. Marketing y carreras afines, experiencia en venta de servicios, interesados enviar cv a admin@argensecur.com', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.526238238542348, -71.9783328473568, 83, 16, NULL, NULL, ' Entidades Privadas', 'Hiroko Nakamura', '999999999', '', '34-1.png', '', '', '', NULL),
(35, 'Vivienda en Urb.Ttio Norte', 'con acceso vehicular, buena iluminacion de 160 m2 y area de retiro de 40m2.', 0, '2017-03-03 20:23:00', '2017-03-13 00:00:00', -13.539841503057971, -71.96122437715532, 31, 6, NULL, NULL, ' Casas', 'Hernan Marmanillo', '941296602', '', '35-1.png', '', '', '', NULL),
(36, 'Vendo Bicimoto -Remate', 'movilizate mejor por donde quieras de manera economica gasta hasta 5 soles por semana, muy practica.', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', -13.530548913346074, -71.97503071278334, 81, 15, NULL, NULL, ' Otros', 'David Llaulle', '952728464', '', '36-1.png', '', '', '', NULL),
(37, 'Remato Cocina 4 hornillas', 'tamaÃ±o regular, buen estado 7/10, marca soluz a 170 soles.', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', -13.536238988892219, -71.96626894176008, 77, 15, NULL, NULL, ' Artefactos', 'Karin Melany Paez', '965359500', '', '37-1.png', '37-2.png', '', '', NULL),
(38, 'Ab Coaster original', 'Vendo por ocasiÃ³n Ab Coaster original de Quality Semi nuevo interesados llamar al 976342717.', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', NULL, NULL, 81, 15, NULL, NULL, ' Otros', 'Mapi Necochea', '976342717', NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'Requiere seÃ±oritas para atenciÃ³n al publico', '\"Cholos CervecerÃ­a\" local turÃ­stico ubicado en el Centro HistÃ³rico de Cusco, especializado en cervezas artesanales y restaurante, iniciando su tercer aÃ±o de funcionamiento, buen sueldo mÃ¡s propinas, excelente ambiente de trabajo y buen trato. RazÃ³n en el mismo local Calle Palacio 110 / Patio', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', NULL, NULL, 19, 4, NULL, NULL, ' Restaurante', 'Carlos Eduardo Torres Arias', '974 742 821', '', NULL, NULL, NULL, NULL, NULL),
(40, 'REQUIERE SRTAS. DE 18 A 25 AÃ‘OS', 'The Palace Club, nuestro trabajo te permitirÃ¡ laborar y a la vez dedicarte a tus actividades acadÃ©micas y/o personales, requisitos: \nmayor de 18 aÃ±os\nresponsables y puntuales\ncontar con disponibilidad para el trabajo\nhorarios flexibles (definidos por ti misma)\nlas interesadas nos pueden contactar al whatsapp 967070099, mayor informaciÃ³n con la srta. katy palomino', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', NULL, NULL, 46, 4, NULL, NULL, ' Otros', 'ToÃ±o Barron', '967070099', '', NULL, NULL, NULL, NULL, NULL),
(41, 'Se alquila departamento de estreno', 'en Urb. santa teresa Marcavalle cuenta con tres habitaciones,dos baÃ±os, cocina sala comedor y lavanderÃ­a interesados llamar al 987720218', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', NULL, NULL, 68, 13, NULL, NULL, ' Departamentos', 'Romulo Andres DueÃ±as Herrera', '987720218', '', NULL, NULL, NULL, NULL, NULL),
(42, 'Housekeeping para Machu Picchu', 'Se necesita SeÃ±orita para trabajar en Housekeeping en Machu Picchu con o sin experiencia, Mas informaciÃ³n al numero 940354242 con el Sr Richard.', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', NULL, NULL, 40, 4, NULL, NULL, ' Limpieza', 'Richard Puma Condori', '940354242', '', NULL, NULL, NULL, NULL, NULL),
(43, 'Ropa Exclusiva', 'Ropa exclusiva, para chicas, todas llas tallas disponinles, contactenos', 0, '2017-03-04 20:24:00', '2017-03-14 00:00:00', -13.528981967308065, -71.97430551052094, 78, 15, NULL, NULL, ' Ropa y accesorios', 'Lan Michi', '999999999', '', '43-1.png', '43-2.png', '43-3.png', '', NULL),
(44, 'Se Requiere Profesora Primaria', 'institucion de prestigio, que se encuentre de la Ugel Cusco.', 0, '2017-03-07 20:27:00', '2017-03-17 00:00:00', -13.534972945809754, -71.96933001279831, 82, 16, NULL, NULL, ' Entidades Publicas', 'Paul Condori', '084228211', '', '44-1.png', '', '', '', NULL),
(45, 'Se Requiere Enfermera', 'Tecnica con experiencia, para institucion educativa, horario escolar.', 0, '2017-03-07 20:27:00', '2017-03-17 00:00:00', -13.529928916375416, -71.97530999779701, 82, 16, NULL, NULL, ' Entidades Publicas', 'Paul condori', '084228211', '', '45-1.png', '', '', '', NULL),
(46, 'Anfitrionas', 'Requiero seÃ±oritas para publicidad de empresa con aÃ±os de experiencia en el mercado a medio tiempo ', 0, '2017-03-07 20:27:00', '2017-03-17 00:00:00', -13.52358216685311, -71.96041401475668, 46, 4, NULL, NULL, ' Otros', 'Sr. Jean Paul', '925641057', '', '', '', '', '', NULL),
(47, 'Licencias de conducir - Tramite', 'Se tramitan licencias, revalidacion, recategorizacion, etc.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.52834077797006, -71.97318937629461, 50, 18, NULL, NULL, ' Varios', 'Alex Ttito', '951277397', '957903804', '47-1.png', '47-2.png', '47-3.png', '', NULL),
(48, 'Chofer Turismo', 'Se requiere chofer con categoria 2B para transporte turistico, trabajo bajo presion, se brinda seguro, buen ambiente laboral, presentar CV en la calle chaparo 279; plazo max. de 3 dias.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.533085929344868, -71.9784827157855, 42, 4, NULL, NULL, ' Turismo', 'Marco Antonio Villafuerte', '989725501', '', '48-1.png', '', '', '', NULL),
(49, 'Cachoro Labrador Retriever', 'macho de color hueso, vacunado y desparasitado.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.530090272209296, -71.97592590004206, 92, 19, NULL, NULL, ' en venta', 'Procrianzas Petshop', '986769052', '', '49-1.png', '49-2.png', '', '', NULL),
(50, 'Cuna Jardin', 'Con la propuesta pedagogica mas innovadora del cusco, nos encontramos en magisterio', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.525903460396417, -71.95304565131664, 50, 18, NULL, NULL, ' Varios', 'Raul Vega Vasquez', '987789970', '', '50-1.png', '50-2.png', '', '', NULL),
(51, 'Telefono Monedero', '100% rentables, para boticas, librerias, bodegas y cabinas de internet, sin recibos mensuales, informes en Av. centenario 549', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.536261154365075, -71.97511319071054, 50, 18, NULL, NULL, ' Varios', 'Manuel Polido Aller', '988985709', '', '51-1.png', '51-2.png', '51-3.png', '', NULL),
(52, 'Departamemto en venta', 'cerca a la universidad andina y alas peruanas, en 3er piso, trato directo, consta de 2 hab., sala comedor, cocina, lavanderia, roperos empotrados y terma solar; area de 95m2 a $84,000 US; hermosa vista.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.554058384591057, -71.8931445479393, 29, 6, NULL, NULL, ' Departamentos', 'Luis Nicoli Segura', '987187781', '', '52-1.png', '52-2.png', '52-3.png', '52-4.png', NULL),
(53, 'Cuadernos Stanford', 'LYLYS, La mejor oferta en cusco, estamos en la av. de la cultura1905, magisterio a una cuadra del Real Plaza San Antonio, toda tu lista en un solo lugar', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.529393344611664, -71.94981627166271, 51, 11, NULL, NULL, ' Productos', 'Raul Vega', '999999999', '', '53-1.png', '', '', '', NULL),
(54, 'Departamento en costanera', 'alquilo varios departamentos altura del molino 2; cocina, baÃ±os y 2 habitaciones.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.542253905368263, -71.95990104228258, 68, 13, NULL, NULL, ' Departamentos', 'Percy Amau', '993787849', '', '54-1763.png', '', '', '', NULL),
(55, 'Departamemto en Urb. Quispicanchis', 'Lo mejores cabados en 3er piso, excelente ilumimacion, 3 dormitorios, sala comedor, cocina, lavanderia, 2 baÃ±o uno con jacuzzi y un ambiente de estudio.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.535116044219734, -71.9455187022686, 29, 6, NULL, NULL, ' Departamentos', 'Jhonatan Monzon', '944259567', '983885080', '55-1.png', '55-2.png', '55-3.png', '55-4.png', NULL),
(56, 'Terreno por Ticatica', 'vente de lotes inscritos en RRPP a 3 min del arco de ticatica -via sencca; entrega inmediata.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.511984161809785, -71.99783116579056, 28, 6, NULL, NULL, ' Terrenos', 'Glee Glenycita', '940846004', '', '56-1.png', '56-2.png', '', '', NULL),
(57, 'Departamento en Huancaro', 'vendo depa en residencial huancaro de 108 m2 en 1er piso, consta de sala comedor cocina, 3 baÃ±os y una cochera.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.533779260504785, -71.97670307010412, 29, 6, NULL, NULL, ' Departamentos', 'Sr. Carlos', '984931209', '', '57-1.png', '57-2.png', '57-3.png', '57-4.png', NULL),
(58, 'Habitaciones en alquiler', 'en quillabamba  se alquila minidepas y habitaciones amoblados.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -12.879588099044064, -72.69635651260614, 69, 13, NULL, NULL, ' Habitaciones', 'FL Liz', '939302038', '', '58-1.png', '58-2.png', '58-3.png', '', NULL),
(59, 'Habitacion por San Sebastian', 'se alquila hab. amoblada para srta. con baÃ±o privado y tv cable, en 4to piso en la urb. Tupac Amaru E1-11, a una cuadra de la canasta, ref. universidad andina.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.557194543465787, -71.92724950611591, 69, 13, NULL, NULL, ' Habitaciones', 'Camucha Pareja', '950308813', '', '59-1.png', '59-2.png', '59-3.png', '59-4.png', NULL),
(60, 'Mascarilla Negra', 'Elimina puntos negros del rostro, precio 1x5 soles y 3x10 soles.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.529984657494019, -71.9785561412573, 50, 18, NULL, NULL, ' Varios', 'Flor Pacheco', '958121501', '', '60-1.png', '', '', '', NULL),
(61, 'Casa en la av. grau cusco', 'vendo propiedad en la av. principal de 391.41 m2 a $1750 dolares el m2.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.528093689802963, -71.98048532009125, 31, 6, NULL, NULL, ' Casas', 'Erika YÃ¡Ã±ez Holgado', '984108125', '', '61-1.png', '', '', '', NULL),
(62, 'Camaras de seguridad', 'vendo e instalo kit de camaras de vigilancia con resolucion de 1080P.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.524532723158144, -71.9797058030963, 50, 18, NULL, NULL, ' Varios', 'GL Margarita', '978892235', '', '62-1.png', '', '', '', NULL),
(63, 'Zapatos para damas', 'de segundo uso tallas 37/38 tal como estan en las fotos.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.526025375764018, -71.9788283854723, 78, 15, NULL, NULL, ' Ropa y accesorios', 'Lycan Batistuta', '993408448', '', '63-1.png', '63-2.png', '63-3.png', '63-4.png', NULL),
(64, 'trabajo para atencion en Joyeria', 'se necesita srta. en el horario de 2pm a 10pm, con ingles intermedio y experiencia en ventas.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.527719471351968, -71.97838112711906, 46, 4, NULL, NULL, ' Otros', 'Srta. Amaris', '941470899', '', '64-1.png', '', '', '', NULL),
(65, 'Auto en Alquiler', 'modelo Kia Picanto par servicio de taxi de forma permanente, turno noche.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.558050119011401, -71.93421754986048, 22, 5, NULL, NULL, ' Autos', 'Sra. Elyzabeth', '958306060', '', '65-1.png', '', '', '', NULL),
(66, 'Taller de fotografia', 'ahora en cusco, reportaje de boda, inicio este 17 de Abril, duracion una semana/6 horas en los horarios de lunes, miercoles y viernes de 6 a 8 pm; costo unico 250 soles, profesor Diego Vargas.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.52898979065486, -71.9694671407342, 89, 17, NULL, NULL, ' Otros', 'Christian Vargas', '084246683', '', '66-1.png', '', '', '', NULL),
(67, 'Vestidos de Novia', 'venta y alquiler de hermozos vestidos, novedades y diseÃ±os exclusivos a precios insuperables, haz tu pedido ya!', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.530784590004489, -71.9767516851425, 50, 18, NULL, NULL, ' Varios', 'Lucia Montesinos', '921139479', '', '67-1.png', '67-2.png', '', '', NULL),
(68, 'Cabina de internet', 'traspaso internet, por motivos de viaje con 18 maquinas, una fotocopiadora y mostradores, clientela fija; frente a la cooperativa Quillacoop.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.542287804645431, -71.90649926662445, 49, 18, NULL, NULL, ' Traspasos', 'Alan Alberto Garcia', '984702763', '', '68-1.png', '', '', '', NULL),
(69, 'Habitaciones detras de la Unsaac', 'para estudiantes en la urb. los incas a 200 soles.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.519561819386334, -71.9576969370246, 69, 13, NULL, NULL, ' Habitaciones', 'Ramiro de los rios', '984394085', '984380604', '69-1.png', '', '', '', NULL),
(70, 'Gatitos en Adopcion', 'Son de raza grande, 2 machos y una hembra, alguien responsable.', 0, '2017-03-13 20:33:00', '2017-03-23 00:00:00', -13.525889117407875, -71.97773806750774, 91, 19, NULL, NULL, ' en adopciÃ³n', 'Tonni Aguilar', '954670892', '', '70-1.png', '', '', '', NULL),
(71, 'Cachora en adopcion', 'Se da en adopcion hermoza cachorra de 5 meses a familia responsable con compromiso de esterelizarla', 0, '2017-03-14 20:34:00', '2017-03-24 00:00:00', -13.54055339408291, -71.96543980389833, 91, 19, NULL, NULL, ' en adopciÃ³n', 'Shuny Nicole', '940857749', '', '71-1.png', '', '', '', NULL),
(72, 'Camisetas Reductoras', 'Hot Shapers, fabricados en neotex con fibras inteligentes que aumentan tu temperatura corporal, al por mayor y menor. a S/65 soles.', 0, '2017-03-14 20:34:00', '2017-03-24 00:00:00', -13.548489987239469, -71.96011628955603, 50, 18, NULL, NULL, ' Varios', 'Ismael Denis', '950349409', '', '72-1.png', '72-2.png', '', '', NULL),
(73, 'trabajo en discoteca', 'se necesita seÃ±orita para discoteca zotano', 0, '2017-03-16 20:36:00', '2017-03-26 00:00:00', -13.514765543822573, -71.9678732380271, 43, 4, NULL, NULL, ' Discotecas', 'luis', '940414253', '', '', '', '', '', NULL),
(74, 'TOPOGRAFÃA Y OBRAS ', ' \"TopografÃ­a en General\" - \"Estudios de Suelos\"\n* Plano PerimÃ©trico y UbicaciÃ³n (Para TrÃ¡mites Registros PÃºblicos, Ministerio de Agricultura y AutoavalÃºo, etc).\n* Levantamiento TopogrÃ¡fico Lotes, Parcelas, Sub-divisiones, HabilitaciÃ³n Urbana.\n* Trazo y Replanteo de Obras.\n* Estudios TopogrÃ¡ficos Perfiles - Expedientes TÃ©cnicos, Etc.\n* CapacitaciÃ³n en Operador de EstaciÃ³n Total y Civil 3D.', 0, '2017-03-17 20:37:00', '2017-03-27 00:00:00', -13.522640082273462, -71.96734014898539, 17, 3, NULL, NULL, ' Otros', 'CIATPERU ', '983021380', '084235399', '74-1.png', '', '', '', NULL),
(75, 'moto yamah m7', 'moto yamaha aÃ±o 2016 con papeles en regla', 0, '2017-03-18 20:38:00', '2017-03-28 00:00:00', -13.522640082273462, -71.96734014898539, 26, 5, NULL, NULL, ' Motos', 'luis', '940414255', '', '75-1.png', '', '', '', NULL),
(76, 'moto ktm 200', 'moto aÃ±o 2016 7000 km recorrido', 0, '2017-03-20 18:40:00', '2017-03-30 00:00:00', -13.522640082273462, -71.96734014898539, 26, 5, NULL, NULL, ' Motos', 'luis', '940414282', '', '', '', '', '', NULL),
(77, 'compro moto yamaha mt 07', 'compro moto mt 07 yamaha con mÃ¡ximo 10 000 km recorrido', 0, '2017-03-20 18:40:00', '2017-03-30 00:00:00', -13.522640082273462, -71.96734014898539, 26, 5, NULL, NULL, ' Motos', 'luis', '940414282', '', '', '', '', '', NULL),
(78, 'vendo moto pulsar 200', 'papeles en regla 25 000 km de recorrido', 0, '2017-03-20 19:45:33', '2017-03-30 00:00:00', -13.522640082273462, -71.96734014898539, 26, 5, NULL, NULL, ' Motos', 'luis', '940414253', '', '', '', '', '', NULL),
(79, 'Camara Espia', 'Tipo lapicero y llavero en remate, solo quedan 2 de cada uno; lapicero 100 soles y llavero a 80 soles.', 0, '2017-03-21 23:47:01', '2017-03-31 00:00:00', -13.532141600581715, -71.97281487286092, 81, 15, NULL, NULL, ' Otros', 'Hiroko Nakamura', '999999999', '', '79-1.png', '', '', '', NULL),
(80, 'Taller de Danza', 'inscribete ya, marinera, danza noderna, salsa y bachata', 0, '2017-03-22 01:07:04', '2017-04-01 00:00:00', -13.5372585985074, -71.90077476203442, 89, 17, NULL, NULL, ' Otros', 'Thamy Flores', '984003320', '', '80-1.png', '80-2.png', '80-3.png', '', NULL),
(81, 'Moto Pulsar', 'vendo moto pulsar con detalles, precio negociable.', 0, '2017-03-22 01:10:47', '2017-04-01 00:00:00', -13.574191400045743, -71.95036411285399, 26, 5, NULL, NULL, ' Motos', 'Antony Gonzales', '958164342', '', '81-1.png', '', '', '', NULL),
(82, 'Casa en Mariscal Gamarra', 'Por motivos de viaje vendo casa en mariscal gamarra, cerca a la av. principal, documentos en regla, a $195 US dolares.', 0, '2017-03-24 10:04:20', '2017-04-03 00:00:00', -13.527237680529128, -71.96206759661436, 31, 6, NULL, NULL, ' Casas', 'Carlos ivan Campos', '984340160', '974783276', '82-1.png', '82-2.png', '82-3.png', '', NULL),
(83, 'Alquilo amplio sÃ³tano', '140m2 ideal para eventos, conferencias,  almacÃ©n', 0, '2017-03-25 14:11:54', '2017-04-04 00:00:00', -13.526019182204061, -71.96641813963652, 66, 13, NULL, NULL, ' Oficinas', 'Dra.Rosa Uribe', '993484394', '', '83-1244.png', '', '', '', NULL),
(84, 'Cocinero, Bartender, Mozos y Azafatas', 'Convoca talentos en rubro gastronomico, sin importar edad ni experiencia para inscripcion en agencia de empleos espezializada en el area de A&B (alimentos y bebidas). interesados en CV joseph@huiza.com', 0, '2017-03-27 01:19:33', '2017-04-06 00:00:00', -13.528313070206833, -71.9753985106945, 19, 4, NULL, NULL, ' Restaurante', 'Diana Ochoa', '986289868', '', '84-1.png', '', '', '', NULL),
(85, 'Traspaso Negocio', 'Por ocasion traspaso negocio en zona centrica, calle belen 306 con clientela fija por motivo de viaje', 0, '2017-03-27 01:30:51', '2017-04-06 00:00:00', -13.536619387235865, -71.98027946054935, 49, 18, NULL, NULL, ' Traspasos', 'Maria Surco', '936528433', '', '85-1607.png', '', '', '', NULL),
(86, 'Sansung Galaxy S5', 'vendo S5 con detalle libre operador imei original, sensor de huellas.', 0, '2017-03-27 01:55:26', '2017-04-06 00:00:00', -13.538947724771766, -71.97334963828325, 3, 1, NULL, NULL, ' Segundo uso', 'Jerri Vizcarra', '974321068', '', '86-1.png', '', '', '', NULL),
(87, 'Se tramita Licencia para Moto', 'Rapido y facil, de un diia para el otro, copia del dni y foto tamaÃ±o pasaporte.', 0, '2017-03-27 02:00:11', '2017-04-06 00:00:00', -13.523324642372621, -71.977828592062, 50, 18, NULL, NULL, ' Varios', 'Jhon Alcazar', '972132900', '', '87-1.png', '', '', '', NULL),
(88, 'Gatito de 4 meses', 'foy en adopcion gatito jugueton y tierno, por motivo de salud de mi niÃ±a.', 0, '2017-03-27 02:02:53', '2017-04-06 00:00:00', -13.524059727336438, -71.97781082242727, 91, 19, NULL, NULL, ' en adopciÃ³n', 'Rosa Oros', '976322526', '', '88-1.png', '', '', '', NULL),
(89, 'Colores triangulares marca Artesco', 'Solo por pocos dias, colores triangulares grandes y temperas de 1/4.', 0, '2017-03-27 02:07:04', '2017-04-06 00:00:00', -13.523380711017392, -71.97552423924208, 51, 11, NULL, NULL, ' Productos', 'Romani Jesus', '924773253', '', '89-1.png', '', '', '', NULL),
(90, 'Mesa de estudio', 'Ideal para niÃ±os de primaria, estado 8/10  S/60 soles', 0, '2017-03-27 02:09:32', '2017-04-06 00:00:00', -13.526206292838896, -71.97813536971807, 76, 15, NULL, NULL, ' Muebles', 'Cinthya Arestegui', '940192002', '', '90-1.png', '', '', '', NULL),
(91, 'Ceramicas nuevas', 'vendo ceramicas para cocinar al carbon.', 0, '2017-03-27 02:15:10', '2017-04-06 00:00:00', -13.531127184564918, -71.9738807156682, 51, 11, NULL, NULL, ' Productos', 'Dyn Cuba Cuenca', '949610588', '', '91-1.png', '91-2.png', '', '', NULL),
(92, 'Handsfree Bluetooth', 'marca sansung original a S/65 soles', 0, '2017-03-27 02:18:05', '2017-04-06 00:00:00', -13.523519252670281, -71.97634298354387, 1, 1, NULL, NULL, ' Accesorios', 'Cinthya Arestegui', '940192002', '', '92-1.png', '92-2.png', '', '', NULL),
(93, 'Departamento amoblado', 'Doy en anticresis en la urb. magisterial cuenta con 3 hab., 2 baÃ±os sala comedor, cocina y lavanderia de 120 m2 por 3 aÃ±os.', 0, '2017-03-27 02:23:30', '2017-04-06 00:00:00', -13.52703199010423, -71.94814290851355, 72, 14, NULL, NULL, ' Departamentos', 'Ryo Ojeda', '957711930', '', '93-1.png', '93-2.png', '', '', NULL),
(94, 'Vendo mesas y sillas', 'ambas de madera eucalipto, estado 9.5/10 a S/90 y S/60 soles respectivamente.', 0, '2017-03-27 02:27:15', '2017-04-06 00:00:00', -13.53703759653836, -71.95936292409897, 76, 15, NULL, NULL, ' Muebles', 'Cinthya Arestegui', '940192002', '', '94-1.png', '94-2.png', '', '', NULL),
(95, 'Estimulacion Temprana Guarderia', 'estudia con nosotros una carrera tecnica, corta y lucrativa, dirigido a profecionales de la salud, docentes , psicologos y publico en general, forma tu propia guarderia.', 0, '2017-03-27 02:35:48', '2017-04-06 00:00:00', -13.539058876812922, -71.97072442620993, 85, 17, NULL, NULL, ' Cursos', 'Carmen Guzman', '972726794', '', '95-1.png', '95-2.png', '95-3.png', '', NULL),
(96, 'Acuario Completo y Peces', 'Acuario de 50cm, 33cm y 26cm de alto, con agular de soporte, envios a todo el pais, Acuario Arrecife Cusco.', 0, '2017-03-27 02:40:34', '2017-04-06 00:00:00', -13.53571777372198, -71.95508647710085, 53, 11, NULL, NULL, ' Otros', 'Leo Maresco', '933157134', '', '96-1.png', '', '', '', NULL),
(97, 'Multi usos Just a Trim', 'vendo a tan solo S/45 soles, precio real (80).', 0, '2017-03-27 02:46:05', '2017-04-06 00:00:00', -13.531332871452387, -71.96884620934725, 51, 11, NULL, NULL, ' Productos', 'Erika Ayrampo', '941543164', '', '97-1.png', '97-2.png', '', '', NULL),
(98, 'Vendo Huawei P9 leica', 'con camara dual, 3 de ram y 32 de almacenamiento, estado 10/10.', 0, '2017-03-27 02:49:35', '2017-04-06 00:00:00', -13.523400269843863, -71.98407210409641, 2, 1, NULL, NULL, ' Nuevos', 'Elvira Zuniga Bonet', '974632105', '', '98-1.png', '', '', '', NULL),
(99, 'Terreno en San Jeronimo', 'de 150 m2 a 2 cuadras del mercado Vino Canchon y la plaza de San Jeronimo con documentos en regla y todos los servicios basicos.', 0, '2017-03-27 10:17:53', '2017-04-06 00:00:00', -13.543183852056744, -71.95397570729256, 28, 6, NULL, NULL, ' Terrenos', 'Marcos Rueda', '957537444', '984012009', '99-1.png', '99-2.png', '99-3.png', '', NULL),
(100, 'Taller de Cocteleria', 'Especializate en cocteleria clasica e internacional, emprende tu propio negocio, inscripciones abiertas en av. Manantiales #164, consulta tambien por turnos de fin de semana.', 0, '2017-03-27 10:25:29', '2017-04-06 00:00:00', -13.533595089519169, -71.94799806922674, 85, 17, NULL, NULL, ' Cursos', 'Univeridad Global', '987720228', '', '100-1.png', '', '', '', NULL),
(101, 'Sansung Galaxy J7', 'Estado 9.5/10, liberado, doy con audifonos y cable usb a S/620 soles negociable', 0, '2017-03-27 11:30:42', '2017-04-06 00:00:00', -13.553556438879639, -71.94952491670847, 3, 1, NULL, NULL, ' Segundo uso', 'Jhonhcito RG', '939568756', '', '101-1.png', '', '', '', NULL),
(102, 'Escuela de Artes', 'ExpressArte!, aprovecha el 2x1 en salsa y bachata ademas partipa del sorteo de 5 medias becas, dale \"me gusta\" a la pagina y comparte a 5 amigos esta publicacion.', 0, '2017-03-27 11:38:08', '2017-04-06 00:00:00', -13.552514409235737, -71.91425655037165, 89, 17, NULL, NULL, ' Otros', 'Thamy Flores', '984003320', '', '102-1.png', '', '', '', NULL),
(103, 'Telefono Publico Rentable', 'deja buen margen de ganancias, hasta mes del 300%, llamanos.', 0, '2017-03-27 11:45:57', '2017-04-06 00:00:00', -13.563561255409857, -71.95562291890383, 50, 18, NULL, NULL, ' Varios', 'Darck Huillca', '987574106', '954340995', '103-1.png', '', '', '', NULL),
(104, 'Chevrolet Sail 2014', 'Precio de ocasion negociable a $8800.', 0, '2017-03-27 11:50:08', '2017-04-06 00:00:00', -13.553545356949371, -71.96180809289217, 22, 5, NULL, NULL, ' Autos', 'Jhon Dan Eduardo', '984424281', '', '104-1.png', '104-2.png', '', '', NULL),
(105, 'Audifonos EardPods', 'Originales estado 9/10, seminuevo en caja y con recibo', 0, '2017-03-27 11:57:10', '2017-04-06 00:00:00', -13.547703152137862, -71.95323340594769, 1, 1, NULL, NULL, ' Accesorios', 'Jhonhcito RG', '939568756', '', '105-1.png', '', '', '', NULL),
(106, 'Yaris 2013 modelo2014', 'Semifull cierre centralizado, alarma, neblineros, luces led, camara de retroceso, lunas polarizadas.', 0, '2017-03-27 12:06:56', '2017-04-06 00:00:00', -13.54018929990083, -71.9658575579524, 22, 5, NULL, NULL, ' Autos', 'Gilbert Mozon Diaz', '958069993', '', '106-1.png', '106-2.png', '', '', NULL),
(107, 'nesecito jovenes', 'nesecito seÃ±oritas y jÃ³venes de 18 a 35 aÃ±os para trabajar en oficina en Ã¡rea de recursos humanos, manejo de archivos y recepciÃ³n de llamadas. generando 450 soles semanales. informes al 986849298/941231612 rpc', 0, '2017-03-27 16:33:31', '2017-04-06 00:00:00', -13.52759625295283, -71.94570779800415, 39, 4, NULL, NULL, ' Oficina', 'srta Gutierrez', '941231612', '986849298', '', '', '', '', NULL),
(108, 'Alquilo oficinas', 'Ubicados en Av. Huayruropata entre jr. Calca y Espinar. Ideal para Centros MÃ©dicos,  oficinas financi eras. Acabados personalizables.', 0, '2017-04-01 16:57:12', '2017-04-11 00:00:00', -13.527354053638549, -71.95929318666458, 66, 13, NULL, NULL, ' Oficinas', 'Sra. Isabel Pardo', '993484394', '', '108-1.png', '108-2.png', '', '', NULL),
(109, 'sistema constructivos liviana safer', 'Ejecutamos proyectos en cusco y provincias para  empresas publicas y privadas en sistema drywall; cielo raso, diseÃ±os, piso laminado, division de\n ambientes, pintura y remodelacion en general. Con trayectoria de 15 aÃ±os en Brasil, Chile y Australia...', 0, '2017-04-04 05:57:37', '2017-04-14 00:00:00', -13.52980537314787, -71.93388428539038, 38, 3, NULL, NULL, ' Drywall', 'JULIO CESAR', '974272907', '931658478', '109-1.png', '109-2.png', '109-3.png', '', NULL),
(110, 'Cachora Pastor Aleman', 'se vende cachorita pastor aleman, adjunto foto del padre.', 0, '2017-04-09 08:09:56', '2017-04-19 00:00:00', -13.524667352396737, -71.97436418384314, 92, 19, NULL, NULL, ' en venta', 'Jhon Enciso Quispe', '960688184', '', '110-1.png', '110-2.png', '110-3.png', '', NULL),
(111, 'INEI Convocatoria', 'Se Requiere 1340 aplicadores y orientadores.', 0, '2017-04-09 08:15:17', '2017-04-19 00:00:00', -13.559339180348854, -71.95779584348202, 82, 16, NULL, NULL, ' Entidades Publicas', 'Roman Marshall', '997951394', '', '111-1.png', '', '', '', NULL),
(112, 'Mochilas marca Quechua', 'Vendo mochilas marca quechua de 5, 10, 20 y 30 lt; entrega inmediata, consulte precios y disponibilidad.', 0, '2017-04-09 08:22:49', '2017-04-19 00:00:00', -13.536786931814738, -71.9710446149111, 50, 18, NULL, NULL, ' Varios', 'Ozz Zuniga', '974798405', '', '112-1.png', '112-2.png', '112-3.png', '', NULL),
(113, 'Departamento en Larapa', 'Cuenta con 3 habitaciones, sala comedor, baÃ±o,lavanderia, terma a gas y roperos empotrados.', 0, '2017-04-09 08:28:33', '2017-04-19 00:00:00', -13.545229852353707, -71.90624479204416, 68, 13, NULL, NULL, ' Departamentos', 'Victor Eego', '946736360', '', '113-1.png', '113-2.png', '113-3.png', '113-4.png', NULL),
(114, 'Oportunidad de trabajo', 'Se requiere orientadores, aplicadores u operadores informaticos', 0, '2017-04-09 08:35:06', '2017-04-19 00:00:00', -13.54582764488022, -71.95981319993734, 46, 4, NULL, NULL, ' Otros', 'Moni Li', '928567454', '', '114-1.png', '', '', '', NULL),
(115, 'Mariachis Cusco', 'Mariachis Vargas, somos la mejor opcion para tus compromisos familiares.', 0, '2017-04-16 04:09:15', '2017-04-26 00:00:00', -13.565077455496265, -71.9449695199728, 17, 3, NULL, NULL, ' Otros', 'Robertito Vargas', '992755332', '946738210', '115-1.png', '115-2.png', '115-3.png', '', NULL),
(116, 'Alquilo casa en urbanizacion Magisterial', 'Casa en alquiler ubicado en urb. magisterial calle oswaldo baca 109 magisterio, consta de 8 ambientes 4 servicios entrada y servicios independiente ideal para negocio y vivienda trato directo con el propietario fotos y detalles www.ubikitajey.jimdo.com celular 983016636 cuscoventas@hotmail.com', 0, '2017-04-16 16:44:27', '2017-04-26 00:00:00', -13.526022441972481, -71.94821868091822, 67, 13, NULL, NULL, ' Casas', 'Mario Aguilar', '983016636', '955531897', '116-1.png', '', '', '', NULL),
(117, 'Restaurant en traspaso', 'traspaso local comercial & salon de eventos de 100m2 en ttio, frente al parque pukllaycancha', 0, '2017-04-17 05:27:39', '2017-04-27 00:00:00', -13.54524973533539, -71.9689766317606, 49, 18, NULL, NULL, ' Traspasos', 'Vizcarra Va-rta', '953490741', '950310017', '117-1.png', '', '', '', NULL),
(118, 'Labradores 100% puros', 'en venta 4 cachorras, interesados contactarse', 0, '2017-04-17 05:32:34', '2017-04-27 00:00:00', -13.529088560386018, -71.97303682565689, 92, 19, NULL, NULL, ' en venta', 'Jeremy Cuba', '984702637', '', '118-1.png', '118-2.png', '', '', NULL),
(119, 'Recepcionostas', 'si eres super pilas y tienes entre 20 -30 aÃ±os, hablas ingles y sabes computacion, enviar cv a playboxgamingcenter@gmail.com', 0, '2017-04-17 05:38:20', '2017-04-27 00:00:00', -13.529286099725415, -71.97848908603191, 46, 4, NULL, NULL, ' Otros', 'Angela Zaltsman', '934824637', '', '119-1.png', '', '', '', NULL),
(120, 'Maquinas de helados de crema', 'Nuevas con garantia, dispensadores de 2 sabores y uno mixto, capacidad de 9 litros, se brinda asesoramiento en la elaboracion de sus propias bases de helados.', 0, '2017-04-19 09:12:06', '2017-04-29 00:00:00', -13.552860557844376, -71.95183999836445, 51, 11, NULL, NULL, ' Productos', 'Renato Leon Uscapi', '925219801', '', '120-1.png', '', '', '', NULL),
(121, 'celular Samsung duos', 'vendo celular samsumg grand prime S/. 300', 0, '2017-04-23 19:34:11', '2017-05-03 00:00:00', -13.533097012228385, -71.98661550879478, 3, 1, NULL, NULL, ' Segundo uso', 'luis', '940414282', '', '', '', '', '', NULL),
(122, 'Vendo Lote 160 m2', 'Ubicado en APV El Artesano (Alto Qosqo) lote C-7 San SebastiÃ¡n a $20\'000 USD', 0, '2017-05-02 15:18:54', '2017-05-12 00:00:00', -13.535356605817558, -71.956981793046, 28, 6, NULL, NULL, ' Terrenos', 'Isaias Condori ', '983021380', '', '122-1.png', '', '', '', NULL),
(123, 'Nancy Manchego en Ukukus Bar', 'El estilo propio de Nancy Manchego a travÃ©s de su voz e interpretaciÃ³n, su sentimentalismo profundo con lo que hace vibrar y despertar al corazÃ³n mas callado e insensible son el sello de su personalidad y han hecho que su canto crezca... Este sabado nos deleitara con sus mejores canciones...solo aqui...', 0, '2017-05-02 20:24:00', '2017-05-12 00:00:00', NULL, NULL, 7, 2, NULL, NULL, ' Artisticos', 'Mady Mejia Delgado', '973 136 432', NULL, NULL, NULL, NULL, NULL, NULL),
(124, 'SE REQUIERE SRTAS', 'COSTA HOME CUSCO, SOLICITA SEÃ‘ORITAS, PARA TRABAJAR LOS FINES DE SEMANA, EL PAGO ES POR HORAS, DEBEN CONTAR CON DISPONIBILIDAD INMEDIATA.', 0, '2017-05-02 20:24:00', '2017-05-12 00:00:00', NULL, NULL, 43, 4, NULL, NULL, ' Discotecas', 'Lucho Palomino', '940800495', '', NULL, NULL, NULL, NULL, NULL),
(125, 'Vendo esmaltes,perfume y rimel natural', 'Productos de belleza en esika,cyzone y lbel', 0, '2017-05-06 20:58:34', '2017-05-16 00:00:00', -13.527678398559342, -71.95853009819984, 51, 11, NULL, NULL, ' Productos', 'Michell', '943080820', '', '125-1.png', '125-2.png', '', '', NULL),
(126, 'VENDO PERFUME GIULIA DE CYZONE', 'Perfume de 50ml cyzone familia olfativa floral', 0, '2017-05-06 21:11:14', '2017-05-16 00:00:00', -13.530593571319999, -71.95782467722893, 51, 11, NULL, NULL, ' Productos', 'Michell', '943080820', '', '126-1.png', '126-2.png', '126-3.png', '', NULL),
(127, 'Vendo PERFUME GIULIA DE CYZONE', 'Perfume giulia 50 ml a SOLO 30 SOLES', 0, '2017-05-06 21:15:20', '2017-05-16 00:00:00', -13.528926877900457, -71.95738110691309, 51, 11, NULL, NULL, ' Productos', 'Michell', '943080820', '', '127-1.png', '127-2.png', '127-3.png', '127-4.png', NULL),
(128, 'Vendo ESMALTE DISEÃ‘ADOR DE UÃ‘AS COLOR NEGRO', 'Esmalte diseÃ±ador de uÃ±as tono negro de CYZONE  a 10 soles\nPRECIAZO', 0, '2017-05-06 21:16:56', '2017-05-16 00:00:00', -13.528926877900457, -71.95738110691309, 51, 11, NULL, NULL, ' Productos', 'Michell', '943080820', '', '128-1.png', '128-2.png', '128-3.png', '128-4.png', NULL),
(129, 'Vendo GEL FIJADOR PARA CEJAS Y PESTAÃ‘AS CYZONE', 'Secret lash de cyzone a solo 10 soles', 0, '2017-05-06 21:18:59', '2017-05-16 00:00:00', -13.528926877900457, -71.95738110691309, 51, 11, NULL, NULL, ' Productos', 'Michell', '943080820', '', '129-1.png', '129-2.png', '129-3.png', '129-4.png', NULL),
(130, 'Transporte turistico', 'servicio de calidad, conductores capacitados, puntualidad y seriedad.', 0, '2017-05-17 23:12:52', '2017-05-27 00:00:00', -13.527402623911083, -71.97704169899225, 99, 21, NULL, NULL, ' Transporte', 'Massielita Loayza Lucero', '979396716', '', '130-1.png', '130-2.png', '130-3.png', '', NULL),
(131, 'Mariachi Aguila Negra del Cusco', 'Le ofrece serenatas maxicanas para celebrar sus cumpleaÃ±os, matrimonios,15 aÃ±os, fiestas patronales, etc.', 0, '2017-05-17 23:19:55', '2017-05-27 00:00:00', -13.534590263078849, -71.96774952113628, 6, 2, NULL, NULL, ' Musicales', 'Antonio Aguilar', '927282822', '951190805', '131-1.png', '', '', '', NULL),
(132, 'CoffeeBreak', 'Brindamos servicios personalizados en coffebreak para talleres, eventos,  compromisos, cuenta con una atencion profesional y al mejor precio', 0, '2017-05-18 00:25:40', '2017-05-28 00:00:00', -13.535502963635498, -71.97817124426363, 36, 2, NULL, NULL, ' GastronÃ³micos', 'Massielita Loayza', '979396716', '', '132-1.png', '132-2.png', '', '', NULL),
(133, 'Terreno en via de evitamiento', 'Urgente, terreno en muy buena ubicacion, techado y al pie de la pista principal, tiene 189 m2 a $2000 US, conversable, preguntar por Juan Jimenez.', 0, '2017-05-18 08:50:22', '2017-05-28 00:00:00', -13.576068964728563, -71.9094667956233, 28, 6, NULL, NULL, ' Terrenos', 'Cusco Mistico', '951795506', '', '133-1.png', '133-2.png', '', '', NULL),
(134, 'GASFITERIA A DOMICILIO', 'se realiza todo tipo de trabajos a domicilio especialistas en GASFITERIA termas electrobombas tanques elevados bombas sumergibles y otros', 0, '2017-06-12 16:14:23', '2017-06-22 00:00:00', -13.520617684662774, -71.95465799421072, 10, 3, NULL, NULL, ' Gasfiteros', 'Si. Rosales ', '921965729', '', '', '', '', '', NULL),
(139, 'reposteros closets cocinas roperos de melamine', 'consultas rÃ¡pidas vÃ­a whatsap 999473013 o llamar preguntar por maestro Juan \nFabrico toda clase de muebles en melanina experto en reposteros y closets 15 aÃ±os de experiencia \nAsesoramiento y visitas completamente gratis\nprevio diseÃ±o 3D', 0, '2018-01-18 00:08:46', '2018-01-28 00:00:00', -12.153656098224268, -76.97336543351412, 13, 3, NULL, NULL, ' Melamineros', 'Juan', '999473013', '999570920', '139-1.png', '139-2.png', '139-3.png', '139-4.png', NULL),
(140, 'DORMITORIO PARA NIÃ‘OS CAMAROTES CAMA BOX', 'Consulta rÃ¡pida vÃ­a whatsap 999473013 maestro Juan\nFabrico toda clase de muebles en melanina 15 aÃ±os de experiencia calidad garantizada', 0, '2018-01-22 21:03:19', '2018-02-02 00:00:00', -12.154413881541595, -76.97023630142212, 13, 3, NULL, NULL, ' Melamineros', 'Juan Carlos', '999473013', '999570920', '140-1.png', '140-2.png', '140-3.png', '140-4.png', NULL),
(141, 'Maestro  en Drywall', 'Maestro en drywall, mejores acabados', 0, '2018-12-01 20:31:00', '2018-12-11 00:00:00', NULL, NULL, 38, 3, NULL, NULL, ' Drywall', 'Luis Angel', '940414885', '984758527', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `anuncio_favorito`
--

CREATE TABLE `anuncio_favorito` (
  `anuncio_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `anuncio_favorito`
--

INSERT INTO `anuncio_favorito` (`anuncio_id`, `usuario_id`, `fecha`) VALUES
(0, 46, '2017-03-04 00:06:08'),
(12, 46, '2017-02-06 17:32:32'),
(39, 47, '2017-03-16 17:05:39'),
(28, 58, '2017-03-07 15:36:42'),
(69, 67, '2017-03-18 15:48:21'),
(73, 47, '2017-03-19 07:57:12'),
(27, 47, '2017-03-19 13:00:52'),
(25, 47, '2017-03-20 11:40:31'),
(30, 103, '2017-04-03 14:55:40'),
(37, 187, '2017-08-28 20:24:47'),
(111, 203, '2017-11-09 04:13:46'),
(111, 204, '2017-11-09 21:55:04'),
(94, 203, '2017-11-09 04:19:31'),
(111, 205, '2017-11-16 09:24:12'),
(94, 204, '2017-11-09 21:58:25'),
(31, 237, '2018-05-04 22:21:45'),
(94, 205, '2017-11-16 09:28:34'),
(94, 237, '2018-05-04 22:20:12'),
(83, 244, '2018-11-09 16:52:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `busqueda`
--

CREATE TABLE `busqueda` (
  `id` int(11) NOT NULL,
  `key` varchar(200) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fecha_ultimaconsulta` datetime DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `busqueda`
--

INSERT INTO `busqueda` (`id`, `key`, `fecha`, `fecha_ultimaconsulta`, `categoria_id`, `usuario_id`) VALUES
(1, 'Eventos', '2017-02-22 14:49:18', NULL, 2, 53),
(2, 'Ofertas', '2017-03-07 15:36:19', NULL, 11, 58),
(3, '', '2017-03-10 12:43:25', '2017-03-10 12:43:25', 0, 53),
(6, 'auto', '2017-03-12 21:26:11', '2017-05-10 19:00:30', 0, 47),
(9, 'moto', '2017-03-16 17:50:45', '2017-05-10 19:00:30', 0, 47),
(7, 'restaurante', '2017-03-16 17:05:57', '2017-05-10 19:00:30', 0, 47),
(8, 'trabajo', '2017-03-16 17:30:05', '2017-05-10 19:00:30', 0, 47),
(10, 'trabajo', '2017-03-16 19:58:58', '2017-03-31 11:44:20', 0, 63),
(12, 'carro', '2017-03-16 20:04:01', '2017-03-31 11:44:20', 0, 63),
(13, 'Alquileres', '2017-03-20 11:38:46', '2017-03-20 11:38:46', 13, 47),
(14, 'celulres', '2017-03-27 14:01:08', '2017-03-28 11:37:28', 0, 81),
(16, 'Celulares', '2017-04-04 14:35:27', '2017-04-04 14:35:27', 1, 108),
(17, 'Servicios', '2017-04-05 00:33:52', '2017-04-05 00:33:52', 3, 111),
(18, 'Ofertas', '2017-04-09 04:03:52', '2017-04-09 04:03:52', 11, 46),
(19, 'Celulares', '2017-04-10 07:58:40', '2017-04-10 07:58:40', 1, 137),
(20, 'Ofertas', '2017-04-13 11:13:40', '2017-04-13 11:13:40', 11, 142),
(21, 'Empleos', '2017-09-02 16:55:58', '2017-09-02 16:55:58', 4, 142),
(22, 'Empleos', '2017-11-01 21:27:58', '2017-11-01 21:27:58', 4, 202),
(23, 'Anticresis', '2017-11-09 04:10:11', '2017-11-09 04:10:11', 14, 203),
(24, 'Baratillo', '2017-11-09 04:19:24', '2017-11-09 04:19:24', 15, 203),
(25, 'Anticresis', '2017-11-09 21:50:37', '2017-11-09 21:50:37', 14, 204),
(26, 'Baratillo', '2017-11-09 21:58:24', '2017-11-09 21:58:24', 15, 204),
(27, 'Anticresis', '2017-11-16 09:19:44', '2017-11-16 09:19:44', 14, 205),
(28, 'Baratillo', '2017-11-16 09:28:33', '2017-11-16 09:28:33', 15, 205),
(29, 'Inmuebles', '2018-01-17 23:58:42', '2018-01-17 23:58:42', 6, 226),
(30, 'alquiler', '2018-04-10 17:21:36', '2018-06-05 17:18:47', 0, 235),
(31, 'Anticresis', '2018-05-04 22:14:36', '2018-05-04 22:14:36', 14, 237),
(32, 'Alquileres', '2018-05-04 22:18:11', '2018-05-04 22:18:11', 13, 237),
(33, 'Baratillo', '2018-05-04 22:20:12', '2018-05-04 22:20:12', 15, 237),
(34, 'Convocatorias', '2018-11-09 16:41:46', '2018-11-09 16:41:46', 16, 244);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `busqueda_usuario`
--

CREATE TABLE `busqueda_usuario` (
  `id` int(11) NOT NULL,
  `palabra` varchar(250) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `fechaRevision` datetime DEFAULT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `busqueda_usuario`
--

INSERT INTO `busqueda_usuario` (`id`, `palabra`, `fecha`, `fechaRevision`, `usuario_id`) VALUES
(1, 'practicante', '2017-03-12 18:19:55', '2017-03-18 00:00:00', 47),
(2, 'moto', '2017-03-12 21:02:47', '2017-03-20 19:15:58', 47),
(3, 'moto', '2017-03-12 21:02:53', NULL, 47),
(4, 'trabajo', '2017-03-12 21:09:11', NULL, 47),
(5, 'auto', '2017-03-12 21:26:07', NULL, 47),
(6, 'trabajo', '2017-03-12 21:58:37', NULL, 47),
(7, 'practica', '2017-03-13 08:42:50', NULL, 47),
(8, 'practicas', '2017-03-13 08:43:05', NULL, 47),
(9, 'trabajo', '2017-03-13 08:43:20', NULL, 47),
(10, 'trabajo', '2017-03-13 08:46:44', NULL, 47),
(11, 'carro', '2017-03-16 17:03:32', NULL, 47),
(12, 'carro', '2017-03-16 17:03:36', NULL, 47),
(13, 'moto', '2017-03-16 17:03:43', NULL, 47),
(14, 'empleo', '2017-03-16 17:03:50', NULL, 47),
(15, 'hotel', '2017-03-16 17:04:03', NULL, 47),
(16, 'restaurant', '2017-03-16 17:04:14', NULL, 47),
(17, 'restaurante', '2017-03-16 17:04:19', NULL, 47),
(18, 'restaurante', '2017-03-16 17:05:36', NULL, 47),
(19, 'restaurante', '2017-03-16 17:05:52', NULL, 47),
(20, 'trabajo', '2017-03-16 17:06:09', NULL, 47),
(21, 'trabajo', '2017-03-16 17:06:15', NULL, 47),
(22, 'restaurante', '2017-03-16 17:06:24', NULL, 47),
(23, 'trabajo', '2017-03-16 17:30:02', NULL, 47),
(24, 'moto', '2017-03-16 17:50:43', NULL, 47),
(25, 'trabajo', '2017-03-16 19:58:50', NULL, 63),
(26, 'moto', '2017-03-16 20:00:14', NULL, 63),
(27, 'carro', '2017-03-16 20:03:59', NULL, 63),
(28, 'moto', '2017-03-17 10:01:06', NULL, 47),
(29, 'moto', '2017-03-17 16:41:34', NULL, 47),
(30, 'moto', '2017-03-17 16:41:40', NULL, 47),
(31, 'trabajo', '2017-03-17 16:41:52', NULL, 47),
(32, 'trabajo', '2017-03-17 16:42:07', NULL, 47),
(33, 'trabajo', '2017-03-17 16:43:13', NULL, 47),
(34, 'trabajo', '2017-03-17 16:45:14', NULL, 47),
(35, 'trabajo', '2017-03-17 16:45:46', NULL, 47),
(36, 'trabajo', '2017-03-17 16:45:56', NULL, 47),
(37, 'trabajo', '2017-03-17 16:45:59', NULL, 47),
(38, 'trabajo', '2017-03-17 16:46:06', NULL, 47),
(39, 'trabajo', '2017-03-17 17:35:34', NULL, 47),
(40, 'trabajo', '2017-03-17 17:35:40', NULL, 47),
(41, 'auto', '2017-03-17 17:35:50', NULL, 47),
(42, 'auto', '2017-03-17 17:40:23', NULL, 47),
(43, 'trabajo', '2017-03-17 17:40:31', NULL, 47),
(44, 'auto', '2017-03-17 17:45:13', NULL, 47),
(45, 'trabajo', '2017-03-17 17:45:26', NULL, 47),
(46, 'trabajo', '2017-03-17 17:45:48', NULL, 47),
(47, 'trabajo', '2017-03-17 17:51:24', NULL, 47),
(48, 'trabajo', '2017-03-17 17:51:46', NULL, 47),
(49, 'trabajo', '2017-03-17 17:51:48', NULL, 47),
(50, 'trabajo', '2017-03-17 18:03:27', NULL, 47),
(51, 'auto', '2017-03-17 18:03:46', NULL, 47),
(52, 'auto', '2017-03-17 18:14:06', NULL, 47),
(53, 'trabajo', '2017-03-17 18:15:30', NULL, 47),
(54, 'trabajo', '2017-03-17 18:17:37', NULL, 47),
(55, 'trabajo', '2017-03-17 18:25:43', NULL, 47),
(56, 'trabajo', '2017-03-17 18:39:40', NULL, 47),
(57, 'auto', '2017-03-17 18:40:08', NULL, 47),
(58, 'trabajo', '2017-03-17 18:41:31', NULL, 47),
(59, 'trabajo', '2017-03-17 18:42:57', NULL, 47),
(60, 'auto', '2017-03-18 09:22:56', NULL, 47),
(61, 'moto', '2017-03-18 14:13:10', NULL, 47),
(62, 'moto', '2017-03-18 14:13:35', NULL, 47),
(63, 'moto', '2017-03-18 14:14:21', NULL, 47),
(64, 'trabajo', '2017-03-18 16:34:21', NULL, 47),
(65, 'trabajo', '2017-03-18 16:34:33', NULL, 47),
(66, 'trabajo', '2017-03-18 16:35:56', NULL, 47),
(67, 'auto', '2017-03-18 20:04:17', NULL, 47),
(68, 'auto', '2017-03-18 22:14:35', NULL, 47),
(69, 'trabajo', '2017-03-18 22:35:55', NULL, 1),
(70, 'trabajo', '2017-03-18 22:38:49', NULL, 1),
(71, 'trabajo', '2017-03-18 22:43:12', NULL, 1),
(72, 'trabajo', '2017-03-18 23:11:01', NULL, 52),
(73, 'trabajo', '2017-03-18 23:24:32', NULL, 52),
(74, 'trabajo', '2017-03-18 23:26:09', NULL, 47),
(75, 'trabajo', '2017-03-18 23:26:13', NULL, 47),
(76, 'trabajo', '2017-03-18 23:26:19', NULL, 47),
(77, 'trabajo', '2017-03-18 23:27:06', NULL, 1),
(78, 'trabajo', '2017-03-18 23:27:31', NULL, 1),
(79, 'trabajo', '2017-03-18 23:28:38', NULL, 1),
(80, 'trabajo', '2017-03-18 23:28:51', NULL, 1),
(81, 'avion', '2017-03-18 23:33:52', NULL, 47),
(82, 'trabajo', '2017-03-18 23:34:16', NULL, 52),
(83, 'trabajo', '2017-03-19 06:23:31', NULL, 47),
(84, 'trabajo', '2017-03-19 06:28:18', NULL, 47),
(85, 'trabajo', '2017-03-19 07:49:13', NULL, 1),
(86, 'trabajo', '2017-03-19 07:49:26', NULL, 1),
(87, 'trabajo', '2017-03-19 07:56:49', NULL, 47),
(88, 'trabajo', '2017-03-19 11:02:42', NULL, 47),
(89, 'trabajo', '2017-03-19 11:02:44', NULL, 47),
(90, 'trabajo', '2017-03-19 12:15:59', NULL, 47),
(91, 'auto', '2017-03-19 18:07:02', NULL, 47),
(92, 'trabajo', '2017-03-19 19:16:51', NULL, 47),
(93, 'camiseta', '2017-03-19 21:15:25', NULL, 47),
(94, 'camiseta', '2017-03-19 21:15:29', NULL, 47),
(95, 'camiseta', '2017-03-19 21:16:55', NULL, 47),
(96, 'trabajo', '2017-03-19 21:19:14', NULL, 47),
(97, 'trabajo', '2017-03-19 21:19:31', NULL, 47),
(98, 'turismo', '2017-03-19 21:19:45', NULL, 47),
(99, 'trabajo turismo', '2017-03-20 06:10:41', NULL, 1),
(100, 'trabajo', '2017-03-20 06:11:27', NULL, 47),
(101, 'trabajo', '2017-03-20 06:11:30', NULL, 47),
(102, 'trabajo +turismo', '2017-03-20 06:11:57', NULL, 1),
(103, 'trabajo turismo', '2017-03-20 06:18:08', NULL, 12),
(104, 'trabajo', '2017-03-20 06:18:57', NULL, 47),
(105, 'trabajo +turismo', '2017-03-20 06:21:44', NULL, 1),
(106, 'discoteca +turismo', '2017-03-20 06:22:01', NULL, 1),
(107, '+discoteca +turismo', '2017-03-20 06:22:21', NULL, 1),
(108, 'discoteca +turismo', '2017-03-20 06:22:51', NULL, 1),
(109, 'turismo', '2017-03-20 06:23:08', NULL, 47),
(110, '+trabajo +turismo', '2017-03-20 06:23:24', NULL, 1),
(111, '+trabajo +cholos', '2017-03-20 06:24:05', NULL, 1),
(112, '+trabajo +publicidad', '2017-03-20 06:24:38', NULL, 1),
(113, 'trabajo +publicidad', '2017-03-20 06:26:09', NULL, 1),
(114, 'trabajo +en +publicidad', '2017-03-20 06:26:24', NULL, 1),
(115, 'trabajo+publicidad', '2017-03-20 06:47:42', NULL, 1),
(116, 'turismo', '2017-03-20 07:03:56', NULL, 47),
(117, 'trabajo', '2017-03-20 08:43:51', NULL, 47),
(118, 'trabajo', '2017-03-20 08:45:06', NULL, 47),
(119, 'trabajo en turismo', '2017-03-20 08:46:07', NULL, 1),
(120, 'trabajo', '2017-03-20 08:49:27', NULL, 77),
(121, 'trabajo turismo', '2017-03-20 08:49:33', NULL, 77),
(122, 'trabajo turismo', '2017-03-20 08:49:47', NULL, 77),
(123, 'trabajo', '2017-03-20 08:58:59', NULL, 47),
(124, 'trabajo+turismo', '2017-03-20 08:59:10', NULL, 47),
(125, 'trabajo+turismo', '2017-03-20 08:59:14', NULL, 47),
(126, 'trabajo+turismo', '2017-03-20 08:59:23', NULL, 47),
(127, 'trabajo+turismo', '2017-03-20 08:59:31', NULL, 47),
(128, 'trabajo +turismo', '2017-03-20 09:00:07', NULL, 1),
(129, 'trabajo turismo', '2017-03-20 09:00:39', NULL, 47),
(130, 'trabajo turismo', '2017-03-20 09:04:16', NULL, 47),
(131, 'trabajo +turismo', '2017-03-20 09:06:51', NULL, 47),
(132, 'trabajo turismo', '2017-03-20 09:08:35', NULL, 47),
(133, 'trabajo +turismo', '2017-03-20 09:08:51', NULL, 1),
(134, 'trabajo turismo', '2017-03-20 09:10:39', NULL, 47),
(135, 'trabajo turismo', '2017-03-20 09:21:10', NULL, 47),
(136, 'trabajo', '2017-03-20 09:26:50', NULL, 47),
(137, 'trabajo +turismo', '2017-03-20 09:26:56', NULL, 47),
(138, 'turismo', '2017-03-20 09:27:15', NULL, 47),
(139, 'discoteca', '2017-03-20 09:28:09', NULL, 47),
(140, 'moto', '2017-03-20 09:28:15', NULL, 47),
(141, 'auto', '2017-03-20 09:28:22', NULL, 47),
(142, 'alquiler', '2017-03-20 09:28:31', NULL, 47),
(143, 'alquiler +nokia', '2017-03-20 09:28:42', NULL, 47),
(144, 'alquiler', '2017-03-20 09:28:50', NULL, 47),
(145, 'alquiler +kia', '2017-03-20 09:29:00', NULL, 47),
(146, 'alquiler', '2017-03-20 09:29:39', NULL, 47),
(147, 'alquiler +kia', '2017-03-20 09:29:49', NULL, 47),
(148, 'trabajo +turismo', '2017-03-20 09:30:01', NULL, 47),
(149, 'trabajo', '2017-03-20 09:30:08', NULL, 47),
(150, 'alquiler', '2017-03-20 09:30:18', NULL, 47),
(151, 'alquiler +kia', '2017-03-20 09:30:25', NULL, 47),
(152, 'alquiler +kia', '2017-03-20 09:30:43', NULL, 1),
(153, 'alquiler +Picanto', '2017-03-20 09:31:03', NULL, 1),
(154, 'alquiler +picanto', '2017-03-20 09:31:21', NULL, 47),
(155, 'trabajo +turismo', '2017-03-20 09:31:46', NULL, 77),
(156, 'trabajo +turismo', '2017-03-20 09:33:54', NULL, 47),
(157, 'alquiler', '2017-03-20 09:34:20', NULL, 47),
(158, 'alquiler +habitacion', '2017-03-20 09:34:42', NULL, 47),
(159, 'cuaco', '2017-03-20 09:54:46', NULL, 47),
(160, 'cusco', '2017-03-20 09:54:52', NULL, 47),
(161, 'moto', '2017-03-20 10:21:58', NULL, 47),
(162, 'moto', '2017-03-20 10:26:50', NULL, 47),
(163, 'moto', '2017-03-20 10:26:54', NULL, 47),
(164, 'mot', '2017-03-20 10:28:31', NULL, 47),
(165, 'moto', '2017-03-20 10:28:34', NULL, 47),
(166, 'moto', '2017-03-20 11:18:24', NULL, 47),
(167, 'moto', '2017-03-20 11:18:27', NULL, 47),
(168, 'moto', '2017-03-20 11:18:30', NULL, 47),
(169, 'moto', '2017-03-20 11:33:33', NULL, 47),
(170, 'moto', '2017-03-20 11:33:41', NULL, 47),
(171, 'moto', '2017-03-20 11:34:15', NULL, 47),
(172, 'moto', '2017-03-20 11:34:27', NULL, 47),
(173, 'moto', '2017-03-20 11:34:59', NULL, 47),
(174, 'moto', '2017-03-20 11:35:07', NULL, 47),
(175, 'moto', '2017-03-20 11:36:36', NULL, 47),
(176, 'motoh', '2017-03-20 11:36:41', NULL, 47),
(177, 'moto', '2017-03-20 11:36:50', NULL, 47),
(178, 'moto', '2017-03-20 11:37:37', NULL, 47),
(179, 'moto', '2017-03-20 11:41:36', NULL, 47),
(180, 'moto', '2017-03-20 11:42:04', NULL, 47),
(181, 'moto', '2017-03-20 11:44:16', NULL, 47),
(182, 'flores', '2017-03-20 11:44:29', NULL, 47),
(183, 'flores', '2017-03-20 11:44:29', NULL, 47),
(184, 'empresa', '2017-03-20 11:44:41', NULL, 47),
(185, 'quilla', '2017-03-20 11:45:34', NULL, 47),
(186, 'moto', '2017-03-20 12:31:37', NULL, 47),
(187, 'moto', '2017-03-20 12:32:53', NULL, 47),
(188, 'moto', '2017-03-20 12:33:13', NULL, 47),
(189, 'moto', '2017-03-20 12:34:06', NULL, 47),
(190, 'moto', '2017-03-20 12:42:03', NULL, 47),
(191, 'moto', '2017-03-20 12:42:35', NULL, 47),
(192, 'trabajo +en +turismo', '2017-03-20 13:10:41', NULL, 47),
(193, 'transporte', '2017-03-20 13:11:20', NULL, 47),
(194, 'trabajo +bajo +presion', '2017-03-20 13:11:42', NULL, 47),
(195, 'seÃ±orita', '2017-03-20 13:12:22', NULL, 47),
(196, 'casa', '2017-03-20 13:13:13', NULL, 47),
(197, 'departamento', '2017-03-20 13:13:28', NULL, 47),
(198, 'departamento', '2017-03-20 13:14:54', NULL, 47),
(199, 'moto', '2017-03-20 19:14:46', NULL, 47),
(200, 'trabajo +turismo', '2017-03-20 19:16:43', NULL, 47),
(201, 'moto', '2017-03-20 19:27:47', NULL, 47),
(202, 'moto', '2017-03-20 19:43:26', NULL, 47),
(203, 'moto', '2017-03-20 19:46:01', NULL, 47),
(204, 'moto', '2017-03-20 19:50:51', NULL, 47),
(205, 'moto', '2017-03-22 06:59:38', NULL, 47),
(206, 'moto', '2017-03-22 06:59:44', NULL, 47),
(207, 'terrenos +en +ollantaytambo', '2017-03-22 15:30:51', NULL, 69),
(208, 'ipad', '2017-03-26 19:57:46', NULL, 79),
(209, 'moto', '2017-03-27 10:13:20', NULL, 47),
(210, 'restaurante', '2017-03-27 10:13:28', NULL, 47),
(211, 'celulares', '2017-03-27 14:00:36', NULL, 81),
(212, 'celulres', '2017-03-27 14:01:02', NULL, 81),
(213, 'empleo', '2017-03-27 16:34:04', NULL, 82),
(214, '', '2017-03-27 16:34:38', NULL, 82),
(215, 'empleo', '2017-03-27 16:36:50', NULL, 82),
(216, 'empleo', '2017-03-27 16:36:53', NULL, 82),
(217, 'j5', '2017-03-29 18:09:52', NULL, 86),
(218, 'celulares', '2017-03-29 18:10:27', NULL, 86),
(219, 'enfermera', '2017-03-29 22:05:45', NULL, 53),
(220, 'moto', '2017-03-30 10:45:51', NULL, 47),
(221, 'moto', '2017-03-31 10:32:04', '2017-03-31 10:32:04', 47),
(222, 'celular +j5', '2017-03-31 11:23:15', '2017-03-31 11:23:15', 47),
(223, 'celular', '2017-03-31 11:23:21', '2017-03-31 11:23:21', 47),
(224, 'celular +J7', '2017-03-31 11:23:47', '2017-03-31 11:23:47', 47),
(225, ' +J7', '2017-03-31 11:23:52', '2017-03-31 11:23:52', 47),
(226, 'secretaria', '2017-04-01 13:13:53', '2017-04-01 13:13:53', 95),
(227, 'jovencitas', '2017-04-01 13:30:27', '2017-04-01 13:30:27', 96),
(228, 'jovencitas', '2017-04-01 13:31:00', '2017-04-01 13:31:00', 96),
(229, '', '2017-04-02 19:25:20', '2017-04-02 19:25:20', 96),
(230, '', '2017-04-02 19:25:51', '2017-04-02 19:25:51', 96),
(231, 's7', '2017-04-02 22:08:24', '2017-04-02 22:08:24', 90),
(232, 'galaxias +s7', '2017-04-02 22:08:56', '2017-04-02 22:08:56', 90),
(233, 'celulares', '2017-04-03 14:50:12', '2017-04-03 14:50:12', 104),
(234, 'Trabajo', '2017-04-03 15:16:45', '2017-04-03 15:16:45', 105),
(235, 'Trabajo', '2017-04-03 15:17:04', '2017-04-03 15:17:04', 105),
(236, 'Trabajo', '2017-04-03 15:17:53', '2017-04-03 15:17:53', 105),
(237, 'auto', '2017-04-03 19:16:22', '2017-04-03 19:16:22', 106),
(238, 'auto', '2017-04-03 19:16:26', '2017-04-03 19:16:26', 106),
(239, 'auto', '2017-04-03 19:16:28', '2017-04-03 19:16:28', 106),
(240, 'auto', '2017-04-03 19:16:36', '2017-04-03 19:16:36', 106),
(241, 'auto', '2017-04-03 19:16:42', '2017-04-03 19:16:42', 106),
(242, 'camioneta', '2017-04-03 19:17:15', '2017-04-03 19:17:15', 106),
(243, 'audifono', '2017-04-04 14:35:42', '2017-04-04 14:35:42', 108),
(244, 'servicios', '2017-04-04 17:15:37', '2017-04-04 17:15:37', 104),
(245, 'servicios', '2017-04-04 17:16:00', '2017-04-04 17:16:00', 104),
(246, 'servicios', '2017-04-04 17:16:07', '2017-04-04 17:16:07', 104),
(247, 'servicios +sexo', '2017-04-04 17:16:29', '2017-04-04 17:16:29', 104),
(248, 'servicios +sexo', '2017-04-04 17:16:49', '2017-04-04 17:16:49', 104),
(249, '', '2017-04-04 17:17:25', '2017-04-04 17:17:25', 104),
(250, '', '2017-04-04 17:17:41', '2017-04-04 17:17:41', 104),
(251, '', '2017-04-04 17:17:48', '2017-04-04 17:17:48', 104),
(252, '', '2017-04-04 17:18:15', '2017-04-04 17:18:15', 104),
(253, '', '2017-04-04 17:19:32', '2017-04-04 17:19:32', 104),
(254, 'celulares', '2017-04-08 13:15:12', '2017-04-08 13:15:12', 110),
(255, 'Huawei +p9 +lite', '2017-04-08 13:15:58', '2017-04-08 13:15:58', 110),
(256, 'Huawei +p9 +lite', '2017-04-08 13:16:04', '2017-04-08 13:16:04', 110),
(257, 'secretaria', '2017-04-08 17:23:31', '2017-04-08 17:23:31', 122),
(258, 'secretaria', '2017-04-08 17:23:52', '2017-04-08 17:23:52', 122),
(259, 'secretaria', '2017-04-08 17:23:53', '2017-04-08 17:23:53', 122),
(260, 'mariachi', '2017-04-08 17:35:45', '2017-04-08 17:35:45', 100),
(261, 'trabajos', '2017-04-08 21:57:22', '2017-04-08 21:57:22', 125),
(262, 'trabajos', '2017-04-08 21:57:31', '2017-04-08 21:57:31', 125),
(263, 'gasfitero +', '2017-04-09 11:03:00', '2017-04-09 11:03:00', 71),
(264, 'cupos', '2017-04-10 13:33:24', '2017-04-10 13:33:24', 137),
(265, 'moto +', '2017-04-10 18:48:02', '2017-04-10 18:48:02', 139),
(266, '', '2017-04-10 18:49:06', '2017-04-10 18:49:06', 139),
(267, 'trabajÃ³ +', '2017-04-11 22:26:29', '2017-04-11 22:26:29', 140),
(268, 'mariachi', '2017-04-14 21:25:46', '2017-04-14 21:25:46', 100),
(269, 'celular', '2017-04-21 09:10:58', '2017-04-21 09:10:58', 148),
(270, 'mis', '2017-04-23 17:18:54', '2017-04-23 17:18:54', -1),
(271, 'moto', '2017-04-23 17:19:09', '2017-04-23 17:19:09', -1),
(272, 'moto', '2017-04-23 17:21:20', '2017-04-23 17:21:20', -1),
(273, 'moto', '2017-04-23 17:27:06', '2017-04-23 17:27:06', -1),
(274, 'moto', '2017-04-23 17:43:44', '2017-04-23 17:43:44', -1),
(275, 'moto', '2017-04-23 17:45:52', '2017-04-23 17:45:52', -1),
(276, 'moto', '2017-04-23 17:47:00', '2017-04-23 17:47:00', -1),
(277, 'moto', '2017-04-23 17:47:49', '2017-04-23 17:47:49', -1),
(278, 'moto', '2017-04-23 18:07:05', '2017-04-23 18:07:05', -1),
(279, 'moto', '2017-04-23 18:32:11', '2017-04-23 18:32:11', -1),
(280, 'moto', '2017-04-23 18:33:49', '2017-04-23 18:33:49', -1),
(281, 'carro', '2017-04-23 18:43:36', '2017-04-23 18:43:36', -1),
(282, 'moto', '2017-04-23 18:43:54', '2017-04-23 18:43:54', -1),
(283, 'moto', '2017-04-23 19:37:29', '2017-04-23 19:37:29', 47),
(284, 'moto', '2017-04-23 19:38:59', '2017-04-23 19:38:59', 47),
(285, 'moto', '2017-04-23 19:39:14', '2017-04-23 19:39:14', 47),
(286, 'celular', '2017-04-23 23:24:46', '2017-04-23 23:24:46', -1),
(287, 'moto', '2017-04-24 07:47:47', '2017-04-24 07:47:47', 47),
(288, 'moto', '2017-04-24 07:47:56', '2017-04-24 07:47:56', 47),
(289, 'moto', '2017-04-24 07:49:15', '2017-04-24 07:49:15', 47),
(290, 'moto', '2017-04-24 07:49:37', '2017-04-24 07:49:37', 47),
(291, 'moto', '2017-04-24 07:50:40', '2017-04-24 07:50:40', 47),
(292, 'moto', '2017-04-24 07:52:29', '2017-04-24 07:52:29', 47),
(293, 'moto', '2017-04-24 07:58:25', '2017-04-24 07:58:25', 47),
(294, 'moto', '2017-04-24 08:03:13', '2017-04-24 08:03:13', 47),
(295, 'moto', '2017-04-24 08:04:07', '2017-04-24 08:04:07', 47),
(296, 'moto', '2017-04-24 08:08:36', '2017-04-24 08:08:36', 47),
(297, 'moto', '2017-04-24 08:09:41', '2017-04-24 08:09:41', 47),
(298, 'moto', '2017-04-24 08:12:52', '2017-04-24 08:12:52', 47),
(299, 'moto', '2017-04-24 08:14:15', '2017-04-24 08:14:15', 47),
(300, 'moto', '2017-04-24 08:15:13', '2017-04-24 08:15:13', 47),
(301, 'moto', '2017-04-24 08:23:51', '2017-04-24 08:23:51', 47),
(302, 'moto', '2017-04-24 08:26:26', '2017-04-24 08:26:26', 47),
(303, 'moto', '2017-04-24 09:17:22', '2017-04-24 09:17:22', 47),
(304, 'moto', '2017-04-24 09:17:57', '2017-04-24 09:17:57', 47),
(305, 'moto', '2017-04-24 09:18:03', '2017-04-24 09:18:03', 47),
(306, 'moto', '2017-04-24 09:18:06', '2017-04-24 09:18:06', 47),
(307, 'moto', '2017-04-24 09:54:39', '2017-04-24 09:54:39', 47),
(308, 'moto', '2017-04-24 09:58:25', '2017-04-24 09:58:25', 47),
(309, 'moto', '2017-04-24 09:58:44', '2017-04-24 09:58:44', 47),
(310, 'moto', '2017-04-24 21:51:58', '2017-04-24 21:51:58', 47),
(311, 'moto', '2017-04-24 21:52:13', '2017-04-24 21:52:13', 47),
(312, 'moto', '2017-04-24 21:56:19', '2017-04-24 21:56:19', 47),
(313, 'Encuestas', '2017-04-30 17:45:15', '2017-04-30 17:45:15', 105),
(314, 'Encuestas', '2017-04-30 17:45:50', '2017-04-30 17:45:50', 105),
(315, 'Encuestas', '2017-04-30 17:45:57', '2017-04-30 17:45:57', 105),
(316, 'Encuestadora', '2017-04-30 17:46:11', '2017-04-30 17:46:11', 105),
(317, 'Tr', '2017-05-02 15:08:05', '2017-05-02 15:08:05', 105),
(318, 'auto', '2017-05-02 21:00:37', '2017-05-02 21:00:37', -1),
(319, '', '2017-05-05 15:02:13', '2017-05-05 15:02:13', 156),
(320, 'habitaciones', '2017-05-05 17:55:31', '2017-05-05 17:55:31', 156),
(321, '', '2017-05-06 07:28:45', '2017-05-06 07:28:45', 156),
(322, 'Encuesta', '2017-05-06 20:50:18', '2017-05-06 20:50:18', 105),
(323, '', '2017-05-06 20:50:24', '2017-05-06 20:50:24', 105),
(324, 'departamento', '2017-05-07 03:23:02', '2017-05-07 03:23:02', 159),
(325, 'moto', '2017-05-07 08:23:00', '2017-05-07 08:23:00', -1),
(326, 'Encuesta', '2017-05-07 21:47:38', '2017-05-07 21:47:38', 105),
(327, 'Inei', '2017-05-07 21:47:48', '2017-05-07 21:47:48', 105),
(328, 'b', '2017-05-08 12:10:55', '2017-05-08 12:10:55', 106),
(329, 'b', '2017-05-08 12:14:13', '2017-05-08 12:14:13', 106),
(330, 'computadoras', '2017-05-09 19:43:54', '2017-05-09 19:43:54', 156),
(331, '', '2017-05-09 19:44:00', '2017-05-09 19:44:00', 156),
(332, '', '2017-05-09 19:44:31', '2017-05-09 19:44:31', 156),
(333, 'computadoras', '2017-05-09 19:44:49', '2017-05-09 19:44:49', 156),
(334, '', '2017-05-09 19:44:52', '2017-05-09 19:44:52', 156),
(335, 'laptop', '2017-05-09 19:50:47', '2017-05-09 19:50:47', 156),
(336, 'laptop', '2017-05-09 19:50:49', '2017-05-09 19:50:49', 156),
(337, '', '2017-05-09 19:50:59', '2017-05-09 19:50:59', 156),
(338, '', '2017-05-09 19:51:01', '2017-05-09 19:51:01', 156),
(339, 'empleo', '2017-05-10 07:12:16', '2017-05-10 07:12:16', 160),
(340, 'empleo', '2017-05-10 07:12:17', '2017-05-10 07:12:17', 160),
(341, 'carro', '2017-05-10 07:12:27', '2017-05-10 07:12:27', 160),
(342, 'carro', '2017-05-10 07:14:08', '2017-05-10 07:14:08', 160),
(343, 'auto', '2017-05-10 07:14:18', '2017-05-10 07:14:18', 160),
(344, 'autos', '2017-05-19 11:52:16', '2017-05-19 11:52:16', -1),
(345, 'mariachi', '2017-05-23 13:30:19', '2017-05-23 13:30:19', 165),
(346, 'playstation', '2017-05-24 23:10:10', '2017-05-24 23:10:10', 165),
(347, 'play', '2017-05-24 23:10:32', '2017-05-24 23:10:32', 165),
(348, 'auto', '2017-05-24 23:10:53', '2017-05-24 23:10:53', 165),
(349, 'celular', '2017-05-27 17:45:30', '2017-05-27 17:45:30', 165),
(350, 'celular', '2017-05-27 17:46:13', '2017-05-27 17:46:13', 165),
(351, '', '2017-05-27 17:46:46', '2017-05-27 17:46:46', 165),
(352, '', '2017-05-27 17:47:02', '2017-05-27 17:47:02', 165),
(353, 'GASFITERIA +', '2017-06-12 16:14:48', '2017-06-12 16:14:48', 169),
(354, 'bicicleta +estacionaria', '2017-06-15 21:04:16', '2017-06-15 21:04:16', 171),
(355, 'bicicleta +estacionaria', '2017-06-15 21:04:32', '2017-06-15 21:04:32', 171),
(356, 'put', '2017-07-04 13:47:12', '2017-07-04 13:47:12', 108),
(357, 'puta', '2017-07-04 13:47:22', '2017-07-04 13:47:22', 108),
(358, 'autos', '2017-07-20 11:23:42', '2017-07-20 11:23:42', -1),
(359, 'autos', '2017-07-22 10:34:48', '2017-07-22 10:34:48', -1),
(360, 'tv +cable', '2017-07-22 11:28:46', '2017-07-22 11:28:46', 152),
(361, 'calculadora', '2017-08-12 01:01:17', '2017-08-12 01:01:17', 105),
(362, 'carros', '2017-10-03 16:34:24', '2017-10-03 16:34:24', 118),
(363, 'habitaciÃ³n', '2017-10-29 08:52:44', '2017-10-29 08:52:44', 201),
(364, 'printing', '2017-11-11 16:05:05', '2017-11-11 16:05:05', -1),
(365, 'empleo', '2017-11-28 18:51:17', '2017-11-28 18:51:17', 212),
(366, '', '2017-11-29 14:54:27', '2017-11-29 14:54:27', 217),
(367, 'casas', '2017-12-04 10:37:11', '2017-12-04 10:37:11', -1),
(368, 'autos', '2017-12-04 10:37:24', '2017-12-04 10:37:24', -1),
(369, 'reposteros +de +melamine', '2018-01-17 23:57:19', '2018-01-17 23:57:19', 226),
(370, 'reposteros +de +melamine', '2018-01-17 23:57:32', '2018-01-17 23:57:32', 226),
(371, 'alquiler', '2018-02-06 14:00:31', '2018-02-06 14:00:31', 229),
(372, 'alquiler', '2018-02-06 14:00:57', '2018-02-06 14:00:57', 229),
(373, 'alquiler', '2018-04-10 17:21:04', '2018-04-10 17:21:04', 235),
(374, 'alquiler', '2018-04-10 17:21:08', '2018-04-10 17:21:08', 235),
(375, 'Anticresis de departamentos', '2018-07-10 20:59:52', '2018-07-10 20:59:52', -1),
(376, 'Anticresis de departamentos', '2018-07-10 21:01:24', '2018-07-10 21:01:24', -1),
(377, 'Anticresis de departamentos', '2018-07-10 21:02:12', '2018-07-10 21:02:12', -1),
(378, 'brujeria', '2018-09-25 15:14:12', '2018-09-25 15:14:12', -1),
(379, 'casa +en +alquiler', '2018-10-26 14:18:58', '2018-10-26 14:18:58', 221),
(380, 'carro', '2018-10-28 10:23:51', '2018-10-28 10:23:51', -1),
(381, 'carro', '2018-10-28 19:50:26', '2018-10-28 19:50:26', -1),
(382, 'antologia', '2018-10-30 14:17:12', '2018-10-30 14:17:12', 243),
(383, 'Yaris +2015', '2018-11-29 22:52:10', '2018-11-29 22:52:10', 246),
(384, 'discotecas', '2018-11-29 22:52:50', '2018-11-29 22:52:50', 246),
(385, 'dry wall', '2018-12-01 18:48:17', '2018-12-01 18:48:17', -1),
(386, 'drywall', '2018-12-01 18:48:22', '2018-12-01 18:48:22', -1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargamoneda`
--

CREATE TABLE `cargamoneda` (
  ` id` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `origen` varchar(255) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `usuario_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `url_imagen` varchar(250) DEFAULT NULL,
  `url_icono` varchar(250) DEFAULT NULL,
  `orden` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`, `url_imagen`, `url_icono`, `orden`) VALUES
(2, 'Eventos', 'todo tipo de eventos reuniones', NULL, NULL, 8),
(3, 'Servicios', 'Se brinda los servicios profesionales y/o tecnicos', NULL, NULL, 7),
(4, 'Empleos', 'undefined', NULL, NULL, 1),
(5, 'Vehiculos', 'Compra venta de todo tipo de vehiculos.', NULL, NULL, 2),
(6, 'Inmuebles', 'Inmuebles', NULL, NULL, 9),
(18, 'Negocios', 'Negocios', NULL, NULL, 10),
(11, 'Ofertas', 'Todas los negocios podrÃ¡n dar a conocer sus promociones mensuales', NULL, NULL, 11),
(13, 'Alquileres', 'alquiler', NULL, NULL, 4),
(14, 'Anticresis', 'Anticresis', NULL, NULL, 5),
(15, 'Baratillo', 'venta de accesorios de segundo uso', NULL, NULL, 13),
(16, 'Convocatorias', 'Convocatorias', NULL, NULL, 14),
(17, 'Formacion', 'Formacion', NULL, NULL, 15),
(19, 'Mascotas', 'Mascotas', NULL, NULL, 17),
(22, 'Especiales', 'Viajes de placer seguro', NULL, NULL, 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicion`
--

CREATE TABLE `condicion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `categoria_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `denuncia`
--

CREATE TABLE `denuncia` (
  `id` int(11) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `anuncio_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `fechaAtendido` date DEFAULT NULL,
  `observacionAtendido` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `denuncia`
--

INSERT INTO `denuncia` (`id`, `tipo`, `fecha`, `titulo`, `descripcion`, `anuncio_id`, `usuario_id`, `fechaAtendido`, `observacionAtendido`) VALUES
(1, '5', '2017-02-03 07:43:48', 'Telefono incorrecto', 'jgugyfy', 2, 47, NULL, NULL),
(2, '1', '2017-02-08 17:49:17', 'No me gusta', 'No me gusta nn', 17, 45, '2017-02-15', NULL),
(3, '3', '2017-02-15 21:25:47', 'Es una estafa', 'kfufyy', 16, 46, '2017-02-15', NULL),
(4, '6', '2017-03-04 00:04:16', 'Anuncio obsoleto', 'kyvgt', 0, 46, NULL, NULL),
(5, '1', '2017-03-04 00:06:11', 'No me gusta', '', 0, 46, NULL, NULL),
(6, '6', '2017-05-18 08:44:05', 'Anuncio obsoleto', 'ya esta tomado', 42, 46, NULL, NULL),
(7, '6', '2017-07-01 09:55:39', 'Anuncio obsoleto', 'ya vencii', 0, 46, NULL, NULL),
(8, '6', '2017-08-29 13:43:15', 'Anuncio obsoleto', 'esta obsoleto', 73, 46, NULL, NULL),
(9, '5', '2017-11-09 04:14:19', 'Telefono incorrecto', '', 111, 203, NULL, NULL),
(10, '5', '2017-11-09 21:55:25', 'Telefono incorrecto', '', 111, 204, NULL, NULL),
(11, '5', '2017-11-16 09:24:40', 'Telefono incorrecto', '', 111, 205, NULL, NULL),
(12, '6', '2017-12-20 10:34:23', 'Anuncio obsoleto', '', 106, 165, NULL, NULL),
(13, '1', '2017-12-20 10:34:29', 'No me gusta', '', 106, 165, NULL, NULL),
(14, '1', '2017-12-20 10:34:35', 'No me gusta', 'gygjk', 106, 165, NULL, NULL),
(15, '5', '2018-05-04 22:18:09', 'Telefono incorrecto', '', 25, 237, NULL, NULL),
(16, '1', '2018-11-09 16:53:51', 'No me gusta', '', 108, 244, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id`, `nombre`, `descripcion`) VALUES
(1, 'Cusco', 'sur de l pais'),
(2, 'Arequipa', 'sur de l pais fgh'),
(3, 'Puno', 'sur del pais'),
(5, 'Lima', 'capital');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `distrito`
--

CREATE TABLE `distrito` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `provincia_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `distrito`
--

INSERT INTO `distrito` (`id`, `nombre`, `descripcion`, `latitud`, `longitud`, `provincia_id`) VALUES
(108, 'Kimbiri', 'Kimbiri', -12.610940224747644, -73.78495216369629, 2),
(107, 'Huayopata', 'Huayopata', -13.006585747995814, -72.6211953163147, 2),
(74, 'Cusco', 'DescripciÃ³n', -13.536275007784575, -71.98680074885488, 1),
(105, 'Santa Ana', 'Santa Ana', -12.872095488482033, -72.6831865310669, 2),
(104, 'Yura', 'Yura', -16.31257135390199, -71.63576245307922, 14),
(103, 'Yarabamba', 'Yarabamba', -16.556589579839226, -71.46076440811157, 14),
(76, 'Selva Alegre', 'Selva Alegre', -16.365444103314797, -71.51967108249664, 14),
(77, 'Arequipa', 'Arequipa', -16.398871481400906, -71.53696060180664, 14),
(102, 'Yanahuara', 'Yanahuara', -16.39739451897823, -71.55674993991852, 14),
(101, 'VÃ­tor', 'VÃ­tor', -16.46788058849743, -71.93809635937214, 14),
(100, 'Uchumayo', 'Uchumayo', -16.42077758349085, -71.63365423679352, 14),
(99, 'Tiabaya', 'Tiabaya', -16.43617007101468, -71.60530596971512, 14),
(98, 'Socabaya', 'Socabaya', -16.466717315885823, -71.53207898139954, 14),
(97, 'Santa Rita de Siguas', 'Santa Rita de Siguas', -16.491738032738485, -72.09100842475891, 14),
(96, 'Santa Isabel de Siguas', 'Santa Isabel de Siguas', -16.333608143699834, -72.10019364953041, 14),
(95, 'San Juan de Tarucani', 'San Juan de Tarucani', -16.184220615328243, -71.05992570519447, 14),
(94, 'San Juan de Siguas', 'San Juan de Siguas', -16.396406440301398, -72.16271460056305, 14),
(93, 'Sachaca', 'Sachaca', -16.42011894409125, -71.58455908298492, 14),
(92, 'SabandÃ­a', 'SabandÃ­a', -16.4547742373011, -71.466603577137, 14),
(91, 'QuequeÃ±a', 'QuequeÃ±a', -16.55757748455028, -71.45096428692341, 14),
(90, 'Polobaya', 'Polobaya', -16.565952939012266, -71.37890875339508, 14),
(89, 'Pocsi', 'Pocsi', -16.504650934768513, -71.36189013719559, 14),
(88, 'Paucarpata', 'Paucarpata', -16.41616706083982, -71.47091925144196, 14),
(87, 'Mollebaya', 'Mollebaya', -16.50514469921149, -71.47175341844559, 14),
(86, 'Miraflores', 'Miraflores', -16.38133504605639, -71.50351077318192, 14),
(85, 'Mariano Melgar', 'Mariano Melgar', -16.395418356610634, -71.49632513523102, 14),
(84, 'La Joya', 'La Joya', -16.728774586697853, -71.8798166513443, 14),
(83, 'JosÃ© Luis Bustamante y Rivero', 'JosÃ© Luis Bustamante y Rivero', -16.434316502425492, -71.52220979332924, 14),
(82, 'Jacobo Hunter', 'Jacobo Hunter', -16.4478596226534, -71.5548375248909, 14),
(81, 'Chiguata', 'Chiguata', -16.40612747549094, -71.37483716011047, 14),
(80, 'Characato', 'Characato', -16.47280303848907, -71.4839655160904, 14),
(79, 'Cerro Colorado', 'Cerro Colorado', -16.381829124244025, -71.5551808476448, 14),
(78, 'Cayma', 'Cayma', -16.344031102193348, -71.54198706150055, 14),
(62, 'San Jeronimo', 'fg', -13.545448565060882, -71.88925409282092, 1),
(72, 'San Sebastian', '2do distrito mas grande', -13.530397010977866, -71.93734216685698, 1),
(75, 'Wanchaq', 'DescripciÃ³n', -13.532671624711867, -71.96015484631062, 1),
(106, 'Echarate', 'Echarate', -12.746349492725233, -72.60594427585602, 2),
(109, 'Maranura', 'Maranura', -12.950380948404192, -72.67338037490845, 2),
(110, 'Ocobamba', 'Ocobamba', -12.875421497851574, -72.4454140663147, 2),
(111, 'Pichari', 'Pichari', -12.875421497851574, -72.4454140663147, 2),
(112, 'QuelloÃºno', 'QuelloÃºno', -12.637009206564924, -72.56214380264282, 2),
(113, 'Santa Teresa', 'Santa Teresa', -13.108257270311528, -72.62531518936157, 2),
(114, 'Vilcabamba', 'Vilcabamba', -13.12633385677473, -72.9853105545044, 2),
(115, 'Inkawasi', 'Inkawasi', -13.07615499622598, -72.39185571670532, 2),
(116, 'Megantoni', 'Megantoni', -12.261854618649886, -72.28189587593079, 2),
(117, 'Urubamba', 'Urubamba', -13.299931691392304, -72.12529242038727, 5),
(118, 'Chinchero', 'Chinchero', -13.405003537449408, -72.03617334365845, 5),
(119, 'Huayllabamba', 'Huayllabamba', -13.339353998260046, -72.06349432468414, 5),
(120, 'Machupicchu', 'Machupicchu', -13.158063930220665, -72.53389477729797, 5),
(121, 'Maras', 'Maras', -13.338200444144453, -72.14466333389282, 5),
(122, 'Ollantaytambo', 'Ollantaytambo', -13.25801235683791, -72.26688623428345, 5),
(123, 'Yucay', 'Yucay', -13.319977734670063, -72.08169043064117, 5),
(124, 'Anta', 'Anta', -13.464927863462336, -72.14795172214508, 3),
(125, 'Ancahuasi', 'Ancahuasi', -13.459768230135472, -72.3025918006897, 3),
(126, 'Cachimayo', 'Cachimayo', -13.457248368788862, -72.06898748874664, 3),
(127, 'Chinchaypujio', 'Chinchaypujio', -13.422024924405287, -72.09100842475891, 3),
(128, 'Huarocondo', 'Huarocondo', -13.415507681137527, -72.2042566537857, 3),
(129, 'Limatambo', 'Limatambo', -13.48647817212599, -72.44816064834595, 3),
(130, 'Mollepata', 'Mollepata', -13.515855659379056, -72.54566431045532, 3),
(131, 'Pucyura', 'Pucyura', -13.48113642242564, -72.1103310585022, 3),
(132, 'Zurite', 'Zurite', -13.455531926310472, -72.25557804107666, 3),
(133, 'Sicuani', 'Sicuani', -14.271079336285462, -71.2165117263794, 9),
(134, 'Checacupe', 'Checacupe', -14.026361036193153, -71.45792126655579, 9),
(135, 'Combapata', 'Combapata', -14.10229213022676, -71.43182873725891, 9),
(136, 'Marangani', 'Marangani', -14.35787336882835, -71.16678357124329, 9),
(137, 'Pitumarca', 'Pitumarca', -13.980218735976045, -71.41701757907867, 9),
(138, 'San Pablo', 'San Pablo', -14.202989813509383, -71.31676733493805, 9),
(139, 'San Pedro', 'San Pedro', -14.18709410641064, -71.34283572435379, 9),
(140, 'Tinta', 'Tinta', -14.139410703981397, -71.40843451023102, 9),
(141, 'Paucartambo', 'Paucartambo', -13.319492264169137, -71.58710718154907, 12),
(142, 'Caicay', 'Caicay', -13.597443669698114, -71.69510900974274, 12),
(143, 'Challabamba', 'Challabamba', -13.21221601010797, -71.64743542671204, 12),
(144, 'Colquepata', 'Colquepata', -13.361239164337375, -71.67215466499329, 12),
(145, 'KosÃ±ipata', 'KosÃ±ipata', -13.057750733711941, -71.18120312690735, 12),
(146, 'Huancarani', 'Huancarani', -13.50358831193944, -71.62150986492634, 12),
(147, 'Calca', 'Calca', -13.325829404617412, -71.95917248725891, 6),
(148, 'Coya', 'Coya', -13.389964019207182, -71.90012097358704, 6),
(149, 'Lamay', 'Lamay', -13.366411352224766, -71.92032873630524, 6),
(150, 'Lares', 'Lares', -13.105582240623578, -72.04029321670532, 6),
(151, 'Pisac', 'Pisac', -13.422369305438306, -71.84253931045532, 6),
(152, 'San Salvador', 'San Salvador', -13.493306465341657, -71.7860895395279, 6),
(153, 'Taray', 'Taray', -13.42689969628733, -71.86759248375893, 6),
(154, 'Yanatile', 'Yanatile', -13.216571471958165, -72.68573999404907, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE `mensaje` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `usuario_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mensaje`
--

INSERT INTO `mensaje` (`id`, `nombre`, `correo`, `mensaje`, `usuario_id`) VALUES
(1, NULL, NULL, NULL, NULL),
(2, 'luis', 'herrere', 'menaje', 47),
(3, NULL, NULL, NULL, NULL),
(4, NULL, NULL, NULL, NULL),
(5, 'luis ', 'correo ', NULL, 47),
(6, 'jose', 'josereyest@hotmail.com', NULL, 46),
(7, 'ndjfj', 'josereyest@hotmail.com', NULL, 46),
(8, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(200) DEFAULT NULL,
  `departamento_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`id`, `nombre`, `descripcion`, `departamento_id`) VALUES
(1, 'Cusco', 'gdfg', 1),
(2, 'La Convencion', NULL, 1),
(3, 'Anta', 'dfg', 1),
(4, 'Chumbivilcas', 'f', 1),
(5, 'Urubamba', 'Urubamba', 1),
(6, 'Calca', 'Calca', 1),
(7, 'Canas', 'Descripcion', 1),
(8, 'Acomayo', 'Descripcion', 1),
(9, 'Canchis', 'Descripcion', 1),
(10, 'Espinar', 'Descripcion', 1),
(11, 'Paruro', 'Descripcion', 1),
(12, 'Paucartambo', 'Descripcion', 1),
(13, 'Quispicanchi', 'Quispicanchi', 1),
(14, 'Arequipa', 'Provincia de Arequipa', 2),
(15, 'CamanÃ¡', 'Provincia de CamanÃ¡', 2),
(16, 'CaravelÃ­', 'Provincia de caravelÃ­', 2),
(17, 'Castilla', 'Provincia de castilla', 2),
(18, 'Caylloma', 'Provincia de caylloma', 2),
(19, 'Condesuyos', 'Provincia de condesuyos', 2),
(20, 'Islay', 'Provincia de islay', 2),
(21, 'Union', 'Provincia de la union', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `Categoria_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`id`, `nombre`, `descripcion`, `Categoria_id`) VALUES
(1, 'Accesorios', NULL, 1),
(2, 'Nuevos', '', 1),
(3, 'Segundo uso', '', 1),
(4, 'De Repuesto', NULL, 1),
(5, 'Politicos', 'df', 2),
(6, 'Musicales', 'bv', 2),
(7, 'Artisticos', '', 2),
(8, 'Diversion', '', 2),
(36, 'GastronÃ³micos', 'GastronÃ³micos', 2),
(10, 'Gasfiteros', '', 3),
(11, 'Electricistas', '', 3),
(12, 'Carpinteros', '', 3),
(13, 'Melamineros', '', 3),
(14, 'NiÃ±eras', '', 3),
(38, 'Drywall', 'drywall', 3),
(16, 'EnseÃ±anza', '', 3),
(17, 'Otros', '', 3),
(19, 'Restaurante', '', 4),
(39, 'Oficina', 'Oficina', 4),
(45, 'Ventas', 'Ventas', 4),
(22, 'Autos', '', 5),
(23, 'Camiones', '', 5),
(24, 'Camionetas', '', 5),
(25, 'Buses', '', 5),
(26, 'Motos', '', 5),
(27, 'Maquinaria pesada', '', 5),
(28, 'Terrenos', '', 6),
(29, 'Departamentos', '', 6),
(48, 'Otros', 'Otros', 6),
(31, 'Casas', '', 6),
(32, 'Restaurantes, pollerias, bares', '', 7),
(33, 'Otros', '', 7),
(34, 'Culturales', 'Culturales', 2),
(37, 'Otros', 'Otros', 2),
(40, 'Limpieza', 'Limpieza', 4),
(43, 'Discotecas', 'Discotecas', 4),
(42, 'Turismo', 'Agencia de viajes y alojamientos', 4),
(44, 'Seguridad', 'Seguridad', 4),
(46, 'Otros', 'Otros', 4),
(47, 'Otros', 'Otros', 5),
(49, 'Traspasos', 'Traspasos', 18),
(50, 'Varios', 'Varios', 18),
(51, 'Productos', 'Productos', 11),
(52, 'Servicios', 'Servicios', 11),
(53, 'Otros', 'Otros', 11),
(55, 'Temporada', 'Temporada', 12),
(56, 'Shows', 'Shows', 12),
(99, 'Transporte', 'servicio de transporte turistico', 21),
(63, 'Ejecutivos', 'Ejecutivos', 12),
(98, 'Anfitrionaje', 'Anfitrionaje', 12),
(62, 'Otros', 'Otros', 12),
(65, 'Tiendas', 'Tiendas', 13),
(66, 'Oficinas', 'Oficinas', 13),
(67, 'Casas', 'Casas', 13),
(68, 'Departamentos', 'Departamentos', 13),
(69, 'Habitaciones', 'Habitaciones', 13),
(70, 'Otros', 'Otros', 13),
(71, 'Casas', 'Casas', 14),
(72, 'Departamentos', 'Departamentos', 14),
(73, 'Ambientes', 'ambientes', 14),
(74, 'Requeridos', 'Requeridos', 14),
(75, 'Otros', 'Otros', 14),
(76, 'Muebles', 'Muebles', 15),
(77, 'Artefactos', 'Artefactos', 15),
(78, 'Ropa y accesorios', 'Ropa y accesorios', 15),
(79, 'Herramientas', 'Herramientas', 15),
(80, 'Compro', 'Compro', 15),
(81, 'Otros', 'Otros', 15),
(82, 'Entidades Publicas', 'Entidades Publicas', 16),
(83, 'Entidades Privadas', 'Entidades Privadas', 16),
(84, 'Otros', 'Otros', 16),
(85, 'Cursos', 'Cursos', 17),
(86, 'MaestrÃ­as', 'MaestrÃ­as', 17),
(87, 'CapacitaciÃ³n', 'CapacitaciÃ³n', 17),
(88, 'Profesores', 'Profesores', 17),
(89, 'Otros', 'otros', 17),
(90, 'Accesorios', 'Accesorios', 19),
(91, 'en adopciÃ³n', 'en adopciÃ³n', 19),
(92, 'en venta', 'en venta', 19),
(93, 'Entidades Publicas', 'Entidades Publicas', 20),
(94, 'Entidades Privadas', 'Entidades Privadas', 20),
(95, 'Agencias de Viaje', 'Agencias de Viaje', 20),
(96, 'Hoteles', 'Hoteles', 20),
(97, 'Hoteles', 'Hoteles', 13),
(100, 'Culturales', 'Agencias de viaje operadores de tours culturales', 21),
(101, 'Aventura', 'Agecnias de viajes dedicadas a tour de aventura', 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `clave` varchar(250) DEFAULT NULL,
  `celular` varchar(10) DEFAULT NULL,
  `facebook_id` varchar(100) DEFAULT NULL,
  `twiter_id` varchar(100) DEFAULT NULL,
  `google_id` varchar(100) DEFAULT NULL,
  `provincia_id` int(11) DEFAULT NULL,
  `latitud` double DEFAULT NULL,
  `longitud` double DEFAULT NULL,
  `id_msg` varchar(250) DEFAULT NULL,
  `genero` varchar(2) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha_validacion` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `correo`, `clave`, `celular`, `facebook_id`, `twiter_id`, `google_id`, `provincia_id`, `latitud`, `longitud`, `id_msg`, `genero`, `device_id`, `fecha_registro`, `fecha_validacion`) VALUES
(69, 'Royer E Ustua', '', NULL, '961796034', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5b6b1a6599da04c2', '2017-03-22 20:42:00', NULL),
(68, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a995e03d8afcfdc4', '2017-03-20 20:40:00', NULL),
(67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bd8605bb11ae7d50', '2017-03-18 20:38:00', NULL),
(66, 'Isaias Atila', '', NULL, '983021380', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bdadad59a488b8ef', '2017-03-17 20:37:00', NULL),
(65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b1b065106ee6d2be', '2017-03-13 20:33:00', NULL),
(63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4b0e311b36d9820d', '2017-03-11 20:31:00', NULL),
(64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c16bb87dfd126f6f', '2017-03-12 20:32:00', NULL),
(61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1e4624c56ce9c971', '2017-03-10 20:30:00', NULL),
(62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dd7facc45e8c3ec7', '2017-03-11 20:31:00', NULL),
(60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8d88de965f32281f', '2017-03-08 20:28:00', NULL),
(59, 'Adzenaiqh DC', '', NULL, '940801683', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fca14b99d9e0e33d', '2017-03-07 20:27:00', NULL),
(58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e6fa1c174c474dc8', '2017-03-07 20:27:00', NULL),
(57, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '39ea26b5e4cfde92', '2017-03-04 20:24:00', NULL),
(75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7d160a283e2561eb', '2017-03-25 20:45:00', NULL),
(74, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c2d24821a3405d8f', '2017-03-25 20:45:00', NULL),
(73, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a153dde7ccd2715d', '2017-03-25 20:45:00', NULL),
(71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '92799863079957b1', '2017-03-25 20:45:00', NULL),
(72, 'Rosa Isabel Uribe Pardo', '', NULL, '993484394', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9dde818f6408b9de', '2017-03-25 20:45:00', NULL),
(70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1f9050367a5849b1', '2017-03-24 20:44:00', NULL),
(56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9c031750c6d477cc', '2017-03-03 20:23:00', NULL),
(55, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '621b1bf5c582b85f', '2017-03-03 20:23:00', NULL),
(47, 'Luiz H Q. Herrera', '', NULL, '940414253', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ede541808c9b8561', '2017-02-11 20:30:00', NULL),
(48, 'eder', 'luixze@gotmai.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-11 12:19:25', NULL),
(49, 'eder', 'luizer@gmil.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-11 12:21:52', NULL),
(50, 'juanito', 'juan@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-14 11:45:12', NULL),
(51, 'jorge', 'jorge@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-14 12:14:25', NULL),
(52, 'Jose Antonio', 'manager@willayanunciosperu.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-17 11:45:18', NULL),
(53, 'Veloz Paul', '', NULL, '925641057', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd87ccf6c817c424b', '2017-02-21 20:40:00', NULL),
(54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4734e7b8460cb794', '2017-03-03 20:23:00', NULL),
(46, 'Jose Antonio Ttito Reyes', '', NULL, '999301416', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '54739b3913e50e60', '2017-02-04 20:23:00', NULL),
(76, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c16a972af5fdaea4', '2017-03-26 20:46:00', NULL),
(77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8fa8cc3c7174a4ae', '2017-03-26 20:46:00', NULL),
(78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e0da449e2521d511', '2017-03-26 20:46:00', NULL),
(79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3ad956df8595ba8c', '2017-03-26 20:46:00', NULL),
(80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '55979bf41c36e72b', '2017-03-27 20:47:00', NULL),
(81, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1506ce2a6f6764d3', '2017-03-27 20:47:00', NULL),
(82, 'Kim Yura Yesi', '', NULL, '941231612', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c57f8180eb9591f4', '2017-03-27 20:47:00', NULL),
(83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '917b8c5ef939f847', '2017-03-28 20:48:00', NULL),
(84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4d302e5c541f4fd5', '2017-03-29 20:49:00', NULL),
(85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c9841cab08643b81', '2017-03-29 20:49:00', NULL),
(86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23389903d1da2d8', '2017-03-29 20:49:00', NULL),
(87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bb311267676c1914', '2017-03-29 20:49:00', NULL),
(88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '441bfab93c001322', '2017-03-30 20:50:00', NULL),
(89, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6250d419aea6c24', '2017-03-30 20:50:00', NULL),
(90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '98b8ad84af01a627', '2017-03-31 20:51:00', NULL),
(91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '869cdaa5a85fd556', '2017-03-31 20:51:00', NULL),
(92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7322e65f8d7b7a18', '2017-03-31 20:51:00', NULL),
(93, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '617d957f1d50b016', '2017-03-31 20:51:00', NULL),
(94, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd52199192a590684', '2017-04-01 20:22:00', NULL),
(95, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a300da1a63f1808f', '2017-04-01 20:22:00', NULL),
(96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ecdb8c762e2b6cf6', '2017-04-01 20:22:00', NULL),
(97, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '349166ab42fe4418', '2017-04-01 20:22:00', NULL),
(98, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b210df4bd9470f9c', '2017-04-01 20:22:00', NULL),
(99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f93b5a61ac5d23b4', '2017-04-01 20:22:00', NULL),
(100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '575a958caa2152b', '2017-04-01 20:22:00', NULL),
(101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '857fda6d7ef30b30', '2017-04-03 20:24:00', NULL),
(102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'cfe181da51817464', '2017-04-03 20:24:00', NULL),
(103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7af0a98f0ff33810', '2017-04-03 20:24:00', NULL),
(104, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f52435d278852e7a', '2017-04-03 20:24:00', NULL),
(105, 'Michell Bravo', '', NULL, '943080820', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a6cef80d33fb24f7', '2017-04-03 20:24:00', NULL),
(106, 'Huayllany Kens Julio Cesar', '', NULL, '931658478', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a1b9c7fd34454cb3', '2017-04-03 20:24:00', NULL),
(107, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2e4808fef176f968', '2017-04-03 20:24:00', NULL),
(108, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8384c268062f46de', '2017-04-04 20:25:00', NULL),
(109, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '52fe066afadf2119', '2017-04-04 20:25:00', NULL),
(110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6d29299e10d1e7b7', '2017-04-04 20:25:00', NULL),
(111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ddc425dd9ee2c2a', '2017-04-05 20:26:00', NULL),
(112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8e4ba8a53ec959ca', '2017-04-05 20:26:00', NULL),
(113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2743674f86d58742', '2017-04-05 20:26:00', NULL),
(114, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f0920cf3735f3e9b', '2017-04-06 20:27:00', NULL),
(115, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bb80a52715645e81', '2017-04-07 20:28:00', NULL),
(116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abcd0f720783084a', '2017-04-08 20:29:00', NULL),
(117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e408beea377303de', '2017-04-08 20:29:00', NULL),
(118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '51acf409e47e34d6', '2017-04-08 20:29:00', NULL),
(119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '555f98a6d057879c', '2017-04-08 20:29:00', NULL),
(120, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9564d12fc918aeaf', '2017-04-08 20:29:00', NULL),
(121, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '626e86c116bff7f9', '2017-04-08 20:29:00', NULL),
(122, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ce7fc652415dafce', '2017-04-08 20:29:00', NULL),
(123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd525a4761bf8fdcf', '2017-04-08 20:29:00', NULL),
(124, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'fa85e76db445198e', '2017-04-08 20:29:00', NULL),
(125, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b5b5ed003da6e7b6', '2017-04-08 20:29:00', NULL),
(126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '72cba68352e520f4', '2017-04-08 20:29:00', NULL),
(127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '42b25c4e135881ee', '2017-04-08 20:29:00', NULL),
(128, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '14ce44b74b6cc651', '2017-04-08 20:29:00', NULL),
(129, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '803c4b55a573339b', '2017-04-08 20:29:00', NULL),
(130, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '35c94759ab64bc8c', '2017-04-08 20:29:00', NULL),
(131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f52d3f3d360e58f7', '2017-04-09 20:30:00', NULL),
(132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2ddc87ad7ec0f658', '2017-04-09 20:30:00', NULL),
(133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '63caa187e552e053', '2017-04-09 20:30:00', NULL),
(134, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '591c7cb1cba7f92d', '2017-04-09 20:30:00', NULL),
(135, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2679fe0f092b474b', '2017-04-09 20:30:00', NULL),
(136, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e10c7b515d8e5f91', '2017-04-09 20:30:00', NULL),
(137, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7701aa8ee735bf42', '2017-04-10 20:31:00', NULL),
(138, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'efa96e369c10be6e', '2017-04-10 20:31:00', NULL),
(139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4cfeb50f33de62c5', '2017-04-10 20:31:00', NULL),
(140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '75f783f8e657d4af', '2017-04-11 20:32:00', NULL),
(141, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1686bf12c954ccc8', '2017-04-12 20:33:00', NULL),
(142, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6e38020b4c57d086', '2017-04-13 20:34:00', NULL),
(143, 'Mario Aguilar', '', NULL, '983016636', '2147483647', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '98a74e6f2bf043a9', '2017-04-16 20:37:00', NULL),
(144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '74610c23b12eadeb', '2017-04-17 20:38:00', NULL),
(145, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '710c302c35d338bf', '2017-04-18 20:39:00', NULL),
(146, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8014f7b7208f0af8', '2017-04-20 20:41:00', NULL),
(147, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b352f3e0d704b65e', '2017-04-21 20:42:00', NULL),
(148, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1ad83288f5c294a7', '2017-04-21 20:42:00', NULL),
(149, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e8f90a74ccff8c34', '2017-04-21 20:42:00', NULL),
(150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3be13ebfd7d86b07', '2017-04-23 20:44:00', NULL),
(151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3917931ebea24029', '2017-04-25 20:46:00', NULL),
(152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '381dea951f598200', '2017-04-26 20:47:00', NULL),
(153, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dacb34d6975f83c3', '2017-04-30 20:51:00', NULL),
(154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a307d4ff9be76836', '2017-05-04 20:26:00', NULL),
(155, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '943b56df9d5bb8cf', '2017-05-04 20:26:00', NULL),
(156, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5f99f7bf9cbf9d3c', '2017-05-05 20:27:00', NULL),
(157, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7b57ac04a9dcbc4e', '2017-05-06 20:28:00', NULL),
(158, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2f1557404f1a3904', '2017-05-06 20:28:00', NULL),
(159, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c2557a4c6fc2835a', '2017-05-07 20:29:00', NULL),
(160, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd816363408e7b4a1', '2017-05-10 20:32:00', NULL),
(161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4246a3cc773002fd', '2017-05-18 20:40:00', NULL),
(162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c586b5ffd198ea1a', '2017-05-19 20:41:00', NULL),
(163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '188fbe4e5a3d66e8', '2017-05-21 20:43:00', NULL),
(164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'db43706183804dc5', '2017-05-22 20:44:00', NULL),
(165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c0f8e4a45f5853df', '2017-05-22 20:44:00', NULL),
(166, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '75aa5f25341b8665', '2017-05-29 20:51:00', NULL),
(167, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7867e266cf1521b5', '2017-06-05 20:28:00', NULL),
(168, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8149b07ae036fc37', '2017-06-05 20:28:00', NULL),
(169, 'Veloz Paul', '', NULL, '925641057', '1438953226115347', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '832121957d6c0e27', '2017-06-12 20:35:00', NULL),
(170, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7e961f00c189bf67', '2017-06-14 20:37:00', NULL),
(171, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2eb33c2700dd4c07', '2017-06-16 20:39:00', NULL),
(172, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd8b92dd889c6f99e', '2017-06-23 20:46:00', NULL),
(173, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '4f19cdb79fe732de', '2017-06-23 20:46:00', NULL),
(174, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9795cfdfb352a683', '2017-06-30 20:53:00', NULL),
(175, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'c0e8c068b14926a6', '2017-07-08 20:32:00', NULL),
(176, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0f7cfe26b74c2ac7', '2017-07-09 20:33:00', NULL),
(177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'abde756a2dd1bf41', '2017-07-18 20:42:00', NULL),
(178, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f1802019f23ceeae', '2017-07-19 20:43:00', NULL),
(179, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b6954c500222a32a', '2017-07-21 20:45:00', NULL),
(180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2f21376f9a1e2f1e', '2017-07-21 20:45:00', NULL),
(181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'af05f34f60dcb9e6', '2017-07-29 20:53:00', NULL),
(182, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'eb59e64e9050308e', '2017-08-04 20:29:00', NULL),
(183, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bd9ad2e512a9a121', '2017-08-06 20:31:00', NULL),
(184, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bd9ad2e512a9a121', '2017-08-06 20:31:00', NULL),
(185, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b9586bd3d1af899e', '2017-08-14 20:39:00', NULL),
(186, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '882edb2c9133b6e7', '2017-08-22 20:47:00', NULL),
(187, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8a368543d82d92e6', '2017-08-29 20:54:00', NULL),
(188, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '66459acb74aa4b0f', '2017-08-29 20:54:00', NULL),
(189, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a927d8ef5c3cc420', '2017-08-29 20:54:00', NULL),
(190, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b375587173cc89dd', '2017-08-30 20:55:00', NULL),
(191, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b431504526fad80a', '2017-09-03 20:29:00', NULL),
(192, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '69221c163bf220a7', '2017-09-07 20:33:00', NULL),
(193, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8ae9d6f5800e61ff', '2017-09-11 20:37:00', NULL),
(194, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a5f7d6a6d489fcf3', '2017-09-13 20:39:00', NULL),
(195, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2923f7ae6889217c', '2017-09-15 20:41:00', NULL),
(196, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd34dca151f054bb8', '2017-09-23 20:49:00', NULL),
(197, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5ecc239bb691f214', '2017-10-01 20:28:00', NULL),
(198, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '83f766a6f1dfaed5', '2017-10-08 20:35:00', NULL),
(199, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '713b0b6af0755e8a', '2017-10-15 20:42:00', NULL),
(200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8ecd161b29afa7f3', '2017-10-23 20:50:00', NULL),
(201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd56d0cd938f3d2e6', '2017-10-29 20:56:00', NULL),
(202, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bc4592b199ff0dbc', '2017-11-02 20:30:00', NULL),
(203, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f46e88ec7188b48b', '2017-11-09 20:37:00', NULL),
(204, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '852a234bcb4a5ea1', '2017-11-10 20:38:00', NULL),
(205, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'adc8942a07028268', '2017-11-16 20:44:00', NULL),
(206, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '82d9f2f4f60548b5', '2017-11-21 20:49:00', NULL),
(207, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7b57f4457eb6c91c', '2017-11-24 20:52:00', NULL),
(208, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '49559b7c5d1504b1', '2017-11-28 20:56:00', NULL),
(209, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '749a5f250102579b', '2017-11-28 20:56:00', NULL),
(210, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '71070a7e8f182010', '2017-11-28 20:56:00', NULL),
(211, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a007d449811b6941', '2017-11-28 20:56:00', NULL),
(212, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '6955e74634a6c230', '2017-11-28 20:56:00', NULL),
(213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'da675563bdbb9e0a', '2017-11-29 20:57:00', NULL),
(214, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b09a68e3f7075705', '2017-11-29 20:57:00', NULL),
(215, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '97847f68ca389d2d', '2017-11-29 20:57:00', NULL),
(216, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '429c917b2dd6a09e', '2017-11-29 20:57:00', NULL),
(217, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '18ef35b221bd547d', '2017-11-29 20:57:00', NULL),
(218, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ff3d25dc92d9b64', '2017-11-29 20:57:00', NULL),
(219, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '21020aab0b8e78c0', '2017-11-29 20:57:00', NULL),
(220, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a35537f30882e407', '2017-12-06 20:35:00', NULL),
(221, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '421d666a61128c01', '2017-12-13 20:42:00', NULL),
(222, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dcae0422e0647a5e', '2017-12-18 20:47:00', NULL),
(223, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8552aa85aa3e97c8', '2017-12-18 20:47:00', NULL),
(224, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a71ca57dad4a96f5', '2017-12-30 20:59:00', NULL),
(225, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '30c1dbd9a9ca455c', '2018-01-05 20:24:00', NULL),
(226, 'Juan Huamanchumo', '', NULL, '999473013', '137795467018526', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9ec4b8ac33ea940a', '2018-01-18 20:37:00', NULL),
(227, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '13676bac856c1759', '2018-01-21 20:40:00', NULL),
(228, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '269521e056727d05', '2018-02-03 20:23:00', NULL),
(229, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '55b44a8a6adcd34', '2018-02-06 20:26:00', NULL),
(230, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '3e0946583a85cb2d', '2018-02-17 20:37:00', NULL),
(231, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7b650f9459bb0bac', '2018-02-21 20:41:00', NULL),
(232, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'be89d579ba29ed8d', '2018-03-13 20:34:00', NULL),
(233, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8daf34e73d31e6e5', '2018-03-22 20:43:00', NULL),
(234, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'b4673e1b58e5701f', '2018-03-27 20:48:00', NULL),
(235, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '8903e28fcfc1bdd2', '2018-04-10 20:32:00', NULL),
(236, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0530f244c49133de', '2018-04-14 20:36:00', NULL),
(237, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '5d2c236bcdddde03', '2018-05-05 20:28:00', NULL),
(238, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '65b6d58102ade5e2', '2018-05-27 20:50:00', NULL),
(239, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'f5271879df493e1e', '2018-07-01 20:26:00', NULL),
(240, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a4477e40b8eb514e', '2018-10-28 20:56:00', NULL),
(241, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '34da7f8bea2392cf', '2018-10-28 20:56:00', NULL),
(242, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '60321ccbd5e5060a', '2018-10-28 20:56:00', NULL),
(243, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '455144d2967b97b', '2018-10-30 20:58:00', NULL),
(244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7d583aae897cf862', '2018-11-09 20:38:00', NULL),
(245, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'e7de99f2cc33ec71', '2018-11-19 20:48:00', NULL),
(246, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '974de15fd24479c8', '2018-11-30 20:59:00', NULL),
(247, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '851b59aa34adc7b1', '2018-12-01 20:31:00', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `anuncio`
--
ALTER TABLE `anuncio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `anuncio_busqueda`
--
ALTER TABLE `anuncio_busqueda`
  ADD PRIMARY KEY (`anuncio_id`);

--
-- Indices de la tabla `anuncio_favorito`
--
ALTER TABLE `anuncio_favorito`
  ADD PRIMARY KEY (`anuncio_id`,`usuario_id`);

--
-- Indices de la tabla `busqueda`
--
ALTER TABLE `busqueda`
  ADD PRIMARY KEY (`id`,`usuario_id`);

--
-- Indices de la tabla `busqueda_usuario`
--
ALTER TABLE `busqueda_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cargamoneda`
--
ALTER TABLE `cargamoneda`
  ADD PRIMARY KEY (` id`),
  ADD KEY `usuario_id_f` (`usuario_id`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `condicion`
--
ALTER TABLE `condicion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `denuncia`
--
ALTER TABLE `denuncia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `distrito`
--
ALTER TABLE `distrito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id`,`Categoria_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `anuncio`
--
ALTER TABLE `anuncio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT de la tabla `busqueda`
--
ALTER TABLE `busqueda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `busqueda_usuario`
--
ALTER TABLE `busqueda_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;
--
-- AUTO_INCREMENT de la tabla `cargamoneda`
--
ALTER TABLE `cargamoneda`
  MODIFY ` id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `condicion`
--
ALTER TABLE `condicion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `denuncia`
--
ALTER TABLE `denuncia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `departamento`
--
ALTER TABLE `departamento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `distrito`
--
ALTER TABLE `distrito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
